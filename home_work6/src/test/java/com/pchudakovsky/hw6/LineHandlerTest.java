package com.pchudakovsky.hw6;

import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.util.*;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class LineHandlerTest {
    private LineHandler lineHandler = new LineHandler();

    @Test
    public void testGetListTextNotEmpty() {
        String text = "Hello java! Hello world!";
        ArrayList<String> list = lineHandler.getListText(text);
        assertTrue(!list.isEmpty());
    }

    @Test
    public void testGetListTextOnSearchFirstElement() {
        String text = " Java Hello! Hello world!";
        ArrayList<String> list = lineHandler.getListText(text.trim());
        Optional<String> first = list.stream().findFirst();
        assertTrue(first.isPresent());
        assertThat(first.get(), is("java"));
    }

    @Test
    public void testGetListTextOnSearchAnyElement() {
        String text = " Java Hello! Hello world!";
        ArrayList<String> list = lineHandler.getListText(text.trim());
        Optional<String> any = list.stream().findAny();
        assertTrue(any.isPresent());
        assertThat(any.get(), anyOf(is("java"), is("hello"), is("world")));
    }

    @Test
    public void testFilterAndGroup() {
        String[] letter = {"A", "B", "C"};
        String text = "apple banana banana pear apple apple";
        ArrayList<String> textList = lineHandler.getListText(text);
        Map<String, Map> resultAll = lineHandler.filterAndGroup(letter, textList);

        Assertions.assertThat(resultAll).isNotEmpty().containsKeys("B", "C")
                //  B, C -are contained in the collection
                //  but C={} values are empty
                .doesNotContainKeys("D");

        lineHandler.print(resultAll);

    }

    @Test
    public void testGetResultOnOnlyCorrectResult() {
        String[] letter = {"A", "B", "C", "D", "P"};
        String text = "apple banana banana pear apple apple";

        ArrayList<String> textList = lineHandler.getListText(text);
        TreeMap<String, Map> resultAll = lineHandler.filterAndGroup(letter, textList);
        Map<String, Map> correctResult = lineHandler.getResult(resultAll);

        assertTrue(lineHandler.isFlag());
        Assertions.assertThat(correctResult).isNotEmpty()
                .containsOnlyKeys("A", "B", "P");

        lineHandler.print(correctResult);
    }
}