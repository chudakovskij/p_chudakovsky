package com.pchudakovsky.hw6;

/**
 * Test class.
 *
 * @author Pavel Chudakovsky.
 */
public class Main {

    public static void main(String[] args) {

        // ! You can change this text for any tests
        String text = "Once upon a  time a Wolf was lapping at a spring "
                + "on a hillside, when, looking up, what should he see but a "
                + "Lamb just beginning to drink a little lower down.";

        Start start = new Start(text, new LineHandler());
        start.run();
    }
}
