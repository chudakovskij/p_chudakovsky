package com.pchudakovsky.hw6;

import com.pchudakovsky.hw6.api.Handler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

import static java.util.Map.Entry;

/**
 * Class represents methods for analysis text.
 * Methods based on Stream API.
 *
 * @author Pavel Chudakovsky.
 */
class LineHandler implements Handler {

    /**
     * Used in tests.
     */
    private boolean flag;

    /**
     * Getter.
     *
     * @return value of boolean.
     */

    boolean isFlag() {
        return flag;
    }

    /**
     * Setter.
     *
     * @param flag as value of boolean.
     */
    private void setFlag(boolean flag) {
        this.flag = flag;
    }

    /**
     * Returns the result of grouping for
     * all letters of the alphabet.
     *
     * @param nameLetter an array of capital letters.
     * @param listText   stores an ArrayList that
     *                   contains text without punctuation.
     *                   Used when filtering and grouping.
     * @return TreeMap collection, which stores
     * key:name of the capital letter
     * and value: is the result of grouping.
     */

    public TreeMap<String, Map>
    filterAndGroup(String[] nameLetter, ArrayList<String> listText) {
        setFlag(false);

        // create a collection TreeMap to store the letter and grouping result
        TreeMap<String, Map> map = new TreeMap<>();
        for (int i = 0; i <= nameLetter.length - 1; i++) {
            Letter letter = new Letter(nameLetter[i]);

            //  filtering text by letter name: filterList as filtered list
            ArrayList<String> filterList = (ArrayList<String>) listText.stream()
                    .filter(x -> x.startsWith(letter.getName().toLowerCase()))
                    .collect(Collectors.toList());

            // for each letter, a grouping is performed in the filtered list:
            // group- stores result a grouping
            Map<Object, Long> group = filterList.stream().
                    collect(Collectors.groupingBy(word -> word,
                            Collectors.counting()));

            // stores of collection group in map
            map.put(letter.getName(), group);
            setFlag(true);
        }
        return map;
    }

    /**
     * Returns ArrayList that contains
     * elements of string (text) without punctuation.
     *
     * @param text string value.
     * @return listText which is used in the method
     * {@link LineHandler#filterAndGroup(String[], ArrayList)};
     */
    public ArrayList<String> getListText(String text) {
        ArrayList<String> listText = new ArrayList<>();
        String line = text.replaceAll("[\\W]", " ").toLowerCase();
        String[] array = line.split(" ");
        Collections.addAll(listText, array);
        return listText;
    }

    /**
     * Returns the grouping result only for those letters
     * that are at the beginning of words.
     *
     * @param resultAll sorted collection to store
     *                  the letter and grouping result.
     * @return the grouping result only for those letters
     * that are at the beginning of words.
     */

    public Map<String, Map> getResult(TreeMap<String, Map> resultAll) {
        return resultAll.entrySet().stream()
                .filter(x -> !x.getValue().isEmpty())
                .collect(Collectors.toMap(Entry::getKey, Entry::getValue));
    }

    /**
     * Method displays the result.
     *
     * @param map collection for print
     */
    public void print(Map<String, Map> map) {
        map.entrySet().stream().sorted(Entry.comparingByKey())
                .forEach(o -> System.out.println("Letter:" + o));
        System.out.println("**********");
    }

}
