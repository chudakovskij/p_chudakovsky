package com.pchudakovsky.hw6.api;


import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

/**
 * Contains methods that are
 * implemented in class LineHandler.
 *
 * @author Pavel Chudakovsky.
 */
public interface Handler {
    /**
     * Returns the result of grouping for
     * all letters of the alphabet.
     *
     * @param nameLetter an array of capital letters.
     * @param listText   stores an ArrayList that
     *                   contains text without punctuation.
     *                   Used when filtering and grouping.
     * @return TreeMap collection, which stores
     * key:name of the capital letter
     * and value: is the result of grouping.
     */
    TreeMap<String, Map>
    filterAndGroup(String[] nameLetter, ArrayList<String> listText);

    /**
     * Returns ArrayList that contains elements
     * of string (text) without punctuation.
     *
     * @param text string value.
     * @return listText which is used in the method
     * {@link Handler#filterAndGroup(String[], ArrayList)};
     */
    ArrayList<String> getListText(String text);

    /**
     * Returns the grouping result only for those letters
     * that are at the beginning of words.
     *
     * @param resultAll sorted collection to store the
     *                  letter and grouping result.
     * @return the grouping result only for those letters
     * that are at the beginning of words.
     */
    Map<String, Map>
    getResult(TreeMap<String, Map> resultAll);

    /**
     * Method displays the result.
     *
     * @param map collection for print
     */
    void print(Map<String, Map> map);
}
