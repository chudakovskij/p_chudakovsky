package com.pchudakovsky.hw6;

/**
 * Class describes of entity "Letter".
 *
 * @author Pavel Chudakovsky.
 */
class Letter {

    /**
     * Array stores of name letter.
     */
    static final String[] NAME_LETTER =
            {"A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "L", "M", "N",
                    "Q", "O", "P", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};

    /**
     * Name  letter.
     */
    private String name;

    /**
     * Sole constructor.
     *
     * @param name as name letter
     */
    Letter(String name) {
        this.name = name;
    }

    /**
     * Getter for name.
     *
     * @return name letter
     */

    String getName() {
        return name;
    }

}
