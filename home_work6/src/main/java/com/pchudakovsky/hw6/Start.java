package com.pchudakovsky.hw6;

import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

import static com.pchudakovsky.hw6.Letter.NAME_LETTER;

/**
 * This class for launch app.
 *
 * @author Pavel Chudakovsky.
 */
class Start {
    /**
     * Input string value.
     */
    private String text;

    /**
     * Used for string analysis.
     */
    private LineHandler lineHandler;

    /**
     * Sole constructor.
     *
     * @param text        string value.
     * @param lineHandler value of type LineHandler.
     */
    Start(String text, LineHandler lineHandler) {
        this.text = text;
        this.lineHandler = lineHandler;
    }

    /**
     * Stores of name letter.
     */
    private String[] nameLetter = NAME_LETTER;

    /**
     * Runs class LineHandler methods.
     *
     * @see LineHandler
     */
    void run() {
        info();
        ArrayList<String> listText = lineHandler.getListText(text);
        TreeMap<String, Map> resultAll = lineHandler.filterAndGroup(nameLetter, listText); // содержит нулевые все результаты
        Map<String, Map> resultCorrect = lineHandler.getResult(resultAll);
        lineHandler.print(resultCorrect);
    }

    /**
     * Prints message.
     */
    private void info() {
        System.out.println("Program *STATISTICS*\n" + "***RESULT***");
    }
}
