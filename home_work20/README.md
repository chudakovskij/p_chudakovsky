[TOC]

#  Online shop

## To run the application:

-    plugin(Intellij IDEA)  -> jetty:run  OR cmd: mvn clean package jetty:run;

-  **for Start page:**
  1) Enter address line of browser:  localhost:8080/online-shop (for start page of Online shop);
  2) Enter **username "admin"** and  **password "admin1"** click button **"Enter"**.
     ( will be redirected ->( localhost:8080/menu  (for menu  page))).

- **for Menu page:**
    Make a product selection and press the *buttons*:

 button  **"Add item"**:
 (will be redirected->(http://localhost:8080/add );

  button   **"Submit"**:
  will be redirected-> (localhost:8080/order (for Order page)).

- **for Order page:**

1)  Click button **"LogOut"** to out online shop .

2) Click button **"Save"** to save order.

------------

###### !Warning: use post requests (through forms and application buttons).

**! INFO:** *URL for controllers:

 -  /menu MenuController (method menu())
 - /order OrderController (method submit())
 - /add BasketController (method add())
 - /save SaveController (method save())
 - /error ErrorController (method error())
 
 **! INFO:** Login(Name) and Password  of User:
 
  - Login **admin**; Password **admin1** .
  - Login **Petrov**; Password **admin2** .
  - Login **Sidorov**; Password **admin3** .
  - Login **Fedorov**; Password **admin4** .
  - Login **Alexsandrov**; Password **admin5** .
  