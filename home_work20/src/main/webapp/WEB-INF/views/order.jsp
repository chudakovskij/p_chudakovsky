<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false"%>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Online Shop</title>
</head>

<body>
    <h1> Dear ${userName}, you order:</h1>
    <div> ${message}</div>
    <c:forEach var="userOrder" items="${order}">
        <div>
            ${userOrder}
        </div>
    </c:forEach>
    <h3> total: $  ${total} </h3>
      <form action="/save" method="post">
           <button type="submit" formaction="/logout"> LogOut</button>
           <button type="submit" > Save</button>
    </form>
</body>

</html>