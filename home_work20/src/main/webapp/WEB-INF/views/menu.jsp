<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false"%>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Online Shop</title>
</head>

<body>

<div>
<h3>Basket: </h3>
 <select>
        <c:forEach var="productInBasket" items="${basket}">
         <option>
          ${productInBasket}
          </option>
           </c:forEach>
  </select>
</div>
    <div align="center">
        <h1> Hello ${userName} !</h1>
        <form method="post" action="order"
            style=" width:350px;height: 350px; border-style: solid; background-color: coral;">
            <div style="margin: 5% 25% 25% 25%;color:ivory;font-size: larger;">
                <div>Make you order:</div>
                <select style="margin: 10% 25% 25% 15%;color:black;font-size: small;" name="selectProducts">
                    <c:forEach var="product" items="${ProductsInShop}">
                        <option>
                            ${product.name} ${product.cost} $
                        </option>
                    </c:forEach>
                </select>
                <div>
                    <input type="submit" value="Add Item" formaction="/add" />
                    <input type="submit" value="Submit" />
                </div>
                <div style="color:ivory;font-size: medium;">
                    <br />
                    Click on the product to select it
                </div>
                   <input type="submit" value="LogOut" formaction="/logout" />
        </form>
    </div>

</body>


</html>