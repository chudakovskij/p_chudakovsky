package com.pchudakovsky.exception;

/**
 * Custom class of exception.
 *
 * @author Pavel Chudakovsky.
 */
public class BadValidateException extends RuntimeException {

    /**
     * Message.
     */
    private String message;

    /**
     * The First constructor.
     *
     * @param message as string.
     */
    public BadValidateException(String message) {
        this.message = message;
    }

    /**
     * The second constructor.
     */
    public BadValidateException() {
        this.message = "Incorrect data entered !";
    }

    /**
     * Returns message about exception.
     *
     * @return message.
     */
    public String getMessage() {
        return message;
    }

}
