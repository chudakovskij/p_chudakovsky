package com.pchudakovsky.exception.handler;

import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.NoHandlerFoundException;

/**
 * Custom ExceptionHandler.
 *
 * @author Pavel Chudakovsky.
 */
@ControllerAdvice
public class GlobalExceptionHandler {

    /**
     * @return error 404.
     */
    @ExceptionHandler(NoHandlerFoundException.class)
    public ModelAndView notFound() {
        return new ModelAndView("error/error404");
    }

    /**
     * @return error 405.
     */
    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public ModelAndView notMapping() {
        return new ModelAndView("error/error405");
    }

}
