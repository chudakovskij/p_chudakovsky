package com.pchudakovsky.service;

import com.pchudakovsky.data.DataBaseStart;
import com.pchudakovsky.entity.User;
import com.pchudakovsky.repository.api.DataBaseRepository;
import com.pchudakovsky.repository.api.UserRepository;
import com.pchudakovsky.role.Role;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * Custom UserDetailService.
 */
@Service
public class CustomUserDetailService implements UserDetailsService {

    /**
     * User repository {@link UserRepository}
     */
    @Autowired
    private UserRepository userRepository;

    /**
     * Data base  repository {@link DataBaseRepository}
     */
    @Autowired
    private DataBaseRepository dataBaseRepository;

    /**
     * Sole constructor.
     *
     * @param userRepository     {@link UserRepository}
     * @param dataBaseRepository {@link DataBaseRepository}
     */
    @Autowired
    public CustomUserDetailService(UserRepository userRepository, DataBaseRepository dataBaseRepository) {
        this.userRepository = userRepository;
        this.dataBaseRepository = dataBaseRepository;
    }

    /**
     * Logger.
     */
    private final static Logger LOGGER = LogManager.getLogger(ShopServiceImpl.class);


    /**
     * Builder.
     */
    private org.springframework.security.core.userdetails
            .User.UserBuilder builder = null;

    /**
     * @param loginUser login of User as string.
     * @return {@link UserDetails}
     * @throws UsernameNotFoundException look at{@link UsernameNotFoundException}
     */
    @Override
    public UserDetails loadUserByUsername(String loginUser) throws UsernameNotFoundException {

        if (!DataBaseStart.isFlag()) {
            try {
                DataBaseStart.createDatabase();
                LOGGER.info("DataBase is created");
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
                LOGGER.error("DataBase is not created", e);
            }
        }

        User user = userRepository.getUserByName(loginUser, dataBaseRepository);
        user.setRole(Role.ADMIN);

        builder = org.springframework.security.core.userdetails
                .User.withUsername(user.getName());
        builder.password(user.getPassword());
        builder.roles(user.getRole().name());
        return builder.build();
    }

}
