package com.pchudakovsky.service.api;

import com.pchudakovsky.entity.Product;
import com.pchudakovsky.repository.DataBaseRepositoryImpl;

import java.math.BigDecimal;
import java.util.List;

/**
 * Provides methods for
 * the operation of the online store service.
 *
 * @author Pavel Chudakovsky.
 * @see com.pchudakovsky.repository.api.OrderRepository
 * @see com.pchudakovsky.repository.api.UserRepository
 * @see com.pchudakovsky.repository.api.ProductRepository
 */
public interface ShopService {

    Integer getIdUserByName(String userName);

    String getNameUser(String userName);

    List<Product> getAllProducts();

    String getNameProductFromSelect(String product);

    Integer getIdProductByTitle(String title);

    BigDecimal getTotalCost(String[] userProducts);

    void saveOrder(Integer userId, BigDecimal totalPrice);

    void getAllOrders();

    Integer getIdOrdersByIdUser(Integer userId);

    void saveOrdersProducts(Integer orderId, Integer productId);

    void getAllOrdersProducts();

    List<String> joinOrdersProducts(Integer userId);


}

