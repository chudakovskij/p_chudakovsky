package com.pchudakovsky.service;

import com.pchudakovsky.entity.Product;
import com.pchudakovsky.repository.api.DataBaseRepository;
import com.pchudakovsky.repository.api.OrderRepository;
import com.pchudakovsky.repository.api.ProductRepository;
import com.pchudakovsky.repository.api.UserRepository;
import com.pchudakovsky.service.api.ShopService;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

/**
 * Provides methods for
 * the operation of the online store service.
 *
 * @author Pavel Chudakovsky.
 * @see OrderRepository
 * @see UserRepository
 * @see ProductRepository
 */
@Service
public class ShopServiceImpl implements ShopService {

    /**
     * Repository for user.
     */
    private UserRepository userRepository;

    /**
     * Repository for product.
     */
    private ProductRepository productRepository;

    /**
     * Repository for Order.
     */
    private OrderRepository orderRepository;

    /**
     * Repository for DataBase.
     */
    private DataBaseRepository baseRepository;

    /**
     * Sole constructor.
     *
     * @param userRepository    {@link UserRepository}
     * @param productRepository {@link ProductRepository}
     * @param orderRepository   {@link OrderRepository}
     * @param baseRepository    {@link DataBaseRepository}
     */
    public ShopServiceImpl(UserRepository userRepository,
                           ProductRepository productRepository,
                           OrderRepository orderRepository,
                           DataBaseRepository baseRepository) {

        this.userRepository = userRepository;
        this.productRepository = productRepository;
        this.orderRepository = orderRepository;
        this.baseRepository = baseRepository;
    }


    /**
     * @param userName username.
     * @return id of user.
     */
    public Integer getIdUserByName(String userName) {
        return userRepository.getIdUserByName(userName, baseRepository);
    }

    /**
     * @param userName username.
     * @return username {@link com.pchudakovsky.repository.api.UserRepository}
     */
    public String getNameUser(String userName) {
        return userRepository.getNameUser(userName, baseRepository);
    }

    /**
     * @return list of products.
     */
    public List<Product> getAllProducts() {
        return productRepository.getAllProducts(baseRepository);
    }

    /**
     * @param product product from select.
     * @return name of product.
     */
    public String getNameProductFromSelect(String product) {
        return productRepository.getNameProductFromSelect(product);
    }

    /**
     * @param title title (name) for product.
     * @return id of product.
     */
    public Integer getIdProductByTitle(String title) {
        return productRepository.getIdProductByTitle(title, baseRepository);
    }

    /**
     * @param userProducts products for user.
     * @return cost as BigDecimal.
     */
    public BigDecimal getTotalCost(String[] userProducts) {
        return productRepository.getTotalCost(userProducts);
    }

    /**
     * Saves orders(table Orders).
     *
     * @param userId     id  for user.
     * @param totalPrice total price as BigDecimal.
     */
    public void saveOrder(Integer userId, BigDecimal totalPrice) {
        orderRepository.saveOrder(userId, totalPrice, baseRepository);
    }

    /**
     * Returns all orders.
     */
    public void getAllOrders() {
        orderRepository.getAllOrders(baseRepository);
    }

    /**
     * Id of order by id of user/
     *
     * @param userId id for user.
     * @return id order.
     */
    public Integer getIdOrdersByIdUser(Integer userId) {
        return orderRepository.getIdOrdersByIdUser(userId, baseRepository);
    }


    /**
     * Saves table Orders_Products.
     *
     * @param orderId   id for order.
     * @param productId id for product.
     */
    public void saveOrdersProducts(Integer orderId, Integer productId) {
        orderRepository.saveOrdersProducts(orderId, productId, baseRepository);
    }

    /**
     * Displays all the table fields in the console
     * (table Orders_Products).
     */
    public void getAllOrdersProducts() {
        orderRepository.getAllOrdersProducts(baseRepository);
    }

    /**
     * @param userId id for user.
     * @return list for orders.
     */
    public List<String> joinOrdersProducts(Integer userId) {
        return orderRepository.joinOrdersProducts(userId, baseRepository);
    }
}


