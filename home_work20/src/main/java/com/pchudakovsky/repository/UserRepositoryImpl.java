package com.pchudakovsky.repository;

import com.pchudakovsky.entity.User;
import com.pchudakovsky.repository.api.DataBaseRepository;
import com.pchudakovsky.repository.api.UserRepository;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Provides methods for
 * for repository of user.
 *
 * @author Pavel Chudakovsky.
 */
@Repository
public class UserRepositoryImpl implements UserRepository {

    // used to create queries
    private static final String SELECT_USER_BY_NAME = "SELECT * FROM Users WHERE name=";

    /**
     * Returns name of user from data base.
     *
     * @param name           name for user.
     * @param baseRepository object fo type  {@link DataBaseRepository}.
     * @return name of user from data base.
     */
    public String getNameUser(String name, DataBaseRepository baseRepository) {
        String nameUser = null;
        Connection connection = baseRepository.getConnection();
        try {
            Statement statement = connection.createStatement();
            ResultSet result = statement.executeQuery("SELECT * FROM Users " +
                    " WHERE name=" + "'" + name + "'");
            while (result.next()) {
                nameUser = result.getString("Name");
            }
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return nameUser;
    }

    /**
     * Id user by username.
     *
     * @param loginUser      login (name) of  user.
     * @param baseRepository object fo type  {@link DataBaseRepository}.
     * @return id user by username.
     */
    public Integer getIdUserByName(String loginUser, DataBaseRepository baseRepository) {
        Integer idUser = null;
        Connection connection = baseRepository.getConnection();
        try {
            Statement statement = connection.createStatement();
            ResultSet result = statement.executeQuery(SELECT_USER_BY_NAME + "'" + loginUser + "'");
            while (result.next()) {
                idUser = result.getInt("id");
            }
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return idUser;
    }

    /**
     * Returns of user by username.
     *
     * @param loginUser      login (name) of  user.
     * @param baseRepository object of type {@link DataBaseRepository}.
     * @return user by username.
     */
    public User getUserByName(String loginUser, DataBaseRepository baseRepository) {

        Connection connection = baseRepository.getConnection();
        String userName = null;
        Integer idUser = null;
        String password = null;
        try {
            Statement statement = connection.createStatement();
            ResultSet result = statement.executeQuery(SELECT_USER_BY_NAME + "'" + loginUser + "'");

            while (result.next()) {
                idUser = result.getInt("id");
                userName = result.getString("Name");
                password = result.getString("Password");
            }
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return new User(idUser, userName, password);


    }
}
