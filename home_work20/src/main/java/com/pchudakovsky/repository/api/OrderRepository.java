package com.pchudakovsky.repository.api;

import java.math.BigDecimal;
import java.util.List;

/**
 * Provides of methods for OrderRepository.
 *
 * @author Pavel Chudakovsky.
 */
public interface OrderRepository {
    /**
     * Saves orders.
     *
     * @param userId         id  for user.
     * @param totalPrice     total price as BigDecimal.
     * @param baseRepository object of {@link DataBaseRepository}.
     */
    void saveOrder(Integer userId, BigDecimal totalPrice, DataBaseRepository baseRepository);

    /**
     * Saves orders and product
     * (table Orders_Products).
     *
     * @param orderId        id for order.
     * @param productId      id for product.
     * @param baseRepository object of {@link DataBaseRepository}.
     */
    void saveOrdersProducts(Integer orderId, Integer productId, DataBaseRepository baseRepository);

    /**
     * Returns Id of order for user.
     *
     * @param idUser         id for user.
     * @param baseRepository object of  {@link DataBaseRepository}.
     * @return id for order.
     */
    Integer getIdOrdersByIdUser(Integer idUser, DataBaseRepository baseRepository);

    /**
     * Displays all the table fields in the console
     * (table Orders).
     *
     * @param baseRepository object of  {@link DataBaseRepository}.
     */
    void getAllOrders(DataBaseRepository baseRepository);

    /**
     * Displays all the table fields in the console
     * (table Orders_Products).
     *
     * @param baseRepository object of  {@link DataBaseRepository}.
     */
    void getAllOrdersProducts(DataBaseRepository baseRepository);

    /**
     * Returns list of orders.
     *
     * @param userId         id for user.
     * @param baseRepository object of  {@link DataBaseRepository}.
     * @return list for orders.
     */
    List<String> joinOrdersProducts(Integer userId, DataBaseRepository baseRepository);
}
