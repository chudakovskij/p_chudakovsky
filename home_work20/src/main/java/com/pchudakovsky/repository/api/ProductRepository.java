package com.pchudakovsky.repository.api;

import com.pchudakovsky.entity.Product;

import java.math.BigDecimal;
import java.util.List;

/**
 * Provides of methods for ProductRepository.
 *
 * @author Pavel Chudakovsky.
 */
public interface ProductRepository {

    /**
     * Returns title for product.
     *
     * @param products string from select(menu)
     * @return name(title) for product.
     */
    String getNameProductFromSelect(String products);

    /**
     * Returns total price of products.
     *
     * @param products information about products as array of string.
     * @return total price as BigDecimal.
     */
    BigDecimal getTotalCost(String[] products);

    /**
     * Returns Id of product by title product.
     *
     * @param title          title of product.
     * @param baseRepository object of type  {@link DataBaseRepository}.
     * @return id for product.
     */
   Integer getIdProductByTitle(String title, DataBaseRepository baseRepository);

    /**
     * Returns list of products from table Products
     *
     * @param baseRepository object of type {@link DataBaseRepository}.
     * @return list of products for menu(select)
     */
    List<Product> getAllProducts(DataBaseRepository baseRepository);
}
