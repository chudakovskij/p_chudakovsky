package com.pchudakovsky.repository.api;

import com.pchudakovsky.entity.User;

/**
 * Provides of methods for UserRepository.
 *
 * @author Pavel Chudakovsky.
 */
public interface UserRepository {

    /**
     * Returns name of user from data base.
     *
     * @param name           name for user.
     * @param baseRepository object fo type {@link DataBaseRepository}
     * @return name of user from data base.
     */
    String getNameUser(String name, DataBaseRepository baseRepository);

    /**
     * Id user by username.
     *
     * @param loginUser      login (name) of  user.
     * @param baseRepository object fo type {@link DataBaseRepository}.
     * @return id user by username.
     */
    Integer getIdUserByName(String loginUser, DataBaseRepository baseRepository);

    /**
     * Returns of user by username.
     *
     * @param loginUser      login (name) of  user.
     * @param baseRepository object of type {@link DataBaseRepository}.
     * @return user by username.
     */
    User getUserByName(String loginUser, DataBaseRepository baseRepository);
}
