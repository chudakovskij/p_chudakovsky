package com.pchudakovsky.webConfiguration;

import com.pchudakovsky.service.CustomUserDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;


/**
 * Spring Security Configuration.
 *
 * @author Pavel Chudakovsky.
 */
@Configuration
@EnableWebSecurity
public class CustomSecurityConfig extends WebSecurityConfigurerAdapter {


    /**
     * @see CustomUserDetailService
     */
    @Autowired
    public CustomUserDetailService customUserDetailService;


    @Autowired
    public void configure(AuthenticationManagerBuilder auth) throws Exception {

        auth.userDetailsService(customUserDetailService).passwordEncoder(passwordEncoder());
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/online-shop").permitAll()
                .antMatchers(HttpMethod.GET, "/menu/**").hasRole("ADMIN")
                .and()
                .formLogin().loginPage("/online-shop")
                .defaultSuccessUrl("/menu")
                .failureUrl("/error")
                .usernameParameter("username").passwordParameter("password")
                .and();
        http.csrf().disable();


    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
