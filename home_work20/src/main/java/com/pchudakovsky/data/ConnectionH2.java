package com.pchudakovsky.data;

import com.pchudakovsky.utility.ReaderProperty;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Provides a connection to the data base.
 */
public class ConnectionH2 {

    /**
     * Driver for DB.
     */
    private static final String DB_DRIVER = ReaderProperty.getPropertyFromDb("jdbc.driverClassName");

    /**
     * URL for DB.
     */
    private static final String DB_URL = ReaderProperty.getPropertyFromDb("jdbc.url");

    /**
     * Username.
     */
    private static final String DB_USER_NAME = ReaderProperty.getPropertyFromDb("jdbc.username");
    private static final String DB_PASSWORD = ReaderProperty.getPropertyFromDb("jdbc.password");

    /**
     * Object Connection.
     */
    private static Connection connection = null;

    /**
     * Checks database connection.
     *
     * @return variable of type Connection.
     * @throws ClassNotFoundException when the class
     *                                is not detected.
     */
    static Connection checkConnection() throws ClassNotFoundException {

        try {
            Class.forName(DB_DRIVER);
            connection = DriverManager.getConnection(DB_URL, DB_USER_NAME, DB_PASSWORD);
            if (connection != null) {
                System.out.println("Connected to the database");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }

    /**
     * Returns object of connection.
     *
     * @return variable of type Connection.
     */
    public Connection getConnection() {
        return connection;
    }
}
