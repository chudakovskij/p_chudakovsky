package com.pchudakovsky.role;

/**
 * Role for User.
 */
public enum Role {
    ADMIN, USER;

    Role() {
    }
}
