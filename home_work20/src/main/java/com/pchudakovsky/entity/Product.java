package com.pchudakovsky.entity;

import java.math.BigDecimal;

/**
 * Describes the product for Online shop.
 *
 * @author Pavel Chudakovsky.
 */
public class Product {

    /**
     * Id of product.
     */
    private Integer idProduct;


    /**
     * Name of product.
     */
    private String name;

    /**
     * Cost of product.
     */
    private BigDecimal cost;

    /**
     * Sole constructor.
     *
     * @param name name of product.
     * @param cost ost of product.
     */
    public Product(Integer idProduct, String name, BigDecimal cost) {
        this.name = name;
        this.cost = cost;
        this.idProduct = idProduct;
    }


    /**
     * Getter for Id of product.
     *
     * @return id of Product.
     */
    @SuppressWarnings("unused")
    public Integer getIdProduct() {
        return idProduct;
    }

    /**
     * Getter for name of product.
     *
     * @return name of product.
     */
    @SuppressWarnings("unused")
    public String getName() {
        return name;
    }

    /**
     * Getter for cost of product.
     *
     * @return cost of product.
     */

    @SuppressWarnings("unused")
    public BigDecimal getCost() {
        return cost;
    }

}
