package com.pchudakovsky.entity;

import java.math.BigDecimal;

/**
 * Describes the order for Online shop.
 *
 * @author Pavel Chudakovsky.
 */
@SuppressWarnings("unused")
public class Order {

    /**
     * Id of Order.
     */
    private Integer id;

    /**
     * Total.
     */
    private BigDecimal totalPrice;


    /**
     * Getter for Id.
     *
     * @return id of order.
     */
    public Integer getId() {
        return id;
    }

    /**
     * Setter for Id.
     *
     * @param id Id for order.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Getter for total Price.
     *
     * @return total price as {@link BigDecimal}
     */
    @SuppressWarnings("unused")
    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    /**
     * Setter for total price.
     *
     * @param totalPrice total price as {@link BigDecimal}
     */
    @SuppressWarnings("unused")
    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }
}
