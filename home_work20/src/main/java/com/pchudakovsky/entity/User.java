package com.pchudakovsky.entity;

import com.pchudakovsky.role.Role;

/**
 * Describes of behavior for user.
 */
public class User {
    /**
     * Id for User.
     */
    private Integer id;
    /**
     * User name.
     */
    private String name;

    /**
     * Password of user.
     */
    private String password;

    /**
     * Role of user.
     */
    private Role role;

    /**
     * Empty constructor.
     */
    public User() {
    }

    /**
     * the First constructor.
     *
     * @param name user name.
     */
    @SuppressWarnings("unused")
    public User(String name) {
        this.name = name;
    }

    /**
     * The second constructor.
     *
     * @param name user name.
     */
    @SuppressWarnings("unused")
    public User(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    /**
     * Third constructor.
     *
     * @param id       id of user.
     * @param name     name of user.
     * @param password password of user.
     */
    public User(Integer id, String name, String password) {
        this.id = id;
        this.name = name;
        this.password = password;
    }

    /**
     * Getter for Id.
     *
     * @return id of user.
     */
    public Integer getId() {
        return id;
    }

    /**
     * Setter for id of user.
     *
     * @param id of user.
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Getter for name.
     *
     * @return user name.
     */
    @SuppressWarnings("unused")
    public String getName() {
        return name;
    }

    /**
     * Setter for name.
     *
     * @param name user name.
     */
    @SuppressWarnings("unused")
    public void setName(String name) {
        this.name = name;
    }


    /**
     * Getter for password.
     *
     * @return password of User.
     */
    public String getPassword() {
        return password;
    }

    /**
     * Setter for password.
     *
     * @param password of user.
     */
    @SuppressWarnings("unused")
    public void setPassword(String password) {
        this.password = password;
    }


    /**
     * Getter for role of user.
     *
     * @return role of user.
     */
    public Role getRole() {
        return role;
    }

    /**
     * Setter for role of user.
     *
     * @param role of user.
     */
    public void setRole(Role role) {
        this.role = role;
    }
}
