package com.pchudakovsky.controllers;

import com.pchudakovsky.entity.User;
import com.pchudakovsky.service.ShopServiceImpl;
import com.pchudakovsky.service.api.ShopService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.bind.annotation.SessionAttributes;

import java.math.BigDecimal;
import java.util.List;

/**
 * Controller to saves orders of User to data base.
 *
 * @author Pavel Chudakovsky.
 */
@Controller
@SessionAttributes({"saveMessage", "orderOnDisplay"})
public class SaveController {

    /**
     * Logger.
     */
    private final static Logger LOGGER = LogManager.getLogger(ShopServiceImpl.class);

    /**
     * Service.
     */
    private final ShopService shopService;

    /**
     * Sole constructor.
     *
     * @param shopService service of Online-shop.
     */
    public SaveController(ShopService shopService) {
        this.shopService = shopService;
    }

    /**
     * Saves orders.
     *
     * @param modelMap      {@link ModelMap}
     * @param user          {@link User}
     * @param total         total as {@link BigDecimal}
     * @param listIdProduct list Id of products as {@link List}
     * @return view as string.
     */
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String save(ModelMap modelMap,
                       @SessionAttribute("user") User user,
                       @SessionAttribute("total") BigDecimal total,
                       @SessionAttribute("listIdProduct") List<Integer> listIdProduct) {

        LOGGER.info("Method save() for "
                + SaveController.class + " is started!");

        // save the order to the Orders table
        shopService.saveOrder(user.getId(), total);
        shopService.getAllOrders();

        Integer idOrders = shopService.getIdOrdersByIdUser(user.getId());

        // save the order to the OrdersProducts table
        if (!listIdProduct.isEmpty()) {
            modelMap.addAttribute("saveMessage", "Order saved...");
            for (Integer id : listIdProduct) {
                shopService.saveOrdersProducts(idOrders, id);
            }
            // for information on the console
            shopService.getAllOrdersProducts();

        } else {
            modelMap.addAttribute("saveMessage", "Order not saved...");
        }

        List<String> onDisplay = shopService.joinOrdersProducts(user.getId());
        modelMap.addAttribute("orderOnDisplay", onDisplay);

        return "orderSave";
    }
}
