package com.pchudakovsky.controllers;

import com.pchudakovsky.service.ShopServiceImpl;
import com.pchudakovsky.service.api.ShopService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.bind.annotation.SessionAttributes;

import java.math.BigDecimal;
import java.util.List;

/**
 * Controller for Order of online-shop.
 *
 * @author Pavel Chudakovsky.
 */
@Controller
@SessionAttributes({"message", "total", "order"})
public class OrderController {

    /**
     * Logger.
     */
    private final static Logger LOGGER = LogManager.getLogger(ShopServiceImpl.class);

    /**
     * Service.
     */
    private final ShopService shopService;

    /**
     * Sole constructor.
     *
     * @param shopService service of Online-shop {@link ShopService}
     */
    public OrderController(ShopService shopService) {
        this.shopService = shopService;
    }

    /**
     * Returns view for orders of User.
     *
     * @param modelMap   {@link ModelMap}
     * @param basketUser basket of User as {@link List}
     * @return view as string.
     */
    @RequestMapping(value = "/order", method = RequestMethod.POST)
    public String submit(ModelMap modelMap,
                         @SessionAttribute("basket") List<String> basketUser) {

        LOGGER.info("Method submit() for "
                + OrderController.class + " is started!");

        if (basketUser.isEmpty()) {
            String message = "Sorry, list of products is empty!";
            modelMap.addAttribute("message", message);
        } else {
            modelMap.addAttribute("message", "");
        }

        // for print basketUser( report).
        String[] userProducts = basketUser.toArray(new String[0]);
        modelMap.addAttribute("order", userProducts);

        BigDecimal total = shopService.getTotalCost(userProducts);
        modelMap.addAttribute("total", total);

        return "order";
    }
}
