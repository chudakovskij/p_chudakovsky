package com.pchudakovsky.controllers;

import com.pchudakovsky.exception.BadValidateException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * Controller of errors.
 *
 * @author Pavel Chudakovsky.
 */
@Controller
public class ErrorController {

    /**
     * @param model {@link ModelMap}
     * @return {@link BadValidateException}
     */
    @RequestMapping("/error")
    public String error(ModelMap model) {
        throw new BadValidateException();
    }

    /**
     * @param exception {@link BadValidateException}
     * @return {@link ModelAndView}
     */
    @ExceptionHandler(BadValidateException.class)
    public ModelAndView badValidate(BadValidateException exception) {
        ModelAndView model = new ModelAndView("start");
        model.addObject("error", exception.getMessage());
        return model;
    }

}
