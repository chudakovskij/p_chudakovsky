package com.pchudakovsky.controllers;

import com.pchudakovsky.service.ShopServiceImpl;
import com.pchudakovsky.service.api.ShopService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Controller for basket of User.
 *
 * @author Pavel Chudakovsky.
 */
@Controller
@SessionAttributes({"product"})
public class BasketController {

    /**
     * Logger.
     */
    private final static Logger LOGGER = LogManager.getLogger(ShopServiceImpl.class);

    /**
     * Service.
     */
    private final ShopService shopService;

    /**
     * Sole constructor.
     *
     * @param shopService {@link ShopService}
     */
    public BasketController(ShopService shopService) {
        this.shopService = shopService;
    }

    /**
     * Adds products to basket.
     *
     * @param product       from menu as string.
     * @param modelMap      {@link ModelMap}
     * @param basket        basket of user as {@link List}
     * @param listIdProduct id of products.
     * @return view as string.
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String add(@RequestParam("selectProducts") String product,
                      ModelMap modelMap,
                      @SessionAttribute("basket") List<String> basket,
                      @SessionAttribute("listIdProduct") List<Integer> listIdProduct) {

        LOGGER.info("Method add() for "
                + BasketController.class);

        if (product != null) {
            basket.add(product);
        }

        modelMap.addAttribute("product", product);

        String title = shopService.getNameProductFromSelect(product);
        Integer idProduct = shopService.getIdProductByTitle(title);
        listIdProduct.add(idProduct);  // / used to populate the link table

        return "menu";
    }
}
