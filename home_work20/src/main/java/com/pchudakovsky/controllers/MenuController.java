package com.pchudakovsky.controllers;

import com.pchudakovsky.entity.Product;
import com.pchudakovsky.entity.User;
import com.pchudakovsky.service.ShopServiceImpl;
import com.pchudakovsky.service.api.ShopService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for Menu of online-shop.
 *
 * @author Pavel Chudakovsky.
 */
@Controller
@SessionAttributes({"user", "userName", "userId",
        "ProductsInShop", "basket", "listIdProduct"})
public class MenuController {
    /**
     * Logger.
     */
    private final static Logger LOGGER = LogManager.getLogger(ShopServiceImpl.class);

    /**
     * Service.
     */
    private final ShopService shopService;

    /**
     * Sole Constructor.
     *
     * @param shopService service.
     */
    public MenuController(ShopService shopService) {
        this.shopService = shopService;
    }

    /**
     * Returns view for menu of Online-shop.
     *
     * @param req   {@link HttpServletRequest}
     * @param model {@link ModelMap}
     * @param user  {@link User}
     * @return view as string
     */
    @RequestMapping(value = "/menu", method = {RequestMethod.GET})
    public String menu(HttpServletRequest req,
                       ModelMap model,
                       @ModelAttribute("user") User user) {

        LOGGER.info("Controller " + MenuController.class + ": method menu()");

        String userName = req.getUserPrincipal().getName();
        Integer id = shopService.getIdUserByName(userName);
        if (userName.equals(shopService.getNameUser(userName))) {
            LOGGER.info("Setting session for name and Id of user");

            user.setName(userName);
            user.setId(id);
            model.addAttribute("userName", user.getName());
            model.addAttribute("userId", user.getId());

            LOGGER.info("Setting session for products of Online-shop");
            List<Product> products = shopService.getAllProducts();
            model.addAttribute("ProductsInShop", products);
        }
        return "menu";
    }

    /**
     * Returns User
     *
     * @return {@link User}
     */
    @ModelAttribute("user")
    public User getUser() {
        return new User();
    }

    /**
     * Returns basket of user.
     *
     * @return basket of user as {@link ArrayList}
     */
    @ModelAttribute("basket")
    public List<String> basket() {
        return new ArrayList<>();
    }

    /**
     * Returns of list of Id for products.
     *
     * @return {@link ArrayList}
     */
    @ModelAttribute("listIdProduct")
    public List<Integer> listIdProduct() {
        return new ArrayList<>();
    }

}
