CREATE TABLE Users (ID int NOT NULL AUTO_INCREMENT, Name VARCHAR(255), Password VARCHAR(255), PRIMARY KEY (ID));
insert into Users values (null,'admin','$2a$10$BoJ4mqy6sQ7mkS9PjwTtmOZlOf/qp3F1N.B1bwvop2I2bGLFP8vVq');
insert into Users values ( null,'Petrov', '$2a$10$xqncZfKCF1o0v01JBjR2Dusb3c5JpdXB8uYm8D04mXkmrh4zy.C1y');
insert into Users values ( null,'Sidorov', '$2a$10$KTBPZny8VlfqiV8MBnBsz.2eQcaaa/fZx.g8pTUngS3fF0.RP0FTa');
insert into Users values (  null,'Fedorov', '$2a$10$qqFJCPO1fL/8rFsRUxmYheHWEe4QfCfRXwQysQFAl3uqxQ0JhVfpm');
insert into Users values (  null,'Alexsandrov', '$2a$10$4mI08UtLQP8J7ciGVs39N.TSpAQkvJyfG.MWQLHstan03urObcoqe');
select * from Users;

CREATE TABLE Orders (ID int NOT NULL AUTO_INCREMENT, UserId VARCHAR(255), TotalPrice DECIMAL,
 PRIMARY KEY (ID),FOREIGN KEY (UserId) REFERENCES Users(ID) );


CREATE TABLE Products (ID int NOT NULL AUTO_INCREMENT, Title VARCHAR(255), Price DECIMAL,
 PRIMARY KEY (ID));

insert into Products values (null,'CarA',1400.45);
insert into Products values (null,'CarB',2400.45);
insert into Products values (null,'CarC',5500.45);
insert into Products values (null,'CarD',3400.45);
insert into Products values (null,'CarF',4400.45);
insert into Products values (null,'Ford',10400.45);
select * from Products;

CREATE TABLE Orders_Products (ID int NOT NULL AUTO_INCREMENT, OrderId INT,ProductId INT,
PRIMARY KEY (ID),FOREIGN KEY (OrderId) REFERENCES Orders(ID),
FOREIGN KEY (ProductId) REFERENCES Products(ID));
