package com.pchudakovsky.service;

import com.pchudakovsky.data.DataBaseStart;
import com.pchudakovsky.repository.DataBaseRepositoryImpl;
import com.pchudakovsky.repository.OrderRepositoryImpl;
import com.pchudakovsky.repository.api.OrderRepository;
import com.pchudakovsky.repository.api.ProductRepository;
import com.pchudakovsky.repository.api.UserRepository;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ShopServiceImplTest {

    private static DataBaseRepositoryImpl baseRepository = new DataBaseRepositoryImpl();

    @BeforeClass
    public static void init() throws ClassNotFoundException {
        DataBaseStart.createDatabase();
        OrderRepository order = new OrderRepositoryImpl();

        order.saveOrder(2, new BigDecimal("2000"), baseRepository);
        order.saveOrder(3, new BigDecimal("3000"), baseRepository);
    }

    @Mock
    private UserRepository userRepository;

    @Mock
    private ProductRepository productRepository;

    @Mock
    private OrderRepository orderRepository;

    @InjectMocks
    private ShopServiceImpl shopService;

    @Test
    public void testGetIdUserByName() {
        // given:
        when(userRepository.getIdUserByName("admin", baseRepository)).thenReturn(1);

        // when:
        Integer actualId = userRepository.getIdUserByName("admin", baseRepository);
        Integer expected = 1;

        //then 1:
        assertEquals(expected, actualId);

        //then 2:
        assertNotEquals(actualId, Integer.valueOf(0));
    }

    @Test
    public void testGetNameUser() {

        // given:
        when(userRepository.getNameUser("admin", baseRepository)).thenReturn("admin");

        // when:
        String actualName = userRepository.getNameUser("admin", baseRepository);

        //then 1:
        assertEquals("admin", actualName);

        //then 2:
        assertNotEquals("petrov", actualName);
    }

    @Test
    public void testGetNameProductFromSelect() {

        // given:
        when(productRepository.getNameProductFromSelect("Ford 20000 $ ")).thenReturn("Ford");

        // when:
        String actualName = productRepository.getNameProductFromSelect("Ford 20000 $ ");

        //then 1:
        assertEquals("Ford", actualName);

        //then 2:
        assertNotNull(actualName);

    }

    @Test
    public void testGetTotalCost() {

        // given:
        String[] product = {"Ford 20000 $ ", "Reno 18000 $ "};
        when(productRepository.getTotalCost(product)).thenReturn(new BigDecimal("38000"));

        // when:
        BigDecimal actualTotal = productRepository.getTotalCost(product);

        //then 1:
        assertEquals(new BigDecimal("38000"), actualTotal);

        //then 2:
        assertNotEquals(new BigDecimal("48000"), actualTotal);
    }

    @Test
    public void testGetIdOrdersByIdUser() {

        // given:
        when(orderRepository.getIdOrdersByIdUser(3, baseRepository)).thenReturn(2);

        // when:
        Integer actualIdOrder = orderRepository.getIdOrdersByIdUser(3, baseRepository);

        //then 1:
        assertEquals(Integer.valueOf(2), actualIdOrder);

        //then 2:
        assertNotEquals(Integer.valueOf(1), actualIdOrder);
    }
}