package com.pchudakovsky.service;

import com.pchudakovsky.data.DataBaseStart;
import com.pchudakovsky.repository.DataBaseRepositoryImpl;
import com.pchudakovsky.repository.OrderRepositoryImpl;
import com.pchudakovsky.repository.ProductRepositoryImpl;
import com.pchudakovsky.repository.UserRepositoryImpl;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ShopServiceImplTest {

    private static DataBaseRepositoryImpl baseRepository = new DataBaseRepositoryImpl();

    @BeforeClass
    public static void init() throws ClassNotFoundException {
        DataBaseStart.createDatabase();
        OrderRepositoryImpl order = new OrderRepositoryImpl();

        order.saveOrder(2, new BigDecimal("2000"), baseRepository);
        order.saveOrder(3, new BigDecimal("3000"), baseRepository);
    }

    @Mock
    UserRepositoryImpl userRepository;

    @Mock
    ProductRepositoryImpl productRepository;

    @Mock
    OrderRepositoryImpl orderRepository;

    @InjectMocks
    ShopServiceImpl shopServiceOne = new ShopServiceImpl(userRepository);

    @InjectMocks
    ShopServiceImpl shopServiceTwo = new ShopServiceImpl(productRepository);

    @InjectMocks
    ShopServiceImpl shopServiceThree = new ShopServiceImpl(orderRepository);


    @Test
    public void testGetIdUserByName() {
        // given:
        when(shopServiceOne.getIdUserByName("admin", baseRepository)).thenReturn(1);

        // when:
        int actualId = shopServiceOne.getIdUserByName("admin", baseRepository);
        int expected = 1;

        //then:
        assertEquals(expected, actualId);

        //then:
        assertNotEquals(actualId, 0);
    }

    @Test
    public void testGetNameUser() {

        // given:
        when(shopServiceOne.getNameUser("admin", baseRepository)).thenReturn("admin");

        // when:
        String actualName = shopServiceOne.getNameUser("admin", baseRepository);

        //then:
        assertEquals("admin", actualName);

        //then:
        assertNotEquals("petrov", actualName);
    }

    @Test
    public void testGetNameProductFromSelect() {

        // given:
        when(shopServiceTwo.getNameProductFromSelect("Ford 20000 $ ")).thenReturn("Ford");

        // when:
        String actualName = shopServiceTwo.getNameProductFromSelect("Ford 20000 $ ");

        //then1:
        assertEquals("Ford", actualName);

        //then2:
        assertNotNull(actualName);

    }

    @Test
    public void testGetTotalCost() {

        // given:
        String[] product = {"Ford 20000 $ ", "Reno 18000 $ "};
        when(shopServiceTwo.getTotalCost(product)).thenReturn(new BigDecimal("38000"));

        // when:
        BigDecimal actualTotal = shopServiceTwo.getTotalCost(product);

        //then1:
        assertEquals(new BigDecimal("38000"), actualTotal);

        //then2:
        assertNotEquals(new BigDecimal("48000"), actualTotal);
    }

    @Test
    public void testGetIdOrdersByIdUser() {

        // given:
        when(shopServiceThree.getIdOrdersByIdUser(3, baseRepository)).thenReturn(2);

        // when:
        int actualIdOrder = shopServiceThree.getIdOrdersByIdUser(3, baseRepository);

        //then1:
        assertEquals(2, actualIdOrder);

        //then2:
        assertNotEquals(1, actualIdOrder);
    }
}