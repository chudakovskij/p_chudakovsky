CREATE TABLE Users (ID int NOT NULL AUTO_INCREMENT, Name VARCHAR(255), Password VARCHAR(255), PRIMARY KEY (ID));
insert into Users values (null,'admin','');
insert into Users values ( null,'Petrov', '');
insert into Users values ( null,'Sidorov', '');
insert into Users values (  null,'Fedorov', '');
insert into Users values (  null,'Alexsandrov', '');
select * from Users;

CREATE TABLE Orders (ID int NOT NULL AUTO_INCREMENT, UserId VARCHAR(255), TotalPrice DECIMAL,
 PRIMARY KEY (ID),FOREIGN KEY (UserId) REFERENCES Users(ID) );


CREATE TABLE Products (ID int NOT NULL AUTO_INCREMENT, Title VARCHAR(255), Price DECIMAL,
 PRIMARY KEY (ID));

insert into Products values (null,'CarA',1400.45);
insert into Products values (null,'CarB',2400.45);
insert into Products values (null,'CarC',5500.45);
insert into Products values (null,'CarD',3400.45);
insert into Products values (null,'CarF',4400.45);
insert into Products values (null,'Ford',10400.45);
select * from Products;

CREATE TABLE Orders_Products (ID int NOT NULL AUTO_INCREMENT, OrderId INT,ProductId INT,
PRIMARY KEY (ID),FOREIGN KEY (OrderId) REFERENCES Orders(ID),
FOREIGN KEY (ProductId) REFERENCES Products(ID));
