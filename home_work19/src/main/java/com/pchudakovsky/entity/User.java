package com.pchudakovsky.entity;

/**
 * Describes of behavior for user.
 */
public class User {
    /**
     * Id for User.
     */
    private int id;
    /**
     * User name.
     */
    private String name;

    /**
     * the First constructor.
     *
     * @param name user name.
     */
    @SuppressWarnings("unused")
    public User(String name) {
        this.name = name;
    }

    /**
     * The second constructor.
     *
     * @param name user name.
     */
    public User(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    /**
     * Getter for name.
     *
     * @return user name.
     */
    @SuppressWarnings("unused")
    public String getName() {
        return name;
    }

    /**
     * Setter for name.
     *
     * @param name user name.
     */
    @SuppressWarnings("unused")
    public void setName(String name) {
        this.name = name;
    }


}
