package com.pchudakovsky.service;

import com.pchudakovsky.entity.Product;
import com.pchudakovsky.repository.DataBaseRepositoryImpl;
import com.pchudakovsky.repository.OrderRepositoryImpl;
import com.pchudakovsky.repository.ProductRepositoryImpl;
import com.pchudakovsky.repository.UserRepositoryImpl;
import com.pchudakovsky.repository.api.UserRepository;
import com.pchudakovsky.service.api.ShopService;

import java.math.BigDecimal;
import java.util.List;

/**
 * Provides methods for
 * the operation of the online store service.
 *
 * @author Pavel Chudakovsky.
 * @see OrderRepositoryImpl
 * @see UserRepositoryImpl
 * @see ProductRepositoryImpl
 */

public class ShopServiceImpl implements ShopService {

    /**
     * Repository for user.
     */
    private UserRepositoryImpl userRepository;

    /**
     * Repository for product.
     */
    private ProductRepositoryImpl productRepository;

    /**
     * Repository for Order.
     */
    private OrderRepositoryImpl orderRepository;

    /**
     * The First constructor.
     *
     * @param orderRepository {@link OrderRepositoryImpl}
     */
    public ShopServiceImpl(OrderRepositoryImpl orderRepository) {
        this.orderRepository = orderRepository;
    }

    /**
     * The second constructor.
     *
     * @param productRepository {@link ProductRepositoryImpl}
     */
    public ShopServiceImpl(ProductRepositoryImpl productRepository) {
        this.productRepository = productRepository;
    }


    /**
     * The third constructor.
     *
     * @param userRepository {@link UserRepositoryImpl}
     */
    public ShopServiceImpl(UserRepositoryImpl userRepository) {
        this.userRepository = userRepository;
    }


    /**
     * The fourth constructor.
     *
     * @param userRepository    {@link UserRepositoryImpl}
     * @param productRepository {@link ProductRepositoryImpl}
     */
    public ShopServiceImpl(UserRepositoryImpl userRepository, ProductRepositoryImpl productRepository) {
        this.userRepository = userRepository;
        this.productRepository = productRepository;
    }

    /**
     * @param userName       username.
     * @param baseRepository {@link DataBaseRepositoryImpl}
     * @return id of user.
     */
    public int getIdUserByName(String userName, DataBaseRepositoryImpl baseRepository) {
        return userRepository.getIdUserByName(userName, baseRepository);
    }

    /**
     * @param userName       username.
     * @param baseRepository {@link DataBaseRepositoryImpl}
     * @return username {@link UserRepository}
     */
    public String getNameUser(String userName, DataBaseRepositoryImpl baseRepository) {
        return userRepository.getNameUser(userName, baseRepository);
    }

    /**
     * @param baseRepository {@link DataBaseRepositoryImpl}
     * @return list of products.
     */
    public List<Product> getAllProducts(DataBaseRepositoryImpl baseRepository) {
        return productRepository.getAllProducts(baseRepository);
    }

    /**
     * @param product product from select.
     * @return name of product.
     */
    public String getNameProductFromSelect(String product) {
        return productRepository.getNameProductFromSelect(product);
    }

    /**
     * @param title          title (name) for product.
     * @param baseRepository {@link DataBaseRepositoryImpl}
     * @return id of product.
     */
    public int getIdProductByTitle(String title, DataBaseRepositoryImpl baseRepository) {
        return productRepository.getIdProductByTitle(title, baseRepository);
    }

    /**
     * @param userProducts products for user.
     * @return cost as BigDecimal.
     */
    public BigDecimal getTotalCost(String[] userProducts) {
        return productRepository.getTotalCost(userProducts);
    }

    /**
     * Saves orders(table Orders).
     *
     * @param userId         id  for user.
     * @param totalPrice     total price as BigDecimal.
     * @param baseRepository object of DataBaseServiceImpl.
     */
    public void saveOrder(int userId, BigDecimal totalPrice, DataBaseRepositoryImpl baseRepository) {
        orderRepository.saveOrder(userId, totalPrice, baseRepository);
    }

    /**
     * Returns all orders.
     *
     * @param baseRepository {@link DataBaseRepositoryImpl}
     */
    public void getAllOrders(DataBaseRepositoryImpl baseRepository) {
        orderRepository.getAllOrders(baseRepository);
    }

    /**
     * Id of order by id of user/
     *
     * @param userId         id for user.
     * @param baseRepository {@link DataBaseRepositoryImpl}
     * @return id order.
     */
    public int getIdOrdersByIdUser(int userId, DataBaseRepositoryImpl baseRepository) {
        return orderRepository.getIdOrdersByIdUser(userId, baseRepository);
    }


    /**
     * Saves table Orders_Products.
     *
     * @param orderId        id for order.
     * @param productId      id for product.
     * @param baseRepository {@link DataBaseRepositoryImpl}
     */
    public void saveOrdersProducts(int orderId, int productId, DataBaseRepositoryImpl baseRepository) {
        orderRepository.saveOrdersProducts(orderId, productId, baseRepository);
    }

    /**
     * Displays all the table fields in the console
     * (table Orders_Products).
     *
     * @param baseService {@link DataBaseRepositoryImpl}
     */
    public void getAllOrdersProducts(DataBaseRepositoryImpl baseService) {
        orderRepository.getAllOrdersProducts(baseService);
    }

    /**
     * @param userId         id for user.
     * @param baseRepository {@link DataBaseRepositoryImpl}
     * @return list for orders.
     */
    public List<String> joinOrdersProducts(int userId, DataBaseRepositoryImpl baseRepository) {
        return orderRepository.joinOrdersProducts(userId, baseRepository);
    }
}


