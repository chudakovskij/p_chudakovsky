package com.pchudakovsky.service.api;

import com.pchudakovsky.entity.Product;
import com.pchudakovsky.repository.DataBaseRepositoryImpl;

import java.math.BigDecimal;
import java.util.List;

/**
 * Provides methods for
 * the operation of the online store service.
 *
 * @author Pavel Chudakovsky.
 * @see com.pchudakovsky.repository.api.OrderRepository
 * @see com.pchudakovsky.repository.api.UserRepository
 * @see com.pchudakovsky.repository.api.ProductRepository
 */
public interface ShopService {

    int getIdUserByName(String userName, DataBaseRepositoryImpl baseService);

    String getNameUser(String userName, DataBaseRepositoryImpl baseService);

    List<Product> getAllProducts(DataBaseRepositoryImpl baseService);

    String getNameProductFromSelect(String product);

    int getIdProductByTitle(String title, DataBaseRepositoryImpl baseService);

    BigDecimal getTotalCost(String[] userProducts);

    void saveOrder(int userId, BigDecimal totalPrice, DataBaseRepositoryImpl baseService);

    void getAllOrders(DataBaseRepositoryImpl baseService);

    int getIdOrdersByIdUser(int userId, DataBaseRepositoryImpl baseService);

    void saveOrdersProducts(int orderId, int productId, DataBaseRepositoryImpl baseService);

    void getAllOrdersProducts(DataBaseRepositoryImpl baseService);

    List<String> joinOrdersProducts(int userId, DataBaseRepositoryImpl baseService);


}

