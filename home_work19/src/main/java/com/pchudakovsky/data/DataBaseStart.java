package com.pchudakovsky.data;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

import org.apache.ibatis.jdbc.ScriptRunner;

/**
 * Provides a method for reading a script of SQL.
 *
 * @author Pavel Chudakovsky.
 */
public class DataBaseStart {

    /**
     * Path for DB SQL
     */
    private final static String PATH_DB_SQL = "src/main/resources/db_online_shop.sql";

    /**
     * Boolean flag (true if data
     * base is created).
     */
    private static boolean flag = false;

    /**
     * Returns true if data base is created.
     *
     * @return variable of boolean type.
     */
    public static boolean isFlag() {
        return flag;
    }

    /**
     * Creates a database.
     *
     * @throws ClassNotFoundException when the class is not detected.
     */
    public static void createDatabase() throws ClassNotFoundException {
        try (Reader reader = new BufferedReader(new FileReader(PATH_DB_SQL))) {
            new ScriptRunner(ConnectionH2.checkConnection()).runScript(reader);
            DataBaseStart.flag = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
