package com.pchudakovsky.repository.api;

import com.pchudakovsky.repository.DataBaseRepositoryImpl;

import java.math.BigDecimal;
import java.util.List;

/**
 * Provides of methods for OrderRepositoryImpl.
 *
 * @author Pavel Chudakovsky.
 */
public interface OrderRepository {
    /**
     * Saves orders.
     *
     * @param userId         id  for user.
     * @param totalPrice     total price as BigDecimal.
     * @param baseRepository object of DataBaseServiceImpl.
     */
    void saveOrder(int userId, BigDecimal totalPrice, DataBaseRepositoryImpl baseRepository);

    /**
     * Saves orders and product
     * (table Orders_Products).
     *
     * @param orderId        id for order.
     * @param productId      id for product.
     * @param baseRepository object of DataBaseRepositoryImpl.
     */
    void saveOrdersProducts(int orderId, int productId, DataBaseRepositoryImpl baseRepository);

    /**
     * Returns Id of order for user.
     *
     * @param idUser         id for user.
     * @param baseRepository object of  DataBaseRepositoryImpl.
     * @return id for order.
     */
    int getIdOrdersByIdUser(int idUser, DataBaseRepositoryImpl baseRepository);

    /**
     * Displays all the table fields in the console
     * (table Orders).
     *
     * @param baseRepository object of  DataBaseRepositoryImpl.
     */
    void getAllOrders(DataBaseRepositoryImpl baseRepository);

    /**
     * Displays all the table fields in the console
     * (table Orders_Products).
     *
     * @param baseRepository object of  DataBaseRepositoryImpl.
     */
    void getAllOrdersProducts(DataBaseRepositoryImpl baseRepository);

    /**
     * Returns list of orders.
     *
     * @param userId         id for user.
     * @param baseRepository object of  DataBaseRepositoryImpl.
     * @return list for orders.
     */
    List<String> joinOrdersProducts(int userId, DataBaseRepositoryImpl baseRepository);
}
