package com.pchudakovsky.repository.api;

import com.pchudakovsky.repository.DataBaseRepositoryImpl;

/**
 * Provides of methods for UserRepositoryImpl.
 *
 * @author Pavel Chudakovsky.
 */
public interface UserRepository {

    /**
     * Returns name of user from data base.
     *
     * @param name           name for user.
     * @param baseRepository object fo type DataBaseServiceImpl.
     * @return name of user from data base.
     */
    String getNameUser(String name, DataBaseRepositoryImpl baseRepository);

    /**
     * Id user by username.
     *
     * @param loginUser   login (name) of  user.
     * @param baseRepository object fo type DataBaseServiceImpl.
     * @return id user by username.
     */
    int getIdUserByName(String loginUser, DataBaseRepositoryImpl baseRepository);
}
