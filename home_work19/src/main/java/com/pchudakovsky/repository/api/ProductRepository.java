package com.pchudakovsky.repository.api;

import com.pchudakovsky.entity.Product;
import com.pchudakovsky.repository.DataBaseRepositoryImpl;

import java.math.BigDecimal;
import java.util.List;

/**
 * Provides of methods for ProductRepositoryImpl.
 *
 * @author Pavel Chudakovsky.
 */
public interface ProductRepository {

    /**
     * Returns title for product.
     *
     * @param products string from select(menu)
     * @return name(title) for product.
     */
    String getNameProductFromSelect(String products);

    /**
     * Returns total price of products.
     *
     * @param products information about products as array of string.
     * @return total price as BigDecimal.
     */
    BigDecimal getTotalCost(String[] products);

    /**
     * Returns Id of product by title product.
     *
     * @param title          title of product.
     * @param baseRepository object of type  DataBaseRepositoryImpl.
     * @return id for product.
     */
    int getIdProductByTitle(String title, DataBaseRepositoryImpl baseRepository);

    /**
     * Returns list of products from table Products
     *
     * @param baseRepository object of type DataBaseRepositoryImpl.
     * @return list of products for menu(select)
     */
    List<Product> getAllProducts(DataBaseRepositoryImpl baseRepository);
}
