package com.pchudakovsky.repository;

import com.pchudakovsky.repository.api.OrderRepository;
import com.pchudakovsky.service.ShopServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents order repository behavior.
 *
 * @author Pavel Chudakovsky.
 */
public class OrderRepositoryImpl implements OrderRepository {

    // used to create queries
    private static final String ALL_ORDERS = "SELECT * FROM Orders";
    private static final String ALL_ORDERS_PRODUCTS = "SELECT * FROM Orders_Products";
    private static final String ID_ORDER_BY_ID_USER = "SELECT * FROM Orders WHERE UserId=";
    private static final String INSERT_ORDER = "INSERT INTO Orders(Id,UserId,totalPrice) VALUES (null,?,?)";
    private static final String INSERT_ORDERS_PRODUCTS = "INSERT INTO Orders_Products(Id,orderId,productId) VALUES(null,?,?)";

    /**
     * Logger.
     */
    private final static Logger LOGGER = LogManager.getLogger(ShopServiceImpl.class);

    /**
     * Saves orders(table Orders).
     *
     * @param userId         id  for user.
     * @param totalPrice     total price as BigDecimal.
     * @param baseRepository object of  DataBaseRepositoryImpl.
     */

    public void saveOrder(int userId, BigDecimal totalPrice, DataBaseRepositoryImpl baseRepository) {
        Connection connection = baseRepository.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(INSERT_ORDER);
            preparedStatement.setInt(1, userId);
            preparedStatement.setBigDecimal(2, totalPrice);
            int result = preparedStatement.executeUpdate();
            LOGGER.info("Order saved... " + "ResultSet=" + result);
            preparedStatement.close();
        } catch (SQLException e) {
            LOGGER.error("Order not saved", e);
        }
    }

    /**
     * Saves orders and product
     * (table Orders_Products).
     *
     * @param orderId        id for order.
     * @param productId      id for product.
     * @param baseRepository object of  DataBaseRepositoryImpl.
     */
    public void saveOrdersProducts(int orderId, int productId, DataBaseRepositoryImpl baseRepository) {
        try {
            Connection connection = baseRepository.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(INSERT_ORDERS_PRODUCTS);
            preparedStatement.setInt(1, orderId);
            preparedStatement.setInt(2, productId);
            int result = preparedStatement.executeUpdate();
            if (result > 0) {
                LOGGER.info("Order saved... " + "ResultSet=" + result);
            } else {
                LOGGER.info("Order not saved... " + "ResultSet=" + result);
            }

            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns Id of order for user.
     *
     * @param idUser         id for user.
     * @param baseRepository object of  DataBaseRepositoryImpl.
     * @return id for order.
     */
    public int getIdOrdersByIdUser(int idUser, DataBaseRepositoryImpl baseRepository) {
        int orderId = 0;
        try {
            Connection connection = baseRepository.getConnection();
            Statement statement = connection.createStatement();
            ResultSet result = statement.executeQuery(ID_ORDER_BY_ID_USER + "'" + idUser + "'");
            while (result.next()) {
                orderId = result.getInt("id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return orderId;
    }

    /**
     * Displays all the table fields in the console
     * (table Orders).
     *
     * @param baseRepository object of  DataBaseRepositoryImpl.
     */
    public void getAllOrders(DataBaseRepositoryImpl baseRepository) {
        Connection connection = baseRepository.getConnection();
        try {
            Statement statement = connection.createStatement();
            ResultSet result = statement.executeQuery(ALL_ORDERS);
            while (result.next()) {
                int id = result.getInt("Id");
                String userId = result.getString("userId");
                BigDecimal total = result.getBigDecimal("totalPrice");
                System.out.print("OrdersId: " + id + " UserId: " + userId + " totalPrice: " + total);
            }

            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Displays all the table fields in the console
     * (table Orders_Products).
     *
     * @param baseRepository object of  DataBaseRepositoryImpl.
     */
    public void getAllOrdersProducts(DataBaseRepositoryImpl baseRepository) {
        Connection connection = baseRepository.getConnection();
        try {
            Statement statement = connection.createStatement();
            ResultSet result = statement.executeQuery(ALL_ORDERS_PRODUCTS);
            while (result.next()) {
                int id = result.getInt("Id");
                int orderId = result.getInt("orderId");
                int productId = result.getInt("productId");
                System.out.print("Id: " + id + " OrdersId: " + orderId + " products id: " + productId);
            }
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns list of orders.
     *
     * @param userId         id for user.
     * @param baseRepository object of  DataBaseRepositoryImpl.
     * @return list for orders.
     */
    public List<String> joinOrdersProducts(int userId, DataBaseRepositoryImpl baseRepository) {
        List<String> listOnDisplay = new ArrayList<>();

        String sdl = "SELECT Orders.id,Orders.UserId, Products.title,Products.price" +
                " FROM Orders,Products,Orders_Products" +
                " WHERE Orders.id=Orders_Products.orderId and " +
                " Products.id=Orders_Products.productId" +
                " and Orders.UserId=" + "'" + userId + "'";
        Connection connection = baseRepository.getConnection();
        try {
            Statement statement = connection.createStatement();
            ResultSet result = statement.executeQuery(sdl);
            while (result.next()) {
                int orderId = result.getInt("Orders.Id");
                int idUser = result.getInt("Orders.userId");
                String title = result.getString("Products.title");
                BigDecimal price = result.getBigDecimal("Products.price");
                String orderDisplay = " Order Id: " + orderId + " user Id: " + idUser
                        + " title: " + title + " price :" + price + " $";
                listOnDisplay.add(orderDisplay);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listOnDisplay;
    }
}
