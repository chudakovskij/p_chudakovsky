package com.pchudakovsky.repository;

import com.pchudakovsky.repository.api.UserRepository;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Provides methods for
 * for repository of user.
 *
 * @author Pavel Chudakovsky.
 */
public class UserRepositoryImpl implements UserRepository {

    // used to create queries
    private static final String ID_USER_BY_NAME = "SELECT * FROM Users WHERE name=";

    /**
     * Returns name of user from data base.
     *
     * @param name           name for user.
     * @param baseRepository object fo type  DataBaseRepositoryImpl.
     * @return name of user from data base.
     */
    public String getNameUser(String name, DataBaseRepositoryImpl baseRepository) {
        String nameUser = null;
        Connection connection = baseRepository.getConnection();
        try {
            Statement statement = connection.createStatement();
            ResultSet result = statement.executeQuery("SELECT * FROM Users " +
                    " WHERE name=" + "'" + name + "'");
            while (result.next()) {
                nameUser = result.getString("Name");
            }
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return nameUser;
    }

    /**
     * Id user by username.
     *
     * @param loginUser      login (name) of  user.
     * @param baseRepository object fo type  DataBaseRepositoryImpl.
     * @return id user by username.
     */
    public int getIdUserByName(String loginUser, DataBaseRepositoryImpl baseRepository) {
        int idUser = 0;
        Connection connection = baseRepository.getConnection();
        try {
            Statement statement = connection.createStatement();
            ResultSet result = statement.executeQuery(ID_USER_BY_NAME + "'" + loginUser + "'");
            while (result.next()) {
                idUser = result.getInt("id");
            }
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return idUser;
    }
}
