package com.pchudakovsky.repository;

import com.pchudakovsky.data.ConnectionH2;
import com.pchudakovsky.repository.api.DataBaseRepository;

import java.sql.Connection;

/**
 * DataBaseService provides a
 * method to get a connection.
 *
 * @author Pavel Chudakovsky.
 */
public class DataBaseRepositoryImpl implements DataBaseRepository {

    /**
     * Returns connection.
     *
     * @return variable of type Connection.
     */
    public Connection getConnection() {
        ConnectionH2 connectionH2 = new ConnectionH2();
        return connectionH2.getConnection();
    }
}
