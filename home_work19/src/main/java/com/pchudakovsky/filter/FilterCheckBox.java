package com.pchudakovsky.filter;

import com.pchudakovsky.service.ShopServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Filter on checkbox.
 *
 * @author Pavel Chudakovsky.
 */
public class FilterCheckBox implements Filter {

    private final static Logger logger = LogManager.getLogger(ShopServiceImpl.class);

    /**
     * Method initializes filter.
     *
     * @param filterConfig {@link FilterConfig}
     */
    @Override
    public void init(FilterConfig filterConfig) {
        logger.info("Method init() for "
                + FilterCheckBox.class + " is started!");

    }

    /**
     * @param request  type of{@link ServletRequest}.
     * @param response type of{@link ServletResponse}.
     * @param chain    type of{@link FilterChain}.
     * @throws IOException      if an input or output exception occurred.
     * @throws ServletException {@link ServletException}.
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        logger.info("Method doFilter() for "
                + FilterCheckBox.class + " is started!");

        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;

        String checkBox = req.getParameter("check");

        if (checkBox == null) {
            req.getRequestDispatcher("/redirect").forward(req, resp);
        } else {
            req.getSession().setAttribute("checkOn", checkBox);
            chain.doFilter(request, response);
        }
    }

    /**
     * @see Filter#destroy()
     */
    @Override
    public void destroy() {
        logger.info("Method destroy() for "
                + FilterCheckBox.class + " is started!");
    }
}
