package com.pchudakovsky.servlets;

import com.pchudakovsky.data.DataBaseStart;
import com.pchudakovsky.entity.Product;
import com.pchudakovsky.entity.User;
import com.pchudakovsky.repository.DataBaseRepositoryImpl;
import com.pchudakovsky.service.ShopServiceImpl;
import com.pchudakovsky.utility.ValidatorName;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * Servlet for greeting the user
 * and displaying the product menu.
 *
 * @author Pavel Chudakovsky.
 */
public class MenuServlet extends HttpServlet {

    /**
     * Object of DataBaseServiceImpl.
     */
    private DataBaseRepositoryImpl baseRepository;

    /**
     * Object of ShopServiceImpl.
     */
    private ShopServiceImpl shopService;

    /**
     * Logger.
     */
    private final static Logger LOGGER = LogManager.getLogger(ShopServiceImpl.class);

    /**
     * Method initializes servlet.
     */
    @Override
    public void init() {
        LOGGER.info("Method init() for "
                + MenuServlet.class + " is started!");

        LOGGER.info("Initialization of bean: \"baseRepository\" ");
        ApplicationContext context = new ClassPathXmlApplicationContext("beansShopService.xml");
        baseRepository = (DataBaseRepositoryImpl) context.getBean("baseRepository");

        LOGGER.info("Initialization of  bean: \"shopServiceForMenu\"");
        shopService = (ShopServiceImpl) context.getBean("shopServiceForMenu");
    }

    /**
     * Post-request.
     *
     * @param req  variable of type {@link HttpServletRequest}.
     * @param resp variable of type {@link HttpServletResponse}.
     * @throws IOException if an input or output exception occurred.
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException,
            ServletException {

        LOGGER.info("Method doPost() for "
                + MenuServlet.class + " is started!");

        if (!DataBaseStart.isFlag()) {
            try {
                DataBaseStart.createDatabase();
                LOGGER.info("DataBase is created");
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
                LOGGER.error("DataBase is not created", e);
            }
        }

        HttpSession session = req.getSession();
        String userName = req.getParameter("userName");

        int id = shopService.getIdUserByName(userName, baseRepository);
        session.setAttribute("userName", userName);
        session.setAttribute("userId", id);

        if (ValidatorName.checkName(userName)) {

            if (userName.equals(shopService.getNameUser(userName, baseRepository))) {
                LOGGER.info("Setting session for username");
                User user = new User(id, userName);
                session.setAttribute("user", user);
                List<Product> products = shopService.getAllProducts(baseRepository);
                session.setAttribute("shopInProducts", products);
                RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/view/menu.jsp");
                dispatcher.forward(req, resp);
            } else {
                LOGGER.info("User not registered.");
                RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/view/nameBad.jsp");
                dispatcher.forward(req, resp);
            }

        } else {
            LOGGER.info("Name user is not correct.");
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/view/nameBad.jsp");
            dispatcher.forward(req, resp);
        }
    }

    /**
     * Called by the servlet container to indicate
     * to a servlet that the servlet is being
     * taken out of shopService.
     */
    @Override
    public void destroy() {
        LOGGER.info("Method destroy() for "
                + MenuServlet.class + " is started!");
    }
}
