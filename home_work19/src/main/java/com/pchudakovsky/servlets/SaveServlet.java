package com.pchudakovsky.servlets;

import com.pchudakovsky.entity.User;
import com.pchudakovsky.repository.DataBaseRepositoryImpl;
import com.pchudakovsky.service.ShopServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Saves and reads data from a database.
 */
public class SaveServlet extends HttpServlet {

    /**
     * Object of DataBaseServiceImpl.
     */
    private DataBaseRepositoryImpl baseRepository;


    /**
     * Object of ShopServiceImpl.
     */
    private ShopServiceImpl shopService;

    /**
     * Logger.
     */
    private final static Logger LOGGER = LogManager
            .getLogger(ShopServiceImpl.class);

    /**
     * Method initializes servlet.
     */
    @Override
    public void init() {
        LOGGER.info("Method init() for "
                + SaveServlet.class + " is started!");

        LOGGER.info("Initialization of bean: \"baseRepository\" ");
        ApplicationContext context = new ClassPathXmlApplicationContext("beansShopService.xml");
        baseRepository = (DataBaseRepositoryImpl) context.getBean("baseRepository");

        LOGGER.info("Initialization of  bean: \"shopService_Order\"");
        shopService = (ShopServiceImpl) context.getBean("shopService_Order");
    }

    /**
     * Post-request.
     *
     * @param req  variable of type {@link HttpServletRequest}.
     * @param resp variable of type {@link HttpServletResponse}.
     * @throws IOException if an input or output exception occurred.
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        LOGGER.info("Method doPost() for "
                + SaveServlet.class + " is started!");

        HttpSession session = req.getSession();
        User user = (User) session.getAttribute("user");

        BigDecimal total = (BigDecimal) session.getAttribute("total");

        session.getAttribute("order");

        // save the order to the Orders table
        shopService.saveOrder(user.getId(), total, baseRepository);
        shopService.getAllOrders(baseRepository);

        int idOrders = shopService.getIdOrdersByIdUser(user.getId(), baseRepository);

        // save the order to the OrdersProducts table
        //noinspection unchecked
        List<Integer> listIdProduct = (List<Integer>) session.getAttribute("listIdProduct");

        if (listIdProduct == null) {
            listIdProduct = new ArrayList<>();
            session.setAttribute("listIdProduct", listIdProduct);
        }
        if (!listIdProduct.isEmpty()) {
            session.setAttribute("saveMessage", "Order saved...");
            for (Integer id : listIdProduct) {
                shopService.saveOrdersProducts(idOrders, id, baseRepository);
            }
            // for information on the console
            shopService.getAllOrdersProducts(baseRepository);

        } else {
            session.setAttribute("saveMessage", "Order not saved...");
        }

        List<String> onDisplay = shopService.joinOrdersProducts(user.getId(), baseRepository);
        session.setAttribute("orderOnDisplay", onDisplay);

        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/view/orderSave.jsp");
        dispatcher.forward(req, resp);
    }


    /**
     * Called by the servlet container to indicate
     * to a servlet that the servlet is being
     * taken out of service.
     */
    @Override
    public void destroy() {
        LOGGER.info("Method destroy() for "
                + SaveServlet.class + " is started!");
    }
}
