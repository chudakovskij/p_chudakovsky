package com.pchudakovsky.servlets;

import com.pchudakovsky.repository.DataBaseRepositoryImpl;
import com.pchudakovsky.service.ShopServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Servlet to add products to basket.
 *
 * @author Pavel Chudakovsky.
 */
public class AddServlet extends HttpServlet {

    /**
     * Object of DataBaseRepositoryImpl.
     */
    private DataBaseRepositoryImpl baseRepository;

    /**
     * Object of ShopServiceImpl.
     */
    private ShopServiceImpl shopService;

    /**
     * Logger.
     */
    private final static Logger LOGGER = LogManager
            .getLogger(ShopServiceImpl.class);


    /**
     * Method initializes servlet.
     */
    @Override
    public void init() {
        LOGGER.info("Method init() for "
                + AddServlet.class + " is started!");

        LOGGER.info("Initialization of bean: \"baseRepository\" ");
        ApplicationContext context = new ClassPathXmlApplicationContext("beansShopService.xml");
        baseRepository = (DataBaseRepositoryImpl) context.getBean("baseRepository");

        LOGGER.info("Initialization of  bean: \"shopService_Product\"");
        shopService = (ShopServiceImpl) context.getBean("shopService_Product");

    }

    /**
     * Post-request.
     *
     * @param req  variable of type {@link HttpServletRequest}.
     * @param resp variable of type {@link HttpServletResponse}.
     * @throws IOException if an input or output exception occurred.
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {

        LOGGER.info("Method doPost() for "
                + AddServlet.class + " is started!");

        String product = req.getParameter("selectProducts");
        HttpSession session = req.getSession();

        List basket;
        basket = (List) session.getAttribute("basket");

        if (basket == null) {
            basket = new ArrayList<>();
            session.setAttribute("basket", basket);
        }

        if (product != null) {
            //noinspection unchecked
            basket.add(product);
            session.setAttribute("basket", basket);
        }
        session.setAttribute("product", product);


        //noinspection unchecked
        List<Integer> listIdProduct = (List<Integer>) session.getAttribute("listIdProduct");

        if (listIdProduct == null) {
            listIdProduct = new ArrayList<>();
            session.setAttribute("listIdProduct", listIdProduct);
        }

        String title = shopService.getNameProductFromSelect(product);
        int idProduct = shopService.getIdProductByTitle(title, baseRepository);
        listIdProduct.add(idProduct);  // / used to populate the link table
        session.setAttribute("listIdProduct", listIdProduct);

        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/view/menu.jsp");
        dispatcher.include(req, resp);
    }

    /**
     * Called by the servlet container to indicate
     * to a servlet that the servlet is being
     * taken out of service.
     */
    @Override
    public void destroy() {
        LOGGER.info("Method destroy() for "
                + AddServlet.class + " is started!");
    }
}
