package com.pchudakovsky.servlets;

import com.pchudakovsky.service.ShopServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Servlet to exit the online shop.
 *
 * @author Pavel Chudakovsky.
 */
public class LogOut extends HttpServlet {
    /**
     * Logger.
     */
    private final static Logger LOGGER = LogManager.getLogger(ShopServiceImpl.class);

    /**
     * Method initializes servlet.
     */
    @Override
    public void init() {

        LOGGER.info("Method init() for "
                + LogOut.class + " is started!");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        LOGGER.info("Method doPost() for "
                + LogOut.class + " is started!");

        HttpSession session = req.getSession();
        List report = (List) session.getAttribute("basket");
        List products = (List) session.getAttribute("shopInProducts");
        //noinspection unchecked
        List listIdProducts = (List<Integer>) session.getAttribute("listIdProduct");

        if (report == null) {
            report = new ArrayList();
            session.setAttribute("basket", report);
        }

        if (products == null) {
            products = new ArrayList();
            session.setAttribute("shopInProducts", products);
        }

        if (listIdProducts == null) {
            listIdProducts = new ArrayList();
            session.setAttribute("listIdProduct", listIdProducts);
        }

        listIdProducts.clear();
        products.clear();
        report.clear();
        session.invalidate();

        LOGGER.info("Session  is invalidate!");
        resp.sendRedirect("/online-shop");
    }

    /**
     * Called by the servlet container to indicate
     * to a servlet that the servlet is being
     * taken out of service.
     */
    @Override
    public void destroy() {
        LOGGER.info("Method destroy() for "
                + LogOut.class + " is started!");
    }
}
