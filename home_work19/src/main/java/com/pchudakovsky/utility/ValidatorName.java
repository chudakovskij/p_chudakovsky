package com.pchudakovsky.utility;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class validator.
 *
 * @author Pavel Chudakovsky.
 */
public class ValidatorName {

    /**
     * Regular expression for username.
     */
    private static final String USER_NAME_PATTERN = "^[a-zA-Z0-9_-]{3,15}$";

    /**
     * Returns a variable of boolean type
     * when validating a username.
     *
     * @param userName as string.
     * @return true when name is correct.
     */
    public static boolean checkName(String userName) {
        boolean flag = false;
        Pattern pattern = Pattern.compile(USER_NAME_PATTERN);
        Matcher matcher = pattern.matcher(userName);
        boolean goodName = matcher.matches();
        if (goodName) {
            flag = true;
        }
        return flag;
    }

    /**
     * Checks nameuser on null.
     *
     * @param userName as string.
     * @return true if username is null.
     */
    static boolean nameIsNull(String userName) {
        boolean flag = true;
        if (userName != null) {
            flag = false;
        }
        return flag;

    }
}
