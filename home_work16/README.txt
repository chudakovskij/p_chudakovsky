
To run the application:

1.plugin -> jetty:run
2. for Start page:
    1)Enter address line:  localhost:8080/start (for start page of Online shop)
    2)Enter username and click button "Enter".
    ( will be redirected ->(localhost:8080/menu (for menu  page)))

3. for Menu page:
   Make a product selection and press the buttons:
   button
  "Add item":
  (will be redirected->(http://localhost:8080/add )

  button
  "Submit":
   will be redirected-> (localhost:8080/report (for Report page))

4. for Report page:
 Click button "LogOut" to out online shop.
(will be redirected-> localhost:8080/start (for Start page)).

!Warning: use post requests (through forms and application buttons).
!INFO: URL for servlets:
 /start StartServlet
 /menu MenuServlet
 /report ReportServlet
 /logOut LogOut
 /redirect RedirectServlet
 /add AddServlet
