package com.pchudakovsky.service;

import com.pchudakovsky.entity.Product;

import java.io.PrintWriter;
import java.util.List;

/**
 * Provides methods for
 * presenting results in a browser.
 */
public interface ServicePage {
    /**
     * Prints report for User.
     *
     * @param products information about products as array of string.
     * @param userName user of name as type of String.
     * @param out      object of {@link PrintWriter}.
     */
    void printReport(String[] products,
                     String userName, PrintWriter out);

    /**
     * Provides a product menu.
     *
     * @param products list products as  type of List.
     * @param userName user of name as type of String.
     * @param out      object of {@link PrintWriter}.
     */
    void formMenu(List<Product> products,
                  String userName, PrintWriter out);

    /**
     * Provides a Web-form for start page.
     *
     * @param out object of {@link PrintWriter}.
     */
    void formStart(PrintWriter out);

    /**
     * Prints information on a page when
     * name of user is not valid.
     *
     * @param out object of {@link PrintWriter}.
     */
    void infoBadName(PrintWriter out);

    /**
     * Prints information on a page when
     * list of  products is empty.
     *
     * @param out object of {@link PrintWriter}.
     */
    void listProductsIsEmpty(PrintWriter out);
}

