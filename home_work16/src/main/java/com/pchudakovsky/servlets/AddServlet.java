package com.pchudakovsky.servlets;

import com.pchudakovsky.entity.Product;
import com.pchudakovsky.service.Service;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Servlet to add products to basket.
 *
 * @author Pavel Chudakovsky.
 */
public class AddServlet extends HttpServlet {
    private Service service;

    /**
     * Logger.
     */
    private final static Logger logger = LogManager.getLogger(Service.class);

    /**
     * Method initializes servlet.
     */
    @Override
    public void init() {
        logger.info("Method init() for "
                + AddServlet.class + " is started!");

        logger.info("Initialization of object for Service");
        service = new Service();
    }

    /**
     * Post-request.
     *
     * @param req  variable of type {@link HttpServletRequest}.
     * @param resp variable of type {@link HttpServletResponse}.
     * @throws IOException if an input or output exception occurred.
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        logger.info("Method doPost() for "
                + AddServlet.class + " is started!");

        String product = req.getParameter("products");
        HttpSession session = req.getSession();

        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();

        List basket;
        basket = (List) session.getAttribute("basket");

        if (basket == null) {
            basket = new ArrayList<>();
            session.setAttribute("basket", basket);
        }

        if (product != null) {
            //noinspection unchecked
            basket.add(product);
            session.setAttribute("basket", basket);
        }

        session.setAttribute("product", product);

        out.print("<h3> User's Basket:</h3>");
        out.print("<select  multiple=true size=\"10\">>");
        for (Object pr : basket) {
            out.print("<option>" + pr + "</option>");
        }
        out.print("</select>");

        String userName = (String) session.getAttribute("userName");
        service.formMenu(Product.listProduct, userName, out);
    }

    /**
     * Called by the servlet container to indicate
     * to a servlet that the servlet is being
     * taken out of service.
     */
    @Override
    public void destroy() {
        logger.info("Method destroy() for "
                + AddServlet.class + " is started!");
    }
}
