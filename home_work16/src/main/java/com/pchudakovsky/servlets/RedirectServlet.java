package com.pchudakovsky.servlets;

import com.pchudakovsky.service.Service;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Servlet to redirect to message page
 *
 * @author Pavel Chudakovsky.
 */
public class RedirectServlet extends HttpServlet {
    /**
     * Logger.
     */
    private final static Logger logger = LogManager.getLogger(Service.class);

    /**
     * Method initializes servlet.
     */
    @Override
    public void init() {

        logger.info("Method init() for "
                + RedirectServlet.class + " is started!");
    }

    /**
     * Post-request.
     *
     * @param req  variable of type {@link HttpServletRequest}.
     * @param resp variable of type {@link HttpServletResponse}.
     * @throws IOException if an input or output exception occurred.
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        logger.info("Method doPost() for "
                + RedirectServlet.class + " is started!");

        PrintWriter out = resp.getWriter();
        out.print("<p>Oops! You shouldn't be here.</p>");
        out.print("<p>Please, agree with "
                + "the terms of service first</p>");
        out.print(Service.LINK_ONLINE_SHOP);
    }

    /**
     * Called by the servlet container to indicate
     * to a servlet that the servlet is being
     * taken out of service.
     */
    @Override
    public void destroy() {
        logger.info("Method destroy() for "
                + RedirectServlet.class + " is started!");
    }
}
