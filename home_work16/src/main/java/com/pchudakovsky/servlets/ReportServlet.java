package com.pchudakovsky.servlets;

import com.pchudakovsky.service.Service;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Servlet to print a purchase report.
 *
 * @author Pavel Chudakovsky.
 */
public class ReportServlet extends HttpServlet {
    /**
     * Object of Service.
     */
    private Service service;

    /**
     * Logger.
     */
    private final static Logger logger = LogManager
            .getLogger(Service.class);

    /**
     * Method initializes servlet.
     */
    @Override
    public void init() {
        logger.info("Method init() for "
                + ReportServlet.class + " is started!");

        logger.info("Initialization of object for Service");
        service = new Service();
    }

    /**
     * Post-request.
     *
     * @param req  variable of type {@link HttpServletRequest}.
     * @param resp variable of type {@link HttpServletResponse}.
     * @throws IOException if an input or output exception occurred.
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        logger.info("Method doPost() for "
                + ReportServlet.class + " is started!");

        resp.setContentType("text/html");
        HttpSession session = req.getSession();

        PrintWriter out = resp.getWriter();

        String userName = (String) session.getAttribute("userName");
        List basketUser = (List) session.getAttribute("basket");

        if (basketUser == null) {
            basketUser = new ArrayList();
            session.setAttribute("basket", basketUser);
        }

        if (basketUser.isEmpty()) {
            service.listProductsIsEmpty(out);
        }

        // for print basketUser( report).
        String[] userProducts = (String[]) basketUser.toArray(new String[0]);
        service.printReport(userProducts, userName, out);
        out.print("<form action=\"/logOut\" method=\"post\">\n" +
                "    <button type=\"submit\"> LogOut</button>\n" +
                "</form>");
    }

    /**
     * Called by the servlet container to indicate
     * to a servlet that the servlet is being
     * taken out of service.
     */
    @Override
    public void destroy() {
        logger.info("Method destroy() for "
                + ReportServlet.class + " is started!");
    }
}
