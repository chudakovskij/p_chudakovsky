package com.pchudakovsky.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Describes the product for Online shop.
 */
public class Product {

    /**
     * Name of product.
     */
    private String name;

    /**
     * Cost of product.
     */
    private BigDecimal cost;

    /**
     * Sole constructor.
     *
     * @param name name of product.
     * @param cost ost of product.
     */
    private Product(String name, BigDecimal cost) {
        this.name = name;
        this.cost = cost;
    }

    /**
     * Getter for name of product.
     *
     * @return name of product.
     */
    public String getName() {
        return name;
    }

    /**
     * Getter for cost of product.
     *
     * @return cost of product.
     */

    public BigDecimal getCost() {
        return cost;
    }

    /**
     * List of products.
     */
    public static List<Product> listProduct = new ArrayList<>();

    static {
        Product pr1 = new Product("CarA", BigDecimal.valueOf(2500.34));
        Product pr2 = new Product("CarB", BigDecimal.valueOf(2600.34));
        Product pr3 = new Product("CarC", BigDecimal.valueOf(2700.34));
        Product pr4 = new Product("CarD", BigDecimal.valueOf(2800.34));
        Product pr5 = new Product("CarE", BigDecimal.valueOf(2900.34));
        Product pr6 = new Product("CarCF", BigDecimal.valueOf(3000.34));

        listProduct.add(pr1);
        listProduct.add(pr2);
        listProduct.add(pr3);
        listProduct.add(pr4);
        listProduct.add(pr5);
        listProduct.add(pr6);
    }
}
