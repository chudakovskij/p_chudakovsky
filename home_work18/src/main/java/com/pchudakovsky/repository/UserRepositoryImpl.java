package com.pchudakovsky.repository;


import com.pchudakovsky.data.ConnectionDataBase;
import com.pchudakovsky.repository.api.UserRepository;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Provides methods for
 * for repository of user.
 *
 * @author Pavel Chudakovsky.
 */
public class UserRepositoryImpl implements UserRepository {

    // used to create queries
    private static final String SELECT_FROM_USERS_WHERE_NAME = "SELECT * FROM Users WHERE name=";

    /**
     * Returns name of user from data base.
     *
     * @param name name for user.
     * @return name of user from data base.
     */
    @Override
    public String getNameUser(String name) {
        String nameUser = null;
        try (Connection connection = ConnectionDataBase.getConnection();
             Statement statement = connection.createStatement()) {

            ResultSet result = statement
                    .executeQuery(SELECT_FROM_USERS_WHERE_NAME + "'" + name + "'");

            while (result.next()) {
                nameUser = result.getString("Name");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return nameUser;
    }


    /**
     * Id user by username.
     *
     * @param loginUser login (name) of  user.
     * @return id user by username.
     */
    @Override
    public int getIdUserByName(String loginUser) {
        int idUser = 0;
        try (Connection connection = ConnectionDataBase.getConnection();
             Statement statement = connection.createStatement()) {

            ResultSet result = statement
                    .executeQuery(SELECT_FROM_USERS_WHERE_NAME + "'" + loginUser + "'");
            while (result.next()) {
                idUser = result.getInt("id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return idUser;
    }
}
