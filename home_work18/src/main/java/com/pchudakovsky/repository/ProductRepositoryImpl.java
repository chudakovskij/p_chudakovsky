package com.pchudakovsky.repository;

import com.pchudakovsky.data.ConnectionDataBase;
import com.pchudakovsky.entity.Product;
import com.pchudakovsky.repository.api.ProductRepository;

import java.math.BigDecimal;
import java.math.MathContext;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents product repository behavior.
 *
 * @author Pavel Chudakovsky.
 */
public class ProductRepositoryImpl implements ProductRepository {

    // used to create queries
    private static final String ALL_PRODUCTS = "SELECT  * FROM Products";
    private static final String ID_PRODUCT_BY_TITLE = "SELECT * FROM Products WHERE title=";

    /**
     * Returns title for product.
     *
     * @param products string from select(menu)
     * @return name(title) for product.
     */
    @Override
    public String getNameProductFromSelect(String products) {
        String[] name = products.split(" ");
        return name[0];
    }

    /**
     * Returns total price of products.
     *
     * @param products information about products as array of string.
     * @return total price as BigDecimal.
     */
    @Override
    public BigDecimal getTotalCost(String[] products) {
        String[] cost = new String[products.length];
        for (int i = 0; i < products.length; i++) {
            String[] array = products[i].split(" ");
            cost[i] = array[1];
        }
        BigDecimal total = BigDecimal.valueOf(0);
        for (String str : cost) {
            BigDecimal sum = new BigDecimal(Double.valueOf(str));
            total = total.add(sum, MathContext.DECIMAL32);
        }
        return total;
    }

    /**
     * Returns list of products from table Products
     *
     * @return list of products for menu(select)
     */
    @Override
    public List<Product> getAllProducts() {
        List<Product> list = new ArrayList<>();
        try (Connection connection = ConnectionDataBase.getConnection();
             Statement statement = connection.createStatement()) {

            ResultSet result = statement.executeQuery(ALL_PRODUCTS);
            while (result.next()) {
                int id = result.getInt("Id");
                String title = result.getString("title");
                BigDecimal price = result.getBigDecimal("price");
                list.add(new Product(id, title, price));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    /**
     * Returns Id of product by title product.
     *
     * @param title title of product.
     * @return id for product.
     */
    @Override
    public int getIdProductByTitle(String title) {
        int idProduct = 0;
        try (Connection connection = ConnectionDataBase.getConnection();
             Statement statement = connection.createStatement();

             ResultSet result = statement.executeQuery(ID_PRODUCT_BY_TITLE + "'" + title + "'");) {
            while (result.next()) {
                idProduct = result.getInt("id");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return idProduct;
    }
}
