package com.pchudakovsky.repository;

import com.pchudakovsky.data.ConnectionDataBase;
import com.pchudakovsky.repository.api.OrderRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents order repository behavior.
 *
 * @author Pavel Chudakovsky.
 */
public class OrderRepositoryImpl implements OrderRepository {

    // used to create queries
    private static final String TOTAL_ORDER_BY_ID_ORDER = "SELECT * FROM Orders WHERE id=";
    private static final String ID_ORDER_BY_ID_USER = "SELECT * FROM Orders WHERE UserId=";
    private static final String INSERT_ORDER = "INSERT INTO Orders(Id,UserId,totalPrice) VALUES (null,?,?)";
    private static final String INSERT_ORDERS_PRODUCTS = "INSERT INTO Orders_Products(Id,orderId,productId) VALUES(null,?,?)";

    private static final String JOIN = "SELECT Orders.id,Orders.UserId, Products.title,Products.price" +
            " FROM Orders,Products,Orders_Products" +
            " WHERE Orders.id=Orders_Products.orderId and " +
            " Products.id=Orders_Products.productId" +
            " and Orders.UserId=";

    /**
     * Logger.
     */
    private final static Logger LOGGER = LogManager.getLogger(OrderRepositoryImpl.class);

    /**
     * Saves orders(table Orders).
     *
     * @param userId     id  for user.
     * @param totalPrice total price as BigDecimal.
     */
    @Override
    public void saveOrder(int userId, BigDecimal totalPrice) {
        try (Connection connection = ConnectionDataBase.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(INSERT_ORDER)) {
            preparedStatement.setInt(1, userId);
            preparedStatement.setBigDecimal(2, totalPrice);
            int result = preparedStatement.executeUpdate();
            LOGGER.info("Order saved... " + "ResultSet=" + result);
        } catch (SQLException e) {
            LOGGER.error("Order not saved", e);
        }
    }

    /**
     * Saves orders and product
     * (table Orders_Products).
     *
     * @param orderId   id for order.
     * @param productId id for product.
     */
    @Override
    public void saveOrdersProducts(int orderId, int productId) {
        try (Connection connection = ConnectionDataBase.getConnection();
             PreparedStatement preparedStatement = connection.
                     prepareStatement(INSERT_ORDERS_PRODUCTS)) {

            preparedStatement.setInt(1, orderId);
            preparedStatement.setInt(2, productId);
            int result = preparedStatement.executeUpdate();
            if (result > 0) {
                LOGGER.info("Order saved... " + "ResultSet=" + result);
            } else {
                LOGGER.info("Order not saved... " + "ResultSet=" + result);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns Id of order for user.
     *
     * @param idUser id for user.
     * @return id for order.
     */
    @Override
    public int getIdOrdersByIdUser(int idUser) {
        int orderId = 0;
        try (Connection connection = ConnectionDataBase.getConnection();
             Statement statement = connection.createStatement()) {

            ResultSet result = statement.executeQuery(ID_ORDER_BY_ID_USER + "'" + idUser + "'");
            while (result.next()) {
                orderId = result.getInt("id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return orderId;
    }


    /**
     * Returns of total.
     *
     * @param idOrder id of user.
     * @return total as {@link BigDecimal}
     */
    public BigDecimal getTotalByIdOrder(int idOrder) {
        BigDecimal total = null;
        try (Connection connection = ConnectionDataBase.getConnection();
             Statement statement = connection.createStatement()) {

            ResultSet result = statement.executeQuery(TOTAL_ORDER_BY_ID_ORDER + "'" + idOrder + "'");
            while (result.next()) {
                total = result.getBigDecimal("totalPrice");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return total;
    }


    /**
     * Returns list of orders.
     *
     * @param userId id for user.
     * @return list for orders.
     */
    @Override
    public List<String> joinOrdersProducts(int userId) {
        List<String> listOnDisplay = new ArrayList<>();
        String sdl = JOIN + "'" + userId + "'";
        try (Connection connection = ConnectionDataBase.getConnection();
             Statement statement = connection.createStatement()) {

            ResultSet result = statement.executeQuery(sdl);
            while (result.next()) {
                int orderId = result.getInt("Orders.Id");
                int idUser = result.getInt("Orders.userId");
                String title = result.getString("Products.title");
                BigDecimal price = result.getBigDecimal("Products.price");
                String orderDisplay = " Order Id: " + orderId + " user Id: " + idUser
                        + " title: " + title + " price :" + price + " $";
                listOnDisplay.add(orderDisplay);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listOnDisplay;
    }


    /**
     * @param userId id of user.
     * @return information about
     * products as {@link List}
     */
    public List<String> info(int userId) {
        String sdl = JOIN + "'" + userId + "'";
        List<String> infoProducts = new ArrayList<>();
        try (Connection connection = ConnectionDataBase.getConnection();
             Statement statement = connection.createStatement()) {

            ResultSet result = statement.executeQuery(sdl);
            int lastOrderId = 0;
            while (result.next()) {
                lastOrderId = result.getInt("Orders.Id");
            }
            ResultSet result2 = statement.executeQuery(sdl);
            while (result2.next()) {
                if (lastOrderId == result2.getInt("Orders.Id")) {
                    String title = result2.getString("Products.title");
                    BigDecimal price = result2.getBigDecimal("Products.price");
                    String infoLine = title + " " + price + " $";
                    infoProducts.add(infoLine);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return infoProducts;
    }


}
