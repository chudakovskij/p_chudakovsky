package com.pchudakovsky.repository.api;

import java.sql.Connection;

/**
 * Provides a
 * method to get a connection.
 *
 * @author Pavel Chudakovsky.
 */
public interface DataBaseRepository {
    /**
     * Returns connection.
     *
     * @return variable of type Connection.
     */
    Connection getConnection();
}
