package com.pchudakovsky.repository.api;

/**
 * Provides of methods for UserServiceImpl.
 *
 * @author Pavel Chudakovsky.
 */
public interface UserRepository {

    /**
     * Returns name of user from data base.
     *
     * @param name name for user.
     * @return name of user from data base.
     */
    String getNameUser(String name);

    /**
     * Id user by username.
     *
     * @param loginUser login (name) of  user.
     * @return id user by username.
     */
    int getIdUserByName(String loginUser);
}
