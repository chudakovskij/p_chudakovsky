package com.pchudakovsky.repository.api;

import java.math.BigDecimal;
import java.util.List;

/**
 * Provides of methods for OrderServiceImpl.
 *
 * @author Pavel Chudakovsky.
 */
public interface OrderRepository {
    /**
     * Saves orders.
     *
     * @param userId     id  for user.
     * @param totalPrice total price as BigDecimal.
     */
    void saveOrder(int userId, BigDecimal totalPrice) throws ClassNotFoundException;

    /**
     * Saves orders and product
     * (table Orders_Products).
     *
     * @param orderId   id for order.
     * @param productId id for product.
     */
    void saveOrdersProducts(int orderId, int productId);

    /**
     * Returns Id of order for user.
     *
     * @param idUser id for user.
     * @return id for order.
     */
    int getIdOrdersByIdUser(int idUser);

    /**
     * Returns list of orders.
     *
     * @param userId id for user.
     * @return list for orders.
     */
    List<String> joinOrdersProducts(int userId);

    /**
     * @param userId id of user.
     * @return information about
     * products as {@link List}
     */
    List<String> info(int userId);

    /**
     * Returns of total.
     *
     * @param idOrder id of user.
     * @return total as {@link BigDecimal}
     */
    BigDecimal getTotalByIdOrder(int idOrder);
}
