package com.pchudakovsky.servlets;

import com.pchudakovsky.repository.ProductRepositoryImpl;
import com.pchudakovsky.service.ShopServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Servlet to print a purchase report.
 *
 * @author Pavel Chudakovsky.
 */
public class OrderServlet extends HttpServlet {

    /**
     * Object of ShopServiceImpl.
     */
    private ShopServiceImpl shopService;

    /**
     * Logger.
     */
    private final static Logger LOGGER = LogManager
            .getLogger(ShopServiceImpl.class);

    /**
     * Method initializes servlet.
     */
    @Override
    public void init() {
        LOGGER.info("Method init() for "
                + OrderServlet.class + " is started!");

        LOGGER.info("Initialization of object for ProductRepositoryImpl");
        ProductRepositoryImpl productRepositoryImpl = new ProductRepositoryImpl();

        LOGGER.info("Initialization of object for ShopServiceImpl");
        shopService = new ShopServiceImpl(productRepositoryImpl);
    }

    /**
     * Post-request.
     *
     * @param req  variable of type {@link HttpServletRequest}.
     * @param resp variable of type {@link HttpServletResponse}.
     * @throws IOException if an input or output exception occurred.
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        LOGGER.info("Method doPost() for "
                + OrderServlet.class + " is started!");

        HttpSession session = req.getSession();
        List basketUser = (List) session.getAttribute("basket");

        if (basketUser == null) {
            basketUser = new ArrayList();
            session.setAttribute("basket", basketUser);
        }

        if (basketUser.isEmpty()) {
            String message = "Sorry, list of products is empty!";
            session.setAttribute("message", message);
        } else {
            session.setAttribute("message", "");
        }

        // for print basketUser( report).
        String[] userProducts = (String[]) basketUser.toArray(new String[0]);
        session.setAttribute("order", userProducts);

        BigDecimal total = shopService.getTotalCost(userProducts);
        session.setAttribute("total", total);

        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/view/order.jsp");
        dispatcher.forward(req, resp);
    }

    /**
     * Called by the servlet container to indicate
     * to a servlet that the servlet is being
     * taken out of shopService.
     */
    @Override
    public void destroy() {
        LOGGER.info("Method destroy() for "
                + OrderServlet.class + " is started!");
    }
}
