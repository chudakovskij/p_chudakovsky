package com.pchudakovsky.service;

import com.pchudakovsky.entity.Product;
import com.pchudakovsky.repository.OrderRepositoryImpl;
import com.pchudakovsky.repository.ProductRepositoryImpl;
import com.pchudakovsky.repository.UserRepositoryImpl;
import com.pchudakovsky.service.api.ShopService;

import java.math.BigDecimal;
import java.util.List;

/**
 * Provides methods for
 * the operation of the online store service.
 *
 * @author Pavel Chudakovsky.
 * @see com.pchudakovsky.repository.api.OrderRepository
 * @see com.pchudakovsky.repository.api.UserRepository
 * @see com.pchudakovsky.repository.api.ProductRepository
 */

public class ShopServiceImpl implements ShopService {

    /**
     * Repository for user.
     */
    private UserRepositoryImpl userRepositoryImpl;

    /**
     * Repository for product.
     */
    private ProductRepositoryImpl productRepositoryImpl;

    /**
     * Repository for Order.
     */
    private OrderRepositoryImpl orderRepositoryImpl;

    /**
     * The First constructor.
     *
     * @param orderRepositoryImpl {@link OrderRepositoryImpl}
     */
    public ShopServiceImpl(OrderRepositoryImpl orderRepositoryImpl) {
        this.orderRepositoryImpl = orderRepositoryImpl;
    }

    /**
     * The second constructor.
     *
     * @param productRepositoryImpl {@link ProductRepositoryImpl}
     */
    public ShopServiceImpl(ProductRepositoryImpl productRepositoryImpl) {
        this.productRepositoryImpl = productRepositoryImpl;
    }

    /**
     * The third constructor.
     *
     * @param userRepositoryImpl {@link UserRepositoryImpl}
     */
    public ShopServiceImpl(UserRepositoryImpl userRepositoryImpl) {
        this.userRepositoryImpl = userRepositoryImpl;
    }

    /**
     * The fourth constructor.
     *
     * @param userRepositoryImpl    {@link UserRepositoryImpl}
     * @param productRepositoryImpl {@link ProductRepositoryImpl}
     */
    public ShopServiceImpl(UserRepositoryImpl userRepositoryImpl,
                           ProductRepositoryImpl productRepositoryImpl) {
        this.userRepositoryImpl = userRepositoryImpl;
        this.productRepositoryImpl = productRepositoryImpl;
    }

    /**
     * @param userName username.
     * @return id of user.
     */
    public int getIdUserByName(String userName) {
        return userRepositoryImpl.getIdUserByName(userName);
    }

    /**
     * @param userName username.
     * @return username {@link UserRepositoryImpl}
     */
    public String getNameUser(String userName) {
        return userRepositoryImpl.getNameUser(userName);
    }

    /**
     * @return list of products.
     */
    public List<Product> getAllProducts() {
        return productRepositoryImpl.getAllProducts();
    }

    /**
     * @param product product from select.
     * @return name of product.
     */
    public String getNameProductFromSelect(String product) {
        return productRepositoryImpl.getNameProductFromSelect(product);
    }

    /**
     * @param title title (name) for product.
     * @return id of product.
     */
    public int getIdProductByTitle(String title) {
        return productRepositoryImpl.getIdProductByTitle(title);
    }

    /**
     * @param userProducts products for user.
     * @return cost as BigDecimal.
     */
    public BigDecimal getTotalCost(String[] userProducts) {
        return productRepositoryImpl.getTotalCost(userProducts);
    }

    public void saveOrder(int userId, BigDecimal totalPrice) {
        orderRepositoryImpl.saveOrder(userId, totalPrice);
    }


    /**
     * Id of order by id of user/
     *
     * @param userId id for user.
     * @return id order.
     */
    public int getIdOrdersByIdUser(int userId) {
        return orderRepositoryImpl.getIdOrdersByIdUser(userId);
    }


    /**
     * Saves table Orders_Products.
     *
     * @param orderId   id for order.
     * @param productId id for product.
     */
    public void saveOrdersProducts(int orderId, int productId) {
        orderRepositoryImpl.saveOrdersProducts(orderId, productId);
    }

    /**
     * @param userId id for user.
     * @return list for orders.
     */
    public List<String> joinOrdersProducts(int userId) {
        return orderRepositoryImpl.joinOrdersProducts(userId);
    }


    /**
     * @param userId id of user.
     * @return info about products as{@link List}.
     */
    public List<String> info(int userId) {
        return orderRepositoryImpl.info(userId);
    }

    /**
     * @param idOrder id of order.
     * @return total as {@link BigDecimal}
     */
    public BigDecimal getTotalByIdOrder(int idOrder) {
        return orderRepositoryImpl.getTotalByIdOrder(idOrder);
    }
}


