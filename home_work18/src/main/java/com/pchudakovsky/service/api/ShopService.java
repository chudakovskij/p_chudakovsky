package com.pchudakovsky.service.api;

import com.pchudakovsky.entity.Product;

import java.math.BigDecimal;
import java.util.List;

/**
 * Provides methods for
 * the operation of the online store service.
 *
 * @author Pavel Chudakovsky.
 * @see com.pchudakovsky.repository.api.OrderRepository
 * @see com.pchudakovsky.repository.api.UserRepository
 * @see com.pchudakovsky.repository.api.ProductRepository
 */
public interface ShopService {

    int getIdUserByName(String userName);

    String getNameUser(String userName);

    List<Product> getAllProducts();

    String getNameProductFromSelect(String product);

    int getIdProductByTitle(String title);

    BigDecimal getTotalCost(String[] userProducts);

    void saveOrder(int userId, BigDecimal totalPrice);

    int getIdOrdersByIdUser(int userId);

    void saveOrdersProducts(int orderId, int productId);

    List<String> joinOrdersProducts(int userId);

    List<String> info(int userId);

    BigDecimal getTotalByIdOrder(int idOrder);


}

