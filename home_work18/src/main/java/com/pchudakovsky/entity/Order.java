package com.pchudakovsky.entity;

import java.math.BigDecimal;

/**
 * Describes the order for Online shop.
 *
 * @author Pavel Chudakovsky.
 */
@SuppressWarnings("unused")
public class Order {

    /**
     * Id of Order.
     */
    private int id;

    /**
     * Total.
     */
    private BigDecimal totalPrice;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @SuppressWarnings("unused")
    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    @SuppressWarnings("unused")
    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }
}
