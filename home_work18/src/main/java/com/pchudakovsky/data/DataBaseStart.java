package com.pchudakovsky.data;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

import org.apache.ibatis.jdbc.ScriptRunner;

/**
 * Provides a method for reading a script of SQL.
 *
 * @author Pavel Chudakovsky.
 */
public class DataBaseStart {

    /**
     * Path for DB SQL
     */
    private final static String PATH_DB_SQL = "src/main/resources/db_online_shop.sql";

    /**
     * Boolean baseCreated (true if data
     * base is created).
     */
    private static boolean baseCreated = false;

    /**
     * Returns true if data base is created.
     *
     * @return variable of boolean type.
     */
    public static boolean isBaseCreated() {
        return baseCreated;
    }

    /**
     * Creates a database.
     */
    public static void createDatabase() {
        try (Reader reader = new BufferedReader(new FileReader(PATH_DB_SQL))) {
            new ScriptRunner(ConnectionDataBase.getConnection()).runScript(reader);
            DataBaseStart.baseCreated = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
