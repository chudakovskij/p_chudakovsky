package com.pchudakovsky.data;

import com.pchudakovsky.utility.ReaderProperty;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Provides a connection to the data base.
 */
public class ConnectionDataBase {

    /**
     * Driver for DB.
     */
    private static final String DB_DRIVER = ReaderProperty.getPropertyFromDb("jdbc.driverClassName");

    /**
     * URL for DB.
     */
    private static final String DB_URL = ReaderProperty.getPropertyFromDb("jdbc.url");

    /**
     * Username.
     */
    private static final String DB_USER_NAME = ReaderProperty.getPropertyFromDb("jdbc.username");
    private static final String DB_PASSWORD = ReaderProperty.getPropertyFromDb("jdbc.password");

    /**
     * Logger.
     */
    private static final Logger LOGGER = LogManager.getLogger(ConnectionDataBase.class);

    /**
     * Checks database connection.
     *
     * @return variable of type Connection.
     */

    public static Connection getConnection() {
        try {
            Class.forName(DB_DRIVER);
            Connection connection = DriverManager.
                    getConnection(DB_URL, DB_USER_NAME, DB_PASSWORD);
            if (connection != null) {
                LOGGER.info("Connected to the database");
                return connection;
            }
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
