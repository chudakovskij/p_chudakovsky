package com.pchudakovsky.utility;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Reader of property.
 */
public class ReaderProperty {

    private static final String PROPERTIES_PATH = "db.properties";

    private static Properties props = new Properties();

    /**
     * Sole constructor.
     */
    public ReaderProperty() {
    }

    static {
        try (InputStream input = ReaderProperty.class.getClassLoader().getResourceAsStream(PROPERTIES_PATH)) {
            props.load(input);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // required to read url, driver ... parameters in ConnectionDataBase
    public static String getPropertyFromDb(String nameProperty) {
        return props.getProperty(nameProperty);
    }
}
