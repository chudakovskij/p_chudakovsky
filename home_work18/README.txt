
To run the application:

1.plugin(Intellij IDEA)  -> jetty:run  OR cmd: mvn clean package jetty:run
2. for Start page:
    1)Enter address line of browser:  localhost:8080/online-shop (for start page of Online shop)
    2)Enter username (for example "admin") and click button "Enter".
    ( will be redirected ->(localhost:8080/menu (for menu  page)))

3. for Menu page:
   Make a product selection and press the buttons:
   button
  "Add item":
  (will be redirected->(http://localhost:8080/add )

  button
  "Submit":
   will be redirected-> (localhost:8080/report (for Report page))

4. for Report page:
 Click button "LogOut" to out online shop.
(will be redirected-> localhost:8080/logOut ( and  Start page)).

5. Click button "Save" to save order.

!Warning: use post requests (through forms and application buttons).
!INFO: URL for servlets:
 /start StartServlet
 /menu MenuServlet
 /report ReportServlet
 /logOut LogOut
 /redirect RedirectServlet
 /add AddServlet
