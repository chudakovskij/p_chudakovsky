package com.pchudakovsky.hw11;

import org.junit.Test;

import static org.junit.Assert.*;

public class LineHandlerTest {

    @Test
    public void testNameFile() {
        String path = "root/folder1/file.exe";
        LineHandler lineHandler = new LineHandler();
        String actual = lineHandler.nameFile(path);
        String expected = "file";
        assertEquals(expected, actual);
    }

    @Test
    public void testCheckFileForCorrectName() {

        String path = "root/folder1/file.exe";
        LineHandler lineHandler = new LineHandler();
        boolean actual = lineHandler.checkFile(path);
        assertTrue(actual);
    }

    @Test
    public void testCheckFileForNameWithNumber() {
        String path = "root/folder1/fi343le.exe";
        LineHandler lineHandler = new LineHandler();
        boolean actual = lineHandler.checkFile(path);
        assertTrue(actual);
    }

    @Test
    public void testCheckFileForNameWithSymbol() {
        String path = "root/folder1/file@$xxx.exe";
        LineHandler lineHandler = new LineHandler();
        boolean actual = lineHandler.checkFile(path);
        assertFalse(actual);
    }

    @Test
    public void testCheckFileForCorrectExtensionFile() {
        String path = "root/folder1/file.mp3";
        LineHandler lineHandler = new LineHandler();
        boolean actual = lineHandler.checkFile(path);
        assertTrue(actual);
    }

    @Test
    public void testCheckFileForExtensionFileWithFirstNumber() {
        String path = "root/folder1/file.4exe";
        LineHandler lineHandler = new LineHandler();
        boolean actual = lineHandler.checkFile(path);
        assertFalse(actual);
    }

    @Test
    public void testCheckFileForExtensionWithSymbol() {
        String path1 = "root/folder1/file.ex#e";
        String path2 = "root/folder1/file.e$x$e";
        LineHandler lineHandler = new LineHandler();
        boolean actual1 = lineHandler.checkFile(path1);
        boolean actual2 = lineHandler.checkFile(path2);
        assertFalse(actual1);
        assertFalse(actual2);
    }


    @Test
    public void testCheckFileForExtensionFileWithLongLength() {
        String path = "root/folder1/file.extension";
        LineHandler lineHandler = new LineHandler();
        boolean actual = lineHandler.checkFile(path);
        assertFalse(actual);
    }

    @Test
    public void testCheckFileForExtensionFileWithSmallLength() {
        String path = "root/folder1/file.ex";
        LineHandler lineHandler = new LineHandler();
        boolean actual = lineHandler.checkFile(path);
        assertFalse(actual);
    }

    @Test
    public void testCheckPathForCorrectNameFolder() {
        String path = "root/folder1/file.exe";
        LineHandler lineHandler = new LineHandler();
        boolean actual = lineHandler.checkPath(path);
        assertTrue(actual);
    }

    @Test
    public void testCheckPathForNotCorrectNameFolder() {
        String path1 = "root/fold:er/myFile.exe";
        String path2 = "root/fold*er/myFile.exe";
        String path3 = "root/fold?er/myFile.exe";
        LineHandler lineHandler = new LineHandler();
        boolean actual1 = lineHandler.checkPath(path1);
        boolean actual2 = lineHandler.checkPath(path2);
        boolean actual3 = lineHandler.checkPath(path3);
        assertFalse(actual1);
        assertFalse(actual2);
        assertFalse(actual3);
    }
}