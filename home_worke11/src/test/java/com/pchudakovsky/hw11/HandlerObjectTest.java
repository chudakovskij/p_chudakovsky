package com.pchudakovsky.hw11;

import org.junit.Test;

import static org.junit.Assert.*;

public class HandlerObjectTest {

    private HandlerObject handlerObject = new HandlerObject();

    @Test
    public void testSave() {
        handlerObject.save(new Folder("TestSave"), "testSave.txt");
        Folder folder = (Folder) handlerObject.download("testSave.txt");
        String actual = folder.getNameFolder();
        String expected = "TestSave";
        assertEquals(expected, actual);
    }

    @Test
    public void testDownload() {
        handlerObject.save(new Folder("TestSave"), "testSave.txt");
        Folder folder = (Folder) handlerObject.download("testSave.txt");
        assertNotNull(folder);

        folder.add(new File("File"));
        handlerObject.save(folder, "testDownload.txt");
        Folder newFolder = (Folder) handlerObject.download("testDownload.txt");
        assertNotNull(newFolder);

        String actual = newFolder.getNameFolder();
        String expected = "TestSave";
        assertEquals(expected, actual);
    }
}