package com.pchudakovsky.hw11;

import com.pchudakovsky.hw11.api.Component;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class FolderTest {

    @Test
    public void testPrint() {
        Folder root = new Folder("root");
        LineHandler lineHandler = new LineHandler();
        File file = new File(lineHandler.nameFile("MyFolder/folder1/ file1.exe"));
        file.setFullPath("MyFolder/folder1/ file1.exe");
        FileSystem fileSystem = new FileSystem();
        List<Folder> list = fileSystem.getListFolder(file, "MyFolder/folder1/file1.exe");
        file.setListFolder(list);
        fileSystem.treeFolder(list, root, 1);
        root.print();
        boolean actual = root.isResult();
        assertTrue(actual);
    }

    @Test
    public void testAdd() {
        Folder folder = new Folder();
        Component file1 = new File("file1");
        folder.add(file1);
        boolean flagAdd = folder.getListPrinter().isEmpty();
        assertFalse(flagAdd);
        boolean actual = folder.isResult();
        assertTrue(actual);
    }
}