package com.pchudakovsky.hw11;


import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class FileTest {

    @Test
    public void testGetNameFile() {
        LineHandler lineHandler = new LineHandler();
        File file = new File(lineHandler.nameFile("MyFolder/folder1/file1.exe"));
        String actual = file.getNameFile();
        String expected = "file1";
        assertEquals(expected, actual);
    }

    @Test
    public void print() {
        LineHandler lineHandler = new LineHandler();
        File file = new File(lineHandler.nameFile("MyFolder/folder1/file1.exe"));
        file.setFullPath("MyFolder/folder1/file1.exe");
        FileSystem fileSystem = new FileSystem();
        List<Folder> list = fileSystem.getListFolder(file, "MyFolder/folder1/file1.exe");
        file.setListFolder(list);
        file.print();
        boolean actual = file.isResult();
        assertTrue(actual);
    }


}