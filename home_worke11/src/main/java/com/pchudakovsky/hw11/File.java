package com.pchudakovsky.hw11;

import com.pchudakovsky.hw11.api.Component;

import java.io.Serializable;
import java.util.List;

/**
 * Class models the behavior of file.
 *
 * @author Pavel Chudakovsky.
 */
public class File implements Component, Serializable {
    /**
     * Used for deserialization.
     */
    private static final long serialVersionUID = 6631034L;

    /**
     * Name of  file.
     */
    private String nameFile;
    /**
     * Path to file.
     */
    private String fullPath;
    /**
     * Used in tests.
     */
    private boolean result;

    //Getters and setters.
    File(String nameFile) {
        this.nameFile = nameFile;
    }

    String getNameFile() {
        return nameFile;
    }

    private String getFullPath() {
        return fullPath;
    }

    void setFullPath(String fullPath) {
        this.fullPath = fullPath;
    }

    boolean isResult() {
        return result;
    }

    private void setResult(boolean result) {
        this.result = result;
    }

    private List<Folder> listFolder;

    private List<Folder> getListFolder() {
        return listFolder;
    }

    void setListFolder(List<Folder> listFolder) {
        this.listFolder = listFolder;
    }

    /**
     * Prints structure of catalog.
     */
    @SuppressWarnings("ForLoopReplaceableByForEach")
    @Override
    public void print() {
        setResult(false);
        String path = getFullPath();
        String[] array = path.split("(/)");
        int space = 1;
        List<Folder> list = getListFolder();
        String tab = null;
        for (int i = 0; i < list.size(); i++) {
            Folder folder = list.get(i);
            String shift = String.valueOf(space += 8);
            tab = "%".concat(shift).concat("s%n");
            System.out.printf(tab, folder.getNameFolder() + "/");
        }
        System.out.printf(tab, array[array.length - 1] + "\n");
        setResult(true);
    }
}
