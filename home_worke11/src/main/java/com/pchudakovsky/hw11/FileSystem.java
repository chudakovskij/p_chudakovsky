package com.pchudakovsky.hw11;


import java.io.IOException;
import java.util.*;

/**
 * Class models the behavior of file system.
 *
 * @author Pavel Chudakovsky.
 */
class FileSystem {

    private static final String FILE = "root.txt";
    private static final String PRINT = "print";
    private static final String ROOT = "root";
    private static final String EXIT = "0";
    private Folder memory;

    /**
     * Starts file system simulation.
     */
    void run() {

        try (Scanner scanner = new Scanner(System.in)
        ) {
            dataFile();

            HandlerObject handlerObject = new HandlerObject();
            Folder rootFolder = new Folder(ROOT);
            while (true) {
                LineHandler lineHandler = new LineHandler();
                info();
                String path = scanner.nextLine();

                if (EXIT.equals(path)) {
                    handlerObject.save(rootFolder, FILE);
                    break;
                } else if (PRINT.equals(path)) {
                    rootFolder = (Folder) handlerObject.download(FILE);
                    rootFolder.print();

                } else if (lineHandler.checkFile(path) & lineHandler.checkPath(path)) {
                    String nameFile = lineHandler.nameFile(path);
                    File file = new File(nameFile);
                    System.out.println("File created with name: " + file.getNameFile());
                    file.setFullPath(path);
                    List<Folder> list = getListFolder(file, path);//  list of folders.
                    file.setListFolder(list);
                    treeFolder(list, rootFolder, 1); // create tree from folders
                    handlerObject.save(rootFolder, FILE);
                    System.out.println("File recorded!");
                } else {
                    message();
                }
            }

        }
    }

    /**
     * File for serialization.
     */
    @SuppressWarnings("ResultOfMethodCallIgnored")
    private void dataFile() {
        java.io.File fileTxt = new java.io.File(FILE);
        try {
            fileTxt.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Prints information
     */
    private void info() {
        System.out.println("\nProgram *File system* ");
        System.out.println("0- for exit");
        System.out.println("print - to print result ");
        System.out.println("Enter path to file: ");
        System.out.println("************************");
    }

    /**
     * Print message
     */
    private void message() {
        System.out.println("Incorrect entry of the folder name. " +
                "Forbidden symbols: |,*<,>,?,:.\n" +
                "OR\n"
                + "Incorrect input of the name or file extension:" +
                "Forbidden symbols:" + "@,!,*,#,$,(,),[,],{,},<,>,?,^=\n" +
                "Warning! Extension cannot be less than 3 and more than 6 characters.\n" +
                "Extension cannot start with a digit:0-9.");
    }

    /**
     * Creation of folders according to
     * the specified file path.
     *
     * @param file value of type File.
     * @param path path to file.
     * @return list folders.
     */
    @SuppressWarnings("ForLoopReplaceableByForEach")
    List<Folder> getListFolder(File file, String path) {
        String[] splitLine = path.split("(/)");// divide the path into tokens
        String[] newPath = Arrays.copyOf(splitLine, splitLine.length - 1);
        List<Folder> listFolder = new ArrayList<>();
        for (int counter = 0; counter < newPath.length; counter++) {
            Folder newFolder = new Folder(newPath[counter]);
            listFolder.add(newFolder);
        }
        listFolder.get(listFolder.size() - 1).add(file);
        return listFolder;
    }

    /**
     * The method provides nesting of folders.
     *
     * @param folders list of folders
     *                from which the tree will be built.
     * @param root    root folder.
     * @param counter counter for recursion.
     */
    void treeFolder(List<Folder> folders, Folder root, int counter) {
        if (folders.size() == 1) {
            System.out.println(folders.get(0).getNameFolder());
            root.add(folders.get(0));
        } else {
            Folder first = folders.get(counter - 1);
            if (counter <= folders.size()) {
                first.add(folders.get(counter));
                if ((counter - 1 == 0)) {
                    memory = first;
                }
                if (counter < folders.size() - 1) {
                    treeFolder(folders, root, counter + 1);
                } else {
                    root.add(memory);
                }
            }
        }
    }

}



