package com.pchudakovsky.hw11;

/**
 * This class for launch app.
 *
 * @author Pavel Chudakovsky.
 */
public class Main {
    public static void main(String[] args) {
        FileSystem run = new FileSystem();
        run.run();
    }
}