package com.pchudakovsky.hw11.api;

public interface SerializeService {
    void save(Object obj, String fileName);

     Object download(String fileName);
}
