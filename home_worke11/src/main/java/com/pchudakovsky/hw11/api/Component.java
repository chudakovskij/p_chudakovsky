package com.pchudakovsky.hw11.api;

/**
 * Single method interface.
 */
public interface Component {
    void print();
}
