package com.pchudakovsky.hw11;

import com.pchudakovsky.hw11.api.SerializeService;

import java.io.*;

/**
 * The class provides methods for serialization
 * and deserialization of objects.
 *
 * @author Pavel Chudakovsky.
 */
public class HandlerObject implements SerializeService {
    /**
     * Method for serialization of objects.
     *
     * @param obj      object to be serialized.
     * @param fileName name of the file that stores
     *                 the object.
     */
    @Override
    public void save(Object obj, String fileName) {
        try (FileOutputStream fileOutput = new FileOutputStream(fileName)) {
            ObjectOutputStream objectOutput = new ObjectOutputStream(fileOutput);
            objectOutput.writeObject(obj);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method for deserialization of objects.
     *
     * @param fileName name of the file that stores
     *                 the object.
     */
    @Override
    public Object download(String fileName) {
        try (FileInputStream fileInput = new FileInputStream(fileName)

        ) {
            ObjectInputStream objectInput = new ObjectInputStream(fileInput);

            return objectInput.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}
