package com.pchudakovsky.hw11;

import com.pchudakovsky.hw11.api.Component;

import java.io.Serializable;
import java.util.HashSet;

/**
 * Class models the behavior of folder.
 *
 * @author Pavel Chudakovsky.
 */
public class Folder implements Component, Serializable {

    /**
     * Used for deserialization.
     */
    private static final long serialVersionUID = 9111031L;

    /**
     * Name of folder.
     */
    private String nameFolder;

    /**
     * Used in tests.
     */
    private boolean result;

    /**
     * Empty constructor.
     */
    Folder() {
    }

    /**
     * Constructor contains the folder name.
     *
     * @param nameFolder as name of folder.
     */
    Folder(String nameFolder) {
        this.nameFolder = nameFolder;
    }


    boolean isResult() {
        return result;
    }

    private void setResult(boolean result) {
        this.result = result;
    }

    /**
     * Returns name of Folder.
     *
     * @return name of folder as String.
     */
    String getNameFolder() {
        return nameFolder;
    }

    //Getter and setters.

    HashSet<Component> getListPrinter() {
        return listPrinter;
    }

    /**
     * Stores components in a folder.
     */
    private HashSet<Component> listPrinter = new HashSet<>();

    /**
     * Prints of structure root folder.
     */
    @Override
    public void print() {
        setResult(false);
        for (Component component : listPrinter) {
            component.print();
        }
        setResult(true);
    }

    /**
     * Adds files and folders to the root folder.
     *
     * @param component as folder or file.
     */
    void add(Component component) {
        setResult(false);
        listPrinter.add(component);
        setResult(true);
    }
}