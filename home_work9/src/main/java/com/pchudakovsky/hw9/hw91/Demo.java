package com.pchudakovsky.hw9.hw91;

/**
 * Test class.
 *
 * @author Pavel Chudakovsky.
 * @see Sorter
 */
public class Demo {

    public static void main(String[] args) {

        int[] array = {15, 45, 23, 15, 12, 100}; // Default array.
        Sorter sorter = new Sorter();
        sorter.run(array);
    }
}
