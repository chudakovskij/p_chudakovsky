package com.pchudakovsky.hw9.hw91;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Class is for sorting an array.
 *
 * @author Pavel Chudacovsky.
 */
class Sorter {

    /**
     * Stores the value of the digits for each element of the array.
     */
    private List<Integer> sumNumbers = new ArrayList<>();
    /**
     * Stores the value of the sorted array.
     */
    private List<Integer> numbers = new ArrayList<>();

    /**
     * Returns list with sum of digit.
     *
     * @return list with value sum of digit.
     */
    List<Integer> getSumNumbers() {
        return sumNumbers;
    }

    /**
     * Returns list with value from sorted array.
     *
     * @return ist with value from sorted array.
     */
    List<Integer> getNumbers() {
        return numbers;
    }

    /**
     * Runs the application.
     *
     * @param array default array(unsorted array).
     */
    void run(int[] array) {
        System.out.println("******* Program Sorter *******");
        int[] memory = Arrays.copyOf(array, array.length);
        System.out.println(Arrays.toString(memory) + " - Default array!");
        sort(array);
        printResult();
    }

    /**
     * Sorts an array.
     *
     * @param array default array(unsorted array).
     */
    private void sort(int[] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length - 1 - i; j++) {
                if (sumForNumber(array[j]) > sumForNumber(array[j + 1])) {
                    int memory = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = memory;
                }
            }
        }

        for (int element : array) {
            numbers.add(element);
            sumNumbers.add(sumForNumber(element));
        }

    }

    /**
     * Prints result.
     */
    private void printResult() {
        System.out.println("Result program *Sorter* :");
        System.out.println(numbers + " - sorted array"
                + "\n" + sumNumbers + "- array from sum of digits");
    }

    /**
     * Returns sum from digit of number.
     *
     * @param number integer value.
     * @return sum from digit of number.
     */
    private int sumForNumber(int number) {
        int sum = 0;
        while (number > 0) {
            sum = sum + number % 10;
            number = number / 10;
        }
        return sum;
    }
}
