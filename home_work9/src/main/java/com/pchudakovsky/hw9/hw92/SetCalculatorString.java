package com.pchudakovsky.hw9.hw92;

import java.util.HashSet;
import java.util.Set;

/**
 * Class is provided by mathematical methods
 * for working with sets of strings.
 *
 * @author Pavel Chudakovsky.
 */
public class SetCalculatorString implements Function<String> {
    /**
     * Union sets.
     *
     * @param a set of strings.
     * @param b set of strings.
     * @return united set of strings .
     */
    @Override
    public Set<String> union(Set<String> a, Set<String> b) {
        Set<String> result = new HashSet<>();
        result.addAll(a);
        result.addAll(b);
        return result;
    }

    /**
     * Intersection sets.
     *
     * @param a set of strings.
     * @param b set of strings.
     * @return intersection sets of strings.
     */
    @Override
    public Set<String> intersection(Set<String> a, Set<String> b) {
        Set<String> result = new HashSet<>(a);
        result.retainAll(b);
        return result;
    }

    /**
     * Difference sets.
     *
     * @param a set of strings.
     * @param b set of strings.
     * @return sets of strings.
     */
    @Override
    public Set<String> minus(Set<String> a, Set<String> b) {
        Set<String> minus = new HashSet<>(a);
        minus.removeAll(b);
        return minus;
    }

    /**
     * Symmetric difference of sets.
     *
     * @param a set of strings.
     * @param b set of strings.
     * @return sets of strings.
     */
    @Override
    public Set<String> difference(Set<String> a, Set<String> b) {
        Set<String> difference = new HashSet<>(a);
        difference.addAll(b);

        Set<String> temp = new HashSet<>(a);
        temp.retainAll(b);
        difference.removeAll(temp);
        return difference;
    }
}
