package com.pchudakovsky.hw9.hw92;


import java.util.Set;

/**
 * Generic interface.
 *
 * @param <T> his is the type parameter.
 */
public interface Function<T> {
    /**
     * Union sets.
     *
     * @param a set of T.
     * @param b set of T.
     * @return united set of T.
     */
    Set<T> union(Set<T> a, Set<T> b);

    /**
     * Intersection sets.
     *
     * @param a set of T.
     * @param b set of T.
     * @return set of T.
     */
    Set<T> intersection(Set<T> a, Set<T> b);

    /**
     * Difference sets of T.
     *
     * @param a set of T.
     * @param b set of T.
     * @return set of T.
     */
    Set<T> minus(Set<T> a, Set<T> b);

    /**
     * Symmetric difference of sets.
     *
     * @param a set of T.
     * @param b set of T.
     * @return sets of T.
     */
    Set<T> difference(Set<T> a, Set<T> b);
}
