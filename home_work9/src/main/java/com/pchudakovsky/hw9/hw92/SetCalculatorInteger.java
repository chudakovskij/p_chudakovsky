package com.pchudakovsky.hw9.hw92;

import java.util.HashSet;
import java.util.Set;

/**
 * Class is provided by mathematical methods
 * for working with sets of numbers.
 *
 * @author Pavel Chudakovsky.
 */
public class SetCalculatorInteger implements Function<Integer> {
    /**
     * Union sets.
     *
     * @param a set of integers.
     * @param b set of integers.
     * @return united set of integers.
     */
    @Override
    public Set<Integer> union(Set<Integer> a, Set<Integer> b) {
        Set<Integer> result = new HashSet<>();
        result.addAll(a);
        result.addAll(b);
        return result;
    }

    /**
     * Intersection sets.
     *
     * @param a set of integers.
     * @param b set of integers.
     * @return intersection sets of integers.
     */
    @Override
    public Set<Integer> intersection(Set<Integer> a, Set<Integer> b) {
        Set<Integer> result = new HashSet<>(a);
        result.retainAll(b);
        return result;
    }

    /**
     * Difference sets.
     *
     * @param a set of integers.
     * @param b set of integers.
     * @return sets of integers.
     */
    @Override
    public Set<Integer> minus(Set<Integer> a, Set<Integer> b) {
        Set<Integer> minus = new HashSet<>(a);
        minus.removeAll(b);
        return minus;
    }

    /**
     * Symmetric difference of sets.
     *
     * @param a set of integers.
     * @param b set of integers.
     * @return sets of integers.
     */
    @Override
    public Set<Integer> difference(Set<Integer> a, Set<Integer> b) {
        Set<Integer> difference = new HashSet<>(a);
        difference.addAll(b);

        Set<Integer> temp = new HashSet<>(a);
        temp.retainAll(b);
        difference.removeAll(temp);
        return difference;
    }
}
