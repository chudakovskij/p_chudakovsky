package com.pchudakovsky.hw9.hw91;

import org.junit.Test;

import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

public class SorterTest {

    private int[] array = {15, 45, 23, 15, 12, 100};

    @Test
    public void testSort() {
        Sorter sorter = new Sorter();
        sorter.run(array);
        List<Integer> sum = sorter.getSumNumbers();
        List<Integer> numbers = sorter.getNumbers();

        assertEquals(new Integer(1), sum.get(0));
        assertEquals(new Integer(3), sum.get(1));

        assertEquals(new Integer(100), numbers.get(0));
        assertEquals(new Integer(12), numbers.get(1));
    }
}