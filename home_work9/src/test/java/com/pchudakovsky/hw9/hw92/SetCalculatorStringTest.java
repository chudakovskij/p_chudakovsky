package com.pchudakovsky.hw9.hw92;

import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

@SuppressWarnings("unchecked")
public class SetCalculatorStringTest {

    private Set<String> setA = new HashSet<>();
    private Set<String> setB = new HashSet<>();
    private Function function = new SetCalculatorString();
    private Set<String> testUnion = new HashSet<>();
    private Set<String> testDifference = new HashSet<>();

    {
        testUnion.add("A");
        testUnion.add("B");
        testUnion.add("C");
        testUnion.add("D");
        testUnion.add("E");
        testUnion.add("F");
    }

    {
        setA.add("A");
        setA.add("B");
        setA.add("C");

        setB.add("D");
        setB.add("E");
        setB.add("F");
    }

    {

        testDifference.add("A");
        testDifference.add("B");
        testDifference.add("D");
        testDifference.add("E");
        testDifference.add("F");
    }


    @Test
    public void testUnion() {
        Set<String> result = function.union(setA, setB);
        assertTrue((result.containsAll(testUnion)));
        System.out.println(setA + " + " + setB + "=" + result);
    }

    @Test
    public void testIntersection() {
        setB.add("C");
        Set<String> result = function.intersection(setA, setB);
        assertTrue((result.contains("C")));
        assertFalse((result.contains("B")));
        System.out.println(setA + " intersection " + setB + "=" + result);
    }

    @Test
    public void testMinus() {
        setB.add("C");
        Set<String> result = function.minus(setA, setB);
        assertTrue((result.contains("A")));
        assertTrue((result.contains("B")));
        System.out.println(setA + " - " + setB + "=" + result);
    }

    @Test
    public void testDifference() {
        setB.add("C");
        Set<String> result = function.difference(setA, setB);
        assertTrue((result.containsAll(testDifference)));
        System.out.println(setA + " difference " + setB + "=" + result);
    }
}