package com.pchudakovsky.hw9.hw92;

import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

@SuppressWarnings("unchecked")
public class SetCalculatorIntegerTest {

    private Set<Integer> setA = new HashSet<>();
    private Set<Integer> setB = new HashSet<>();
    private Function function = new SetCalculatorInteger();
    private Set<Integer> testUnion = new HashSet<>();
    private Set<Integer> testDifference = new HashSet<>();

    {
        testUnion.add(1);
        testUnion.add(2);
        testUnion.add(3);
        testUnion.add(4);
        testUnion.add(5);
        testUnion.add(6);
    }

    {
        setA.add(1);
        setA.add(2);
        setA.add(3);

        setB.add(4);
        setB.add(5);
        setB.add(6);
    }

    {

        testDifference.add(1);
        testDifference.add(2);
        testDifference.add(4);
        testDifference.add(5);
        testDifference.add(6);
    }


    @Test
    public void testUnion() {
        Set<Integer> result = function.union(setA, setB);
        assertTrue((result.containsAll(testUnion)));
        System.out.println(setA + " + " + setB + "=" + result);
    }


    @Test
    public void testIntersection() {
        setB.add(3);
        Set<Integer> result = function.intersection(setA, setB);
        assertTrue((result.contains(3)));
        assertFalse((result.contains(5)));
        System.out.println(setA + " intersection " + setB + "=" + result);
    }

    @Test
    public void testMinus() {
        setB.add(3);
        Set<Integer> result = function.minus(setA, setB);
        assertTrue((result.contains(1)));
        assertTrue((result.contains(2)));
        System.out.println(setA + " - " + setB + "=" + result);
    }

    @Test
    public void testDifference() {
        setB.add(3);
        Set<Integer> result = function.difference(setA, setB);
        assertTrue((result.containsAll(testDifference)));
        System.out.println(setA + " difference " + setB + "=" + result);
    }
}