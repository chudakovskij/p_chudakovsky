package com.pchudakovsky.hw14.utility;

import com.fasterxml.jackson.databind.JsonNode;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import static com.pchudakovsky.hw14.data.Data.URI;
import static org.junit.Assert.*;

public class MapperJsonTest {
    private static HttpURLConnection connection;
    private static HttpResponse response;

    @Before
    public void initHttpConnection() throws IOException {
        int id = 35;
        URL obj = new URL(URI + id);
        connection = (HttpURLConnection) obj.openConnection();
    }

    @Before
    public void initHttpClient() throws IOException {
        int id = 3;
        HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(URI + id);
        response = client.execute(request);
    }

    @Test
    public void testJsonNodeForHttpUrlNotNull() throws IOException {
        JsonNode node = MapperJson.jsonNodeForHttpUrl(connection);
        int expectedId = 35;
        assertNotNull(node);
        assertEquals(expectedId, node.get("id").asInt());
    }

    @Test
    public void testJsonNodeForHttpClientNotNull() throws IOException {
        JsonNode node = MapperJson.jsonNodeForHttpClient(response);
        int expectedId = 3;
        assertNotNull(node);
        assertEquals(expectedId, node.get("id").asInt());
    }

    @Test
    public void testJsonNodeToStringArray() throws IOException {
        JsonNode node = MapperJson.jsonNodeForHttpClient(response);
        String[] actual = MapperJson.jsonNodeToStringArray(node);
        String[] expected = {node.get("userId").asText(),
                node.get("id").asText(), node.get("title").asText(),
                node.get("body").asText()};

        assertNotNull(actual);
        assertArrayEquals(expected, actual);
    }
}