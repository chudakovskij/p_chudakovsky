package com.pchudakovsky.hw14.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;

import static org.junit.Assert.*;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class HttpClientServiceTest {

    private String[] bodyOne = null;
    private String[] bodyTwo = null;

    @Before
    public void init() throws IOException {
        HttpClientService httpClientService = new HttpClientService();
        bodyOne = httpClientService.get(7);
        bodyTwo = httpClientService.get(10);
    }

    @Mock
    private HttpClientService mockHttpClient;

    @Test
    public void testGetOnNotNull() throws IOException {

        // given:
        int id = 7;
        String[] body = bodyOne;
        when(mockHttpClient.get(id)).thenReturn(body);

        // when, then:
        assertNotNull(mockHttpClient.get(id));

        // then:
        verify(mockHttpClient, times(1)).get(id);
    }

    @Test
    public void testGetOnAllBodyResponse() throws IOException {

        // given:
        int id = 10;
        String[] body = bodyTwo;
        when(mockHttpClient.get(id)).thenReturn(body);

        // when, then:
        assertArrayEquals(bodyTwo, mockHttpClient.get(id));

        // then:
        verify(mockHttpClient, times(1)).get(id);
    }

    @Test
    public void testGetOnNotEqualsBodyResponse() throws IOException {

        // given:
        int id = 10;
        String[] body = bodyTwo;
        when(mockHttpClient.get(id)).thenReturn(body);

        // when:
        String[] actual = mockHttpClient.get(id);

        //then:
        String[] expected = bodyOne;
        assertNotEquals(expected, actual);

        // then:
        verify(mockHttpClient).get(id);
    }

    @Test
    public void testPostCalledVerified() throws IOException {

        // given:
        int userId = 101;
        int articleId = 101;
        String newTitle = "Java";
        String newBodyArticle = "Java 8";

        doNothing().when(mockHttpClient).post(isA(Integer.class), isA(Integer.class),
                isA(String.class), isA(String.class));

        //when:
        mockHttpClient.post(userId, articleId, newTitle, newBodyArticle);

        // then:
        verify(mockHttpClient, times(1))
                .post(userId, articleId, newTitle, newBodyArticle);
    }
}