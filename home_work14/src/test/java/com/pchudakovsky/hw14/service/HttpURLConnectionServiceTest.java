package com.pchudakovsky.hw14.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class HttpURLConnectionServiceTest {

    private String[] bodyOne = null;
    private String[] bodyTwo = null;

    @Before
    public void init() throws IOException {
        HttpURLConnectionService httpURLService =
                new HttpURLConnectionService();
        bodyOne = httpURLService.get(7);
        bodyTwo = httpURLService.get(10);
    }

    @Mock
    private HttpURLConnectionService mockHttpURL;

    @Test
    public void testGetOnNotNull() throws IOException {

        // given:
        int id = 7;
        String[] body = bodyOne;
        when(mockHttpURL.get(id)).thenReturn(body);

        // when, then:
        assertNotNull(mockHttpURL.get(id));

        // then:
        verify(mockHttpURL, times(1)).get(id);
    }

    @Test
    public void testGetOnAllBodyResponse() throws IOException {

        // given:
        int id = 10;
        String[] body = bodyTwo;
        when(mockHttpURL.get(id)).thenReturn(body);

        // when, then:
        assertArrayEquals(bodyTwo, mockHttpURL.get(id));

        // then:
        verify(mockHttpURL, times(1)).get(id);
    }

    @Test
    public void testGetOnNotEqualsBodyResponse() throws IOException {
        // given:
        int id = 10;
        String[] body = bodyTwo;
        when(mockHttpURL.get(id)).thenReturn(body);

        // when:
        String[] actual = mockHttpURL.get(id);

        //then:
        String[] expected = bodyOne;
        assertNotEquals(expected, actual);

        // then:
        verify(mockHttpURL).get(id);
    }

    @Test
    public void testPostCalledVerified() throws IOException {
        // given:
        int userId = 101;
        int articleId = 101;
        String newTitle = "Java";
        String newBodyArticle = "Java 8";

        doNothing().when(mockHttpURL).post(isA(Integer.class), isA(Integer.class),
                isA(String.class), isA(String.class));

        //when:
        mockHttpURL.post(userId, articleId, newTitle, newBodyArticle);

        // then:
        verify(mockHttpURL, times(1))
                .post(userId, articleId, newTitle, newBodyArticle);
    }

}