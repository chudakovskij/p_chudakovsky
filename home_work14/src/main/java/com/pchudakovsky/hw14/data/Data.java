package com.pchudakovsky.hw14.data;

/**
 * Class data.
 *
 * @author Pavel Chudakovsky.
 */
public class Data {

    /**
     * Status code for Get-request.
     */
    public static final int OK = 200;

    /**
     * Status code for Post-request.
     */
    public static final int CREATED = 201;

    /**
     * Web-service.
     */
    public static final String URI = "https://jsonplaceholder.typicode.com/posts/";
}
