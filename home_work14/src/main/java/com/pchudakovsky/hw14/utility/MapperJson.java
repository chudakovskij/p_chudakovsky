package com.pchudakovsky.hw14.utility;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpResponse;

import java.io.IOException;
import java.net.HttpURLConnection;

/**
 * Class mapper.
 *
 * @author Pavel Chudakovsky.
 */
public class MapperJson {

    /**
     * Returns object of type JsonNode.
     *
     * @param connection variable of type HttpURLConnection.
     * @return variable JsonNode.
     * @throws IOException f an I/O error occurs while
     *                     creating the input stream.
     * @see com.pchudakovsky.hw14.service.HttpURLConnectionService
     */
    public static JsonNode jsonNodeForHttpUrl(HttpURLConnection connection)
            throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        JsonNode node = mapper.readTree(connection.getInputStream());
        info(node);
        return node;
    }

    /**
     * Returns object of type JsonNode.
     *
     * @param response variable of type HttpResponse.
     * @return variable of type JsonNode.
     * @throws IOException if entity content cannot be
     *                     represented as {@link java.io.InputStream}.
     * @see com.pchudakovsky.hw14.service.HttpClientService
     */
    public static JsonNode jsonNodeForHttpClient(HttpResponse response)
            throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        JsonNode node = mapper.readTree(response.getEntity().getContent());
        info(node);
        return node;
    }

    /**
     * Provides information for Get-request.
     *
     * @param node object of type JsonNode.
     */
    private static void info(JsonNode node) {
        System.out.println("JSON: " + node.toString() + "\n");
        System.out.println("********");
        System.out.println("*Id user: " + node.get("userId").intValue() + ".\n");
        System.out.println("*Id article: " + node.get("id").intValue() + ".\n");
        System.out.println("*Title article: " + node.get("title").asText() + ".\n");
        System.out.println("*Body article: " + node.get("body").asText() + ".\n");
        System.out.println("********");
    }

    /**
     * Returns array of string.
     *
     * @param node object of type JsonNode.
     * @return array of string that contains
     * information about article.
     */
    public static String[] jsonNodeToStringArray(JsonNode node) {
        return new String[]{node.get("userId").asText(),
                node.get("id").asText(), node.get("title").asText(),
                node.get("body").asText()};
    }
}
