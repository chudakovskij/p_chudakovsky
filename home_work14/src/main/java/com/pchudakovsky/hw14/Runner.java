package com.pchudakovsky.hw14;

import com.pchudakovsky.hw14.api.HttpInterface;
import com.pchudakovsky.hw14.service.HttpClientService;
import com.pchudakovsky.hw14.service.HttpURLConnectionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Scanner;

/**
 * Class Runner provides basic method run() for starts app
 * "article-service".
 *
 * @author Pavel Chudakovsky.
 */
class Runner {
    /**
     *  Used to exit the application.
     */
    private static final int EXIT = 0;

    /**
     * Is used in the method  run()to
     * select a connection using HttpUrlConnection.
     */
    private static final int HTTP_URL_CONNECTION = 1;

    /**
     * Is used in the method  run() to
     * select a connection using HttpClient.
     */
    private static final int HTTP_CLIENT = 2;

    /**
     * Is used for choose method Get-request.
     */
    private static final int GET = 1;

    /**
     * Is used for choose method Post-request.
     */
    private static final int POST = 2;

    /**
     * Logger.
     */
    private static Logger log = LoggerFactory.getLogger(Runner.class);

    /**
     * Calls method run().
     */
    void start() {
        try {
            run();
        } catch (Exception e) {
            log.error("Invalid parameter entered."
                    + "Restart app and enter integer!");
        }
    }

    /**
     * Basic method for starts app.
     *
     * @throws Exception if an I/O exception occurs.
     */
    private void run() throws Exception {
        log.info("App started!");
        try (Scanner scanInt = new Scanner(System.in);
             Scanner scanLine = new Scanner(System.in)) {
            while (true) {
                printInfoStart();
                int value = scanInt.nextInt();
                if (value == HTTP_URL_CONNECTION) {
                    System.out.println("\n........using of HttpURLConnection: ");
                    HttpURLConnectionService httpURL = new HttpURLConnectionService();
                    requestMethod(scanInt, scanLine, httpURL);
                } else if (value == HTTP_CLIENT) {
                    System.out.println("\n ........using of HttpClient: ");
                    HttpClientService httpClient = new HttpClientService();
                    requestMethod(scanInt, scanLine, httpClient);
                } else if (value == EXIT) {
                    System.out.println("Exit");
                    log.info("App finished working!");
                    break;
                }
            }
        }
    }

    /**
     * Prints information at start app.
     */
    private void printInfoStart() {
        System.out.println("\nProgram  Article-Service: GET and POST request");
        System.out.println(" \n For using of HttpURLConnection press 1 ");
        System.out.println(" For using of HttpClient press 2 ");
        System.out.println("Exit-0");
    }

    /**
     * Provides information for choose
     * requestMethod of "GET" or "POST".
     */
    private void printInfoChoice() {
        System.out.println("Testing 1 - Send Http GET request");
        System.out.println("Testing 2 - Send Http POST request");
        System.out.println("Exit - 0");
    }

    /**
     * Selects a request requestMethod: "GET" or "POST".
     *
     * @param scanInt     object of Scanner for input integer.
     * @param scanLine    object of Scanner for input line.
     * @param httpService variable of type  HttpInterface.
     * @throws IOException if an I/O exception occurs.
     */
    private void requestMethod(Scanner scanInt, Scanner scanLine, HttpInterface httpService)
            throws IOException {
        printInfoChoice();
        int value = scanInt.nextInt();
        if (value == GET) {
            get(scanInt, httpService);
        } else if (value == POST) {
            post(scanInt, scanLine, httpService);
        }
    }

    /**
     * Calls a method GET on an  object
     * of type HttpInterface.
     *
     * @param scanner object of type Scanner.
     * @param http    object of type HttpInterface.
     * @throws IOException if an I/O exception occurs.
     */
    private void get(Scanner scanner, HttpInterface http)
            throws IOException {
        System.out.println("Testing 1 - Send Http GET request");
        System.out.println(" Enter value Id: ");
        int id = scanner.nextInt();
        http.get(id);
    }

    /**
     * Calls a method POST on an  object
     * of type HttpInterface.
     *
     * @param scanInt  object of Scanner for input integer.
     * @param scanLine object of Scanner for input line.
     * @param http     object of type HttpInterface.
     * @throws IOException if an I/O exception occurs.
     */
    private void post(Scanner scanInt, Scanner scanLine, HttpInterface http)
            throws IOException {
        System.out.println("\n Testing 2 - POST request");
        System.out.println(" Enter  new value UserId: ");
        int newUserId = scanInt.nextInt();

        System.out.println(" Enter  new value Id: ");
        int newId = scanInt.nextInt();

        System.out.println(" Enter  new  title: ");
        String newTitle = scanLine.nextLine();

        System.out.println(" Enter  new  body: ");
        String newBody = scanLine.nextLine();

        http.post(newUserId, newId, newTitle, newBody);
    }
}

