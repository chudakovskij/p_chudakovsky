package com.pchudakovsky.hw14.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.pchudakovsky.hw14.api.HttpInterface;
import com.pchudakovsky.hw14.utility.MapperJson;
import org.apache.http.HttpException;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.pchudakovsky.hw14.data.Data.CREATED;
import static com.pchudakovsky.hw14.data.Data.OK;
import static com.pchudakovsky.hw14.data.Data.URI;

/**
 * Class for sending HTTP requests and
 * receiving HTTP responses from a resource
 * identified by a URI.
 *
 * @author Pavel Chudakovsky.
 * @see HttpClient
 */
public class HttpClientService implements HttpInterface {

    /**
     * Logger.
     */
    private static Logger log = LoggerFactory
            .getLogger(HttpClientService.class);

    /**
     * Variable for working with JSON.
     */
    private JsonNode jsonNode;

    /**
     * Variable is used for Unit tests.
     */
    private boolean flag;

    /**
     * Returns variable of type boolean.
     *
     * @return boolean value.
     */
    @SuppressWarnings("unused")
    boolean isFlag() {
        return flag;
    }

    /**
     * Setter for variable flag.
     *
     * @param flag variable of type boolean.
     */
    private void setFlag(boolean flag) {
        this.flag = flag;
    }

    /**
     * Method creates of  get-request.
     * Returns array of string that contains
     * *information about article.
     *
     * @param id value for id of article.
     * @return array of string.
     * @throws IOException in case of a problem or
     *                     the connection was aborted.
     */
    public String[] get(int id) throws IOException {
        log.info("method execution get().");
        HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(URI + id);
        HttpResponse response;
        response = client.execute(request);
        int statusCode = response.getStatusLine().getStatusCode();
        checkResponse(statusCode, OK, response);
        return MapperJson.jsonNodeToStringArray(jsonNode);
    }

    /**
     * Method creates of  post-request.
     *
     * @param newUserId new Id for user.
     * @param newId     new Id for article.
     * @param newTitle  new title for article.
     * @param newBody   new body for article.
     * @throws IOException in case of a problem or
     *                     the connection was aborted.
     */
    public void post(int newUserId, int newId, String newTitle, String newBody)
            throws IOException {
        log.info("method execution post().");
        setFlag(false);
        HttpClient client = HttpClientBuilder.create().build();
        HttpPost post = new HttpPost(URI);

        List<NameValuePair> urlParameter = new ArrayList<>();
        urlParameter.add(new BasicNameValuePair("userId", Integer.toString(newUserId)));
        urlParameter.add(new BasicNameValuePair("id", Integer.toString(newId)));
        urlParameter.add(new BasicNameValuePair("title", newTitle));
        urlParameter.add(new BasicNameValuePair("body", newBody));
        post.setEntity(new UrlEncodedFormEntity(urlParameter));

        HttpResponse response = client.execute(post);
        int statusCode = response.getStatusLine().getStatusCode();
        checkResponse(statusCode, CREATED, response);
    }

    /**
     * Checks response status.
     *
     * @param statusCode actual status code.
     * @param code       expected code.
     * @param response   variable of type HttpResponse.
     * @throws IOException if entity content
     *                     cannot be represented
     *                     as {@link java.io.InputStream}
     */
    private void checkResponse(int statusCode, int code,
                               HttpResponse response) throws IOException {
        if (statusCode == code) {
            System.out.println("Response Code : "
                    + statusCode);
            jsonNode = MapperJson.jsonNodeForHttpClient(response);
            setFlag(true);
            log.info("Method finished working!");
        } else {
            try {
                setFlag(false);
                throw new HttpException("Bad request!Create a new request!");
            } catch (HttpException e) {
                log.error("Response code: " + statusCode + ".", e);
            }
        }
    }
}

