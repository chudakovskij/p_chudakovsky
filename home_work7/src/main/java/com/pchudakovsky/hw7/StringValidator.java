package com.pchudakovsky.hw7;

import com.pchudakovsky.hw7.abstract_class.Validator;
import com.pchudakovsky.hw7.exception.ValidationFailedException;

/**
 * Class describing the behavior of a string validator.
 *
 * @author Pavel Chudakovsky.
 */
public class StringValidator extends Validator<String> {

    /**
     * The method checks the validity of
     * a string expression.
     *
     * @param value sting value.
     * @throws ValidationFailedException exception thrown
     *                                   when validation fails.
     */
    @Override
    public final void validate(final String value)
            throws ValidationFailedException {
        String startWith = "^[A-Z].*";
        String emptyLine = "^\\s*$";
        boolean capsLetter = value.matches(startWith);
        boolean lineIsEmpty = value.matches(emptyLine);
        if (!capsLetter | lineIsEmpty) {
            throw new ValidationFailedException(message());
        } else {
            response();
        }
    }

    /**
     * Prints validator response.
     */
    private void response() {
        System.out.println("!!!!****_STRING_VALIDATOR_****!!!!\n"
                + "Message: OK!\n"
                + "***************");
    }

    /**
     * Passes the message to the constructor
     * of class {@link ValidationFailedException}.
     *
     * @return message for exception.
     */
    private String message() {
        String message;
        message = "\n[WARN] The line must begin with a upper-case letter!\n"
                + "[WARN] The line should not be empty!";
        return message;
    }
}


