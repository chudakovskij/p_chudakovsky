package com.pchudakovsky.hw7.exception;

/**
 * Class exception.
 * Used when validation fails.
 *
 * @author Pavel Chudakovsky.
 */
public class ValidationFailedException extends Exception {

    /**
     * Sole constructor.
     *
     * @param s string value.
     */
    public ValidationFailedException(final String s) {
        super(s);
    }
}
