package com.pchudakovsky.hw7;

import com.pchudakovsky.hw7.abstract_class.Validator;
import com.pchudakovsky.hw7.exception.ValidationFailedException;

/**
 * Class describing the behavior of a integer validator.
 *
 * @author Pavel Chudakovsky.
 */
public class IntegerValidator extends Validator<Integer> {
    /**
     * Upper bound of the segment.
     */
    private static final Integer UPPER_BOUND = 10;
    /**
     * Lower bound of the segment.
     */
    private static final Integer LOWER_BOUND = 1;

    /**
     * The method checks the validity of
     * a integer expression.
     *
     * @param value integer value.
     * @throws ValidationFailedException exception thrown
     *                                   when validation fails.
     */
    @Override
    public final void validate(final Integer value)
            throws ValidationFailedException {
        if (value >= LOWER_BOUND & value <= UPPER_BOUND) {
            response();
        } else {
            throw new ValidationFailedException(message());
        }
    }

    /**
     * Prints validator response.
     */
    private void response() {
        System.out.println("!!!!****_INTEGER_VALIDATOR_****!!!!\n"
                + "Message: OK!\n"
                + "***********");
    }

    /**
     * Passes the message to the constructor
     * of class {@link ValidationFailedException}.
     *
     * @return message for exception.
     */
    private String message() {
        String message;
        message = "\n[WARN] The number should be in the range: "
                + LOWER_BOUND + " - " + UPPER_BOUND;
        return message;
    }

}





