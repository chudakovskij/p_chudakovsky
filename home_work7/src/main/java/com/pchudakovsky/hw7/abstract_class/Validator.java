package com.pchudakovsky.hw7.abstract_class;

import com.pchudakovsky.hw7.exception.ValidationFailedException;

/**
 * Generic class.
 *
 * @param <T> this is the type parameter.
 * @author Pavel Chudakovsky.
 */

public abstract class Validator<T> {
    /**
     * Generic method.
     *
     * @param value type parameter T.
     * @throws ValidationFailedException exception thrown
     *                                   then validation fails.
     */
    public abstract void validate(T value) throws ValidationFailedException;
}
