package com.pchudakovsky.hw32;

import java.util.Arrays;

/**
 * Utility class Median represents  static methods
 * for calculating median of arrays.
 *
 * @author Pavel Chudakovsky
 */
class Median {

    /**
     * Static method for calculation median of integer array.
     *
     * @param array integer array
     * @return value for median type of float.
     */
    static float median(int[] array) {
        float result;
        int size = array.length;
        Arrays.sort(array);
        if (size % 2 == 0) {
            result = 0.5f * (array[(size / 2) - 1] + array[size / 2]);
        } else {
            result = array[size / 2];
        }
        return result;
    }

    /**
     * Static method for calculation median of real array.
     *
     * @param array real array type of double.
     * @return value for median type of double.
     */
    static double median(double[] array) {
        double result;
        Arrays.sort(array);
        int size = array.length;
        Arrays.sort(array);
        if (array.length % 2 == 0) {
            result = 0.5 * (array[(size / 2) - 1] + array[size / 2]);
        } else {
            result = array[size / 2];
        }
        return result;
    }
}