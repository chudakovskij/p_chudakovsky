package com.pchudakovsky.hw31;

import com.pchudakovsky.hw31.service.ServiceCard;
import com.pchudakovsky.hw31.service.ServiceCurrency;

import java.math.BigDecimal;

/**
 * Class Run demonstrates the work methods from {@link ServiceCard}
 *
 * @author Pavel Chudakovsky
 */
public class Run {
    public static void main(String[] args) {

        //input parameters
        BigDecimal balance = new BigDecimal("1000.10");
        BigDecimal addMoney = new BigDecimal("0.10");
        BigDecimal withdrawMoney = new BigDecimal("100.0");

        Card card = new Card("Pavel Chudakovsky", balance);
        ServiceCard serviceCard = new ServiceCard(card);

        System.out.println("Demonstration:");

        // Demo: getOwnerName()
        System.out.println("Name of owner: " + serviceCard.getOwnerName());

        // Demo: getBalance()
        System.out.println("Balance: " + serviceCard.getBalance());

        // Demo: addBalance()
        serviceCard.addBalance(addMoney);
        System.out.println("Increase in balance by " + addMoney + " : " + serviceCard.getBalance());

        // Demo: withdrawBalance()
        serviceCard.withdrawBalance(withdrawMoney);
        System.out.println("Decrease in  balance by " + withdrawMoney + ": " + serviceCard.getBalance());

        // Demo: convertBalance()
        ServiceCurrency currencyDollar = new ServiceCurrency(new BigDecimal("2.053"));
        ServiceCurrency currencyRub = new ServiceCurrency(new BigDecimal("0.032"));
        serviceCard.convertBalance("USA", currencyDollar.getExchangeRate());
        serviceCard.convertBalance("RUB", currencyRub.getExchangeRate());
    }


}
