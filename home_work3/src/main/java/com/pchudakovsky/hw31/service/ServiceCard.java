package com.pchudakovsky.hw31.service;

import com.pchudakovsky.hw31.Card;

import java.math.BigDecimal;

/**
 * Class ServiceCard represents of methods work to
 * with bank card {@link Card}.
 *
 * @author Pavel Chudakovsky.
 * @see Card#Card(String, BigDecimal)
 * @see Card#Card(String)
 */
public class ServiceCard {
    /**
     * Variable <b>card</b> provides access to methods from the class Card.
     *
     * @see Card
     */
    private Card card;

    /**
     * Currency balance.
     */
    private BigDecimal balanceCurrency;

    /**
     * Sole constructor.
     *
     * @param card variable type of  {@link Card}.
     */
    public ServiceCard(Card card) {
        this.card = card;
    }

    /**
     * Method returns name of owner
     *
     * @return name of owner as String
     */
    public String getOwnerName() {
        return card.getOwnerName();
    }

    /**
     * Method returns balance in currency.
     *
     * @return balance(type BigDecimal) in currency.
     */
    private BigDecimal getBalanceCurrency() {
        return balanceCurrency;
    }

    /**
     * Method sets balance in currency.
     *
     * @param balanceCurrency balance in currency.
     */
    private void setBalanceCurrency(BigDecimal balanceCurrency) {
        this.balanceCurrency = balanceCurrency;
    }

    /**
     * Returns of balance for bank card.
     *
     * @return value of type BigDecimal.
     */
    public BigDecimal getBalance() {
        return card.getBalance();
    }

    /**
     * Replenishes the cash balance.
     *
     * @param many amount of money.
     */
    public void addBalance(BigDecimal many) {
        card.setBalance(getBalance().add(many));
    }

    /**
     * Method reduces cash balance.
     *
     * @param money amount of money.
     */
    public void withdrawBalance(BigDecimal money) {
        boolean condition1 = (getBalance().compareTo(money) > 0);
        boolean condition2 = (getBalance().compareTo(money) == 0);
        if (condition1 | condition2) {
            card.setBalance(getBalance().subtract(money));
        } else {
            System.out.println("Card balance less than the entered amount.");
        }
    }

    /**
     * Method converts balance to currency.
     *
     * @param nameCurrency name of currency.
     * @param rate         rate of currency.
     * @return balance(type BigDecimal) in currency.
     */
    public BigDecimal convertBalance(String nameCurrency, BigDecimal rate) {
        setBalanceCurrency(getBalance().divide(rate, 2));
        info(nameCurrency, rate);
        return getBalanceCurrency();
    }

    /**
     * Method prints information about currency
     *
     * @param nameCurrency name of currency.
     * @param rate         currency exchange rate as BigDecimal
     */
    private void info(String nameCurrency, BigDecimal rate) {
        System.out.println("*******************************");
        System.out.println("Balance conversion:");
        System.out.println("Currency of " + nameCurrency + ". Rate: " + rate);
        System.out.println("Balance in currency-> " + nameCurrency + ": "
                + getBalanceCurrency());
    }

}



