package com.pchudakovsky.hw31.service;

import java.math.BigDecimal;

/**
 * Class ServiceCurrency   represents of methods work to
 * with currency rate.
 *
 * @author Pavel Chudakovsky
 */
public class ServiceCurrency {

    /**
     * Currency exchange rate.
     */
    private BigDecimal exchangeRate;


    /**
     * Sole constructor
     *
     * @param exchangeRate currency exchange rate
     */
    public ServiceCurrency(BigDecimal exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    /**
     * Method returns currency exchange rate.
     *
     * @return exchange rate as BigDecimal.
     */
    public BigDecimal getExchangeRate() {
        return exchangeRate;
    }

    /**
     * Method sets currency exchange rate
     *
     * @param exchangeRate currency rate of type BigDecimal.
     */
    void setExchangeRate(BigDecimal exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

}
