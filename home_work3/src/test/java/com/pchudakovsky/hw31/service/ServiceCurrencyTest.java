package com.pchudakovsky.hw31.service;

import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;

public class ServiceCurrencyTest {
    private ServiceCurrency currency = new ServiceCurrency(new BigDecimal("100.5"));

    @Test
    public void testGetExchangeRateTest() {
        BigDecimal actual = currency.getExchangeRate();
        BigDecimal expected = new BigDecimal("100.5");
        Assert.assertEquals(expected, actual);

    }

    @Test
    public void testSetExchangeRate() {
        currency.setExchangeRate(new BigDecimal("2000.5"));
        BigDecimal actual = currency.getExchangeRate();
        BigDecimal expected = new BigDecimal("2000.5");
        Assert.assertEquals(expected, actual);
    }
}