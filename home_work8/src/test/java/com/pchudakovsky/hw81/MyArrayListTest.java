package com.pchudakovsky.hw81;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;

public class MyArrayListTest {

    private MyArrayList<String> myArrayList = new MyArrayList<>(11);

    {
        myArrayList.add("Java");
        myArrayList.add("C#");
        myArrayList.add("Delphi");
    }

    @Test
    public void testGetSize() {
        int actual = myArrayList.getSize();
        int expected = 3;
        assertEquals(expected, actual);
    }

    @Test
    public void testAdd() {
        myArrayList.add("C++");
        System.out.println(myArrayList);
        String actual = myArrayList.getElementAt(3);
        String expected = "C++";
        int actualSize = myArrayList.getSize();
        int expectedSize = 4;

        assertEquals(expected, actual);
        assertEquals(expectedSize, actualSize);
    }

    @Test
    public void testAddBack() {
        MyArrayList<String> testList = new MyArrayList<>(5);
        testList.addBack("C++");
        System.out.println(testList);
        String actual = testList.getElementAt(5);
        String expected = "C++";
        int actualSize = testList.getSize();
        int expectedSize = 1;

        assertEquals(expected, actual);
        assertEquals(expectedSize, actualSize);
    }

    @Test
    public void testRemoveFirst() {
        System.out.println(myArrayList);
        myArrayList.removeFirst();
        String expected = myArrayList.getElementAt(0);
        assertNull(expected);
    }

    @Test
    public void testRemoveLast() {
        System.out.println(myArrayList);
        myArrayList.removeLast();
        String expected = myArrayList.getElementAt(4);
        assertNull(expected);
    }

    @Test
    public void testSet() {
        myArrayList.set("Pascal", 2);
        System.out.println(myArrayList);
        String actual = myArrayList.getElementAt(2);
        String expected = "Pascal";
        assertEquals(expected, actual);
    }

    @Test
    public void testRemoveByIndex() {
        myArrayList.removeByIndex(0);
        String expected = myArrayList.getElementAt(0);
        assertNull(expected);
    }


    @Test
    public void testRemoveByValue() {
        System.out.println(myArrayList);
        myArrayList.removeByValue("Delphi");
        String expected = myArrayList.getElementAt(3);
        assertNull(expected);
    }

    @Test
    public void testRemoveAll() {
        myArrayList.add("CSS");
        myArrayList.add("CSS");
        myArrayList.add("CSS");
        myArrayList.removeAll("CSS");

        String expected1 = myArrayList.getElementAt(3);
        String expected2 = myArrayList.getElementAt(4);
        String expected3 = myArrayList.getElementAt(5);
        assertNull(expected1);
        assertNull(expected2);
        assertNull(expected3);
    }

    @Test
    public void isEmpty() {
        assertFalse(myArrayList.isEmpty());
    }

    @Test
    public void indexOf() {
        int expected;
        System.out.println("Test indexOf(): ");
        myArrayList.set("JavaTest", 1);
        int actual = myArrayList.indexOf("JavaTest");
        expected = 1;
        Assert.assertEquals(expected, actual);
        System.out.println("Insert: JavaTest; expected: " + expected);
        System.out.println("*******************");
    }

    @Test
    public void lastIndexOf() {
        int expected;
        System.out.println("Test indexOf(): ");
        myArrayList.add("HTML");
        myArrayList.set("JavaTest", 2);
        int actual = myArrayList.lastIndexOf("JavaTest");
        expected = 2;
        Assert.assertEquals(expected, actual);
        System.out.println("Insert: JavaTest; expected: " + expected);
        System.out.println("*******************");
    }

    @Test
    @SuppressWarnings(value = "unchecked")
    public void equals() {
        MyArrayList<String> m1 = new MyArrayList<>(10);
        MyArrayList<String> m2 = new MyArrayList<>(10);
        MyArrayList<String> m3 = new MyArrayList<>(10);
        System.out.println("Test equals(): ");

        m1.set("JavaTest", 8);
        try {
            m2 = (MyArrayList<String>) m1.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        System.out.println("m1_ " + m1.toString());
        System.out.println("m2_ " + m2.toString());
        System.out.println("m3_ " + m3.toString());

        Assert.assertTrue(m1.equals(m2));
        Assert.assertFalse(m1.equals(m3));
        System.out.println("*******************");
    }

    @Test
    public void getElementAt() {
        String actual = myArrayList.getElementAt(2);
        String expected = "Delphi";
        assertEquals(expected, actual);
    }

    @Test
    public void testClear() {
        myArrayList.clear();
        String expected0 = myArrayList.getElementAt(0);
        String expected1 = myArrayList.getElementAt(1);
        String expected2 = myArrayList.getElementAt(2);
        assertNull(expected0);
        assertNull(expected1);
        assertNull(expected2);
    }

    @Test
    public void testTrimToSize() {
        MyArrayList<String> list = new MyArrayList<>(10);
        list.add("A");
        list.add("B");
        list.add("C");
        list.trimToSize();
        int actual = list.getCapacity();
        int expected = 3;
        assertEquals(expected, actual);
    }
}