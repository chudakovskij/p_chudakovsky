package com.pchudakovsky.hw82;

import com.pchudakovsky.hw82.data.BrigadeInitialization;
import com.pchudakovsky.hw82.data.TenderInitialization;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;

public class TenderTest {

    private Brigade brigade1 = BrigadeInitialization.brigadeOne();
    private Brigade brigade2 = BrigadeInitialization.brigadeTwo();
    private Brigade brigade3 = BrigadeInitialization.brigadeThird();

    private BigDecimal finance = new BigDecimal("3000.0");
    private Tender tender = TenderInitialization.getTender(finance);
    private List<Brigade> brigadeForTender = new ArrayList<>();
    private Map<Brigade, List<Profession>> brigadeMap = new HashMap<>();

    {
        brigadeForTender.add(brigade1);
        brigadeForTender.add(brigade2);
        brigadeForTender.add(brigade3);
        tender.setListBrigade(brigadeForTender);
    }

    @Test
    public void testBrigadeAndProfessionOnTrueKeyAndValue() {
        brigadeMap = tender.brigadeAndProfession(brigadeForTender);

        Assert.assertTrue(brigadeMap.containsKey(brigade1));
        Assert.assertTrue(brigadeMap.containsValue(brigadeMap.get(brigade1)));

        Assert.assertTrue(brigadeMap.containsKey(brigade2));
        Assert.assertTrue(brigadeMap.containsValue(brigadeMap.get(brigade2)));

        Assert.assertTrue(brigadeMap.containsKey(brigade3));
        Assert.assertTrue(brigadeMap.containsValue(brigadeMap.get(brigade3)));
    }

    @Test
    public void testBrigadeAndProfessionOnValue() {
        brigadeMap = tender.brigadeAndProfession(brigadeForTender);
        List<Profession> actualProfessions = brigadeMap.get(brigade1);
        List<Profession> expected = new ArrayList<>();
        expected.add(Profession.PLASTERER);
        expected.add(Profession.CARPENTER);
        expected.add(Profession.MOLAR);
        Collections.sort(expected);
        Assert.assertEquals(expected, actualProfessions);
    }

    @Test
    public void testGetWinnerOnMinimalSalary() {
        brigadeMap = tender.brigadeAndProfession(brigadeForTender);
        Optional<Map.Entry<Brigade, List<Profession>>> result = tender.getWinner(brigadeMap, tender);
        boolean minSalary = result.isPresent();
        Assert.assertTrue(minSalary);
        Assert.assertThat(result.get().getKey().getBrigadeSalary(), is(new BigDecimal("201")));
        Assert.assertThat(result.get().getKey().getNumber(), is(1));
    }

    @Test
    public void testGetWinnerOnBrigadeNumberWithMinimalSalary() {
        brigadeMap = tender.brigadeAndProfession(brigadeForTender);
        Optional<Map.Entry<Brigade, List<Profession>>> result = tender.getWinner(brigadeMap, tender);
        boolean minSalary = result.isPresent();
        Assert.assertTrue(minSalary);
        Assert.assertThat(result.get().getKey().getNumber(), is(1));
    }
}