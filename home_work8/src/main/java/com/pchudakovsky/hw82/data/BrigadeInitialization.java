package com.pchudakovsky.hw82.data;

import com.pchudakovsky.hw82.Brigade;
import com.pchudakovsky.hw82.Profession;
import com.pchudakovsky.hw82.Worker;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Initialisation class.
 *
 * @author Pavel hudakovsky.
 */
public class BrigadeInitialization {

    /**
     * Returns first brigade.
     *
     * @return value type of Brigade
     */
    public static Brigade brigadeOne() {
        Worker ivanov = new Worker.WorkerBuilder(new Worker()).setFamily("Ivanov").
                setProfession(Profession.CARPENTER).setSalary(new BigDecimal("100")).build();

        Worker petrov = new Worker.WorkerBuilder(new Worker()).setFamily("Petrov").
                setProfession(Profession.MOLAR).setSalary(new BigDecimal("100")).build();
        Worker sidorov = new Worker.WorkerBuilder(new Worker()).setFamily("Sidorov").
                setProfession(Profession.PLASTERER).setSalary(new BigDecimal("1")).build();

        List<Worker> workerList1 = new ArrayList<>();
        workerList1.add(ivanov);//carpenter
        workerList1.add(petrov);//molar
        workerList1.add(sidorov);//plasterer

        return new Brigade.BrigadeBuilder(new Brigade())
                .setNumber(1).amountWorker(3).setListWorker(workerList1)
                .brigadeSalary(workerList1).build();
    }

    /**
     * Returns second brigade.
     *
     * @return value type of Brigade
     */
    public static Brigade brigadeTwo() {

        Worker pavlov = new Worker.WorkerBuilder(new Worker()).setFamily("Pavlov").
                setProfession(Profession.CARPENTER).setSalary(new BigDecimal("100")).build();

        Worker fedorov = new Worker.WorkerBuilder(new Worker()).setFamily("Fedorov")
                .setProfession(Profession.MOLAR).setSalary(new BigDecimal("100")).build();

        Worker vasiliev = new Worker.WorkerBuilder(new Worker()).setFamily("Vasiliev").
                setProfession(Profession.PLASTERER).setSalary(new BigDecimal("100")).build();

        List<Worker> workerList2 = new ArrayList<>();
        workerList2.add(pavlov);  // CARPENTER
        workerList2.add(fedorov);// MOLAR
        workerList2.add(vasiliev);// PLASTERER

        return new Brigade.BrigadeBuilder(new Brigade())
                .setNumber(2).amountWorker(3).setListWorker(workerList2)
                .brigadeSalary(workerList2).build();
    }

    /**
     * Returns third brigade.
     *
     * @return value type of Brigade
     */
    public static Brigade brigadeThird() {
        Worker pavlov = new Worker.WorkerBuilder(new Worker()).setFamily("Pavlov").
                setProfession(Profession.CARPENTER).setSalary(new BigDecimal("100")).build();
        Worker petrov = new Worker.WorkerBuilder(new Worker()).setFamily("Petrov").
                setProfession(Profession.MOLAR).setSalary(new BigDecimal("100")).build();

        Worker aleksandrov = new Worker.WorkerBuilder(new Worker()).setFamily("Aleksandrov").
                setProfession(Profession.PLASTERER).setSalary(new BigDecimal("10")).build();

        List<Worker> workerList3 = new ArrayList<>();
        workerList3.add(pavlov);// CARPENTER
        workerList3.add(petrov);//MOLAR
        workerList3.add(aleksandrov);//CARPENTER

        return new Brigade.BrigadeBuilder(new Brigade())
                .setNumber(3).amountWorker(3).setListWorker(workerList3)
                .brigadeSalary(workerList3).build();
    }

}
