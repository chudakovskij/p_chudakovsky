package com.pchudakovsky.hw82.data;

import com.pchudakovsky.hw82.Profession;
import com.pchudakovsky.hw82.Tender;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Class initialization for Tender.
 */
public class TenderInitialization {

    /**
     * Returns object of type Tender.
     *
     * @param finance financial condition.
     * @return value of type Tender.
     */
    public static Tender getTender(BigDecimal finance) {

        Tender tender = new Tender.TenderBuilder(new Tender(finance))
                .firstProfession(Profession.MOLAR).secondProfession(Profession.CARPENTER)
                .thirdProfession(Profession.PLASTERER).build();

//  Create a list of profession
        List<Profession> professionForTender = new ArrayList<>();
        professionForTender.add(tender.getFirstProfession());
        professionForTender.add(tender.getSecondProfession());
        professionForTender.add(tender.getThirdProfession());
        tender.setProfession(professionForTender);
        return tender;
    }
}
