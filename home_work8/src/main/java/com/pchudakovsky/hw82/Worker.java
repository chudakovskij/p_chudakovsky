package com.pchudakovsky.hw82;

import java.math.BigDecimal;

/**
 * Class describing the behavior of a "Worker".
 *
 * @author Pavel Chudakovsky.
 */
@SuppressWarnings("unused")
public class Worker {

    /**
     * Family of  worker.
     */
    private String family;
    /**
     * Salary of worker.
     */
    private BigDecimal salary;
    /**
     * Profession of worker.
     */
    private Profession profession;


    // Getters and setters
    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    BigDecimal getSalary() {
        return salary;
    }

    public void setSalary(BigDecimal salary) {
        this.salary = salary;
    }

    Profession getProfession() {
        return profession;
    }

    public void setProfession(Profession profession) {
        this.profession = profession;
    }

    /**
     * Class Builder for Worker.
     */

    public static class WorkerBuilder {

        /**
         * Value of type Worker.
         */
        private Worker newWorker;

        /**
         * Sole constructor.
         *
         * @param newWorker value of type Worker.
         */
        public WorkerBuilder(Worker newWorker) {
            this.newWorker = newWorker;
        }

        /**
         * Sets family.
         *
         * @param family string value.
         * @return type WorkerBuilder.
         */
        public WorkerBuilder setFamily(String family) {
            newWorker.family = family;
            return this;
        }


        /**
         * Sets profession.
         *
         * @param profession profession of worker
         *                   (Type Profession).
         * @return type WorkerBuilder.
         */
        public WorkerBuilder setProfession(Profession profession) {
            newWorker.profession = profession;
            return this;
        }

        /**
         * Sets salary of worker.
         *
         * @param salary salary of Worker.
         * @return type Worker Builder.
         */
        public WorkerBuilder setSalary(BigDecimal salary) {
            newWorker.salary = salary;
            return this;
        }

        /**
         * Returns Worker.
         *
         * @return value of type Worker.
         */
        public Worker build() {
            return newWorker;
        }

    }
}

