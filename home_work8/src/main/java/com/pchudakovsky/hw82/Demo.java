package com.pchudakovsky.hw82;

import com.pchudakovsky.hw82.data.BrigadeInitialization;
import com.pchudakovsky.hw82.data.TenderInitialization;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Test class.
 *
 * @author Pavel Chudakovsky.
 */
public class Demo {

    public static void main(String[] args) {

        System.out.println("** Demo Tender **");
        // Create brigades
        Brigade brigade1 = BrigadeInitialization.brigadeOne();
        Brigade brigade2 = BrigadeInitialization.brigadeTwo();
        Brigade brigade3 = BrigadeInitialization.brigadeThird();

        // Create tender
        BigDecimal finance = new BigDecimal("3000.0");
        Tender tender = TenderInitialization.getTender(finance);
        List<Brigade> brigadeForTender = new ArrayList<>();

        brigadeForTender.add(brigade1);
        brigadeForTender.add(brigade2);
        brigadeForTender.add(brigade3);
        tender.setListBrigade(brigadeForTender);

        // Result of tender
        Map<Brigade, List<Profession>> brigadeMap;
        brigadeMap = tender.brigadeAndProfession(brigadeForTender);
        tender.printPretenders(brigadeMap, tender);
        tender.getWinner(brigadeMap, tender);
    }
}
