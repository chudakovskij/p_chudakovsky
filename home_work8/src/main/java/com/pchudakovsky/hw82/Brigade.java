package com.pchudakovsky.hw82;

import java.math.BigDecimal;
import java.util.List;

/**
 * Class describing the behavior of a "Brigade".
 *
 * @author Pavel Chyuadkovsky.
 */
@SuppressWarnings("unused")
public class Brigade {

    /**
     * Number of brigade.
     */
    private int number;
    /**
     * Amount of Worker.
     */
    private int amountWorkers;
    /**
     * Salary of Brigade.
     */
    private BigDecimal brigadeSalary;
    /**
     * Lists of workers.
     */
    private List<Worker> listWorker;

    // Getters and setters.
    int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getAmountWorkers() {
        return amountWorkers;
    }

    public void setAmountWorkers(int amountWorkers) {
        this.amountWorkers = amountWorkers;
    }

    BigDecimal getBrigadeSalary() {

        return brigadeSalary;
    }


    public void setBrigadeSalary(BigDecimal brigadeSalary) {
        this.brigadeSalary = brigadeSalary;
    }


    List<Worker> getListWorker() {
        return listWorker;
    }

    public void setListWorker(List<Worker> listWorker) {
        this.listWorker = listWorker;
    }


    /**
     * Class Builder for Brigade.
     *
     * @author Pavel Chudakovsky.
     */

    public static class BrigadeBuilder {

        /**
         * Value of type Brigade.
         */
        private Brigade brigade;

        /**
         * Sole constructor.
         *
         * @param brigade value of type Brigade.
         */
        public BrigadeBuilder(Brigade brigade) {
            this.brigade = brigade;
        }

        /**
         * Sets number of brigade.
         *
         * @param number number of brigade.
         * @return value of type Brigade Builder.
         */
        public BrigadeBuilder setNumber(int number) {
            brigade.number = number;
            return this;
        }

        /**
         * Sets amount of Worker.
         *
         * @param amount amount fo Worker.
         * @return value of type Brigade Builder.
         */
        public BrigadeBuilder amountWorker(int amount) {
            brigade.amountWorkers = amount;
            return this;
        }


        /**
         * Sets salary of brigade.
         *
         * @param listWorker value of type List<Worker>.
         * @return value of type Brigade Builder.
         */
        public BrigadeBuilder brigadeSalary(List<Worker> listWorker) {
            BigDecimal salary = new BigDecimal("0");
            for (Worker workers : listWorker) {
                salary = salary.add(workers.getSalary());
            }
            brigade.brigadeSalary = salary;
            return this;
        }

        /**
         * Sets list of workers for brigade.
         *
         * @param list value of type List<Worker>
         * @return value of type Brigade Builder.
         */
        public BrigadeBuilder setListWorker(List<Worker> list) {
            brigade.listWorker = list;
            return this;
        }

        /**
         * Returns brigade.
         *
         * @return value of type Brigade.
         */
        public Brigade build() {
            return brigade;
        }

    }

}
