package com.pchudakovsky.hw81;

import java.util.Arrays;
import java.util.Random;

/**
 * Class describing the behavior of a "ArrayList".
 *
 * @param <T> this is the type parameter.
 * @author Pavel Chudakovsky.
 */
public class MyArrayList<T> implements Cloneable {

    /**
     * The number of  default elements in the array.
     */
    private static final int DEFAULT_SIZE = 0;
    /**
     * Default array capacity.
     */
    private static final int DEFAULT_CAPACITY = 10;

    /**
     * Generic array
     */
    private T[] data;
    /**
     * The number of  elements in the array.
     */
    private int size;
    /**
     * Array capacity.
     */
    private int capacity;

    /**
     * Constructor sets of capacity array.
     *
     * @param capacity array capacity.
     */
    @SuppressWarnings("unchecked")
    MyArrayList(int capacity) {
        this.capacity = capacity;
        this.size = DEFAULT_SIZE;
        this.data = (T[]) new Object[capacity];
    }

    /**
     * constructor with empty parameters
     */
    @SuppressWarnings({"unchecked", "unused"})
    MyArrayList() {
        this.capacity = DEFAULT_CAPACITY;
        this.size = DEFAULT_SIZE;
        data = (T[]) new Object[capacity];
    }

    /**
     * Method  returns size for array.
     *
     * @return size of array.
     */
    int getSize() {
        this.size = data.length - capacity;
        return this.size;
    }

    /**
     * Method returns capacity for array.
     *
     * @return capacity of array.
     */
    int getCapacity() {
        return capacity;
    }

    /**
     * Prints data about array.
     *
     * @return string value.
     */
    @Override
    public String toString() {
        return "data:" + Arrays.toString(data);
    }

    /**
     * Redistributes capacity of array.
     */
    private void ensureCapacity() {
        if (getSize() > capacity) {
            capacity = (int) (capacity * 1.5) + 1;
            T[] newData;
            newData = Arrays.copyOf(data, capacity);
            data = newData;
        }
    }

    /**
     * Adds an element to the end of the array.
     *
     * @param value this is the type parameter.
     */
    void addBack(T value) {
        ensureCapacity();
        T[] arrayNew = Arrays.copyOf(data, data.length + 1);
        data = arrayNew;
        System.out.println(Arrays.toString(arrayNew));
        data[data.length - 1] = value;
        System.out.println("add()_  " + toString());
    }

    /**
     * Adds an element to  the array.
     *
     * @param value this is the type parameter.
     */
    void add(T value) {
        ensureCapacity();
        T[] arrayNew = Arrays.copyOf(data, data.length + 1);
        System.out.println(Arrays.toString(arrayNew));
        data = arrayNew;
        data[getSize() - 1] = value;
        System.out.println("add()_  " + toString());
    }

    /**
     * Removes the first element of an array
     */
    void removeFirst() {
        data[0] = null;
        System.out.println("removeFirst()_   " + toString());
    }

    /**
     * Sets value in array by index.
     *
     * @param value value of element.
     * @param index index of element.
     */
    void set(T value, int index) {
        if ((index <= data.length) && (index >= 0))
            data[index] = value;
        System.out.println("set()_   " + toString());
    }

    /**
     * Removes of element from array by index.
     *
     * @param index index of element.
     */
    void removeByIndex(int index) {
        if ((index <= data.length) && (index >= 0))
            data[index] = null;
        System.out.println("removeByIndex()_   " + toString());
    }

    /**
     * Removes element from array by value.
     *
     * @param value value of element.
     */
    void removeByValue(T value) {
        for (int i = 0; i <= data.length - 1; i++) {
            if (data[i] == value) {
                data[i] = null;
                break;
            }
        }
        System.out.println("removeByValue()_   " + toString());
    }

    /**
     * Removes all elements with value "value".
     *
     * @param value value of elements.
     */
    void removeAll(T value) {
        for (int i = 0; i <= data.length - 1; i++) {
            if (data[i] == value) {
                data[i] = null;
            }
        }
        System.out.println("removeAll()_   " + toString());
    }

    /**
     * Removes last element.
     */
    void removeLast() {
        data[data.length - 1] = null;
        System.out.println("removeLast()_   " + toString());
    }

    /**
     * Removes all elements.
     */
    void clear() {
        for (int i = 0; i <= data.length - 1; i++) {
            data[i] = null;
        }
        size = 0;
        System.out.println("clear():");
        System.out.println("size= " + size);
        System.out.println("null_" + toString());
    }

    /**
     * Checks array on empty elements.
     *
     * @return boolean value.
     */
    @SuppressWarnings("RedundantIfStatement")
    boolean isEmpty() {
        boolean a;
        if (getSize() == 0) {
            a = true;
        } else {
            a = false;
        }
        return a;
    }

    /**
     * Equates capacity to size.
     */
    void trimToSize() {
        capacity = getSize();
        data = Arrays.copyOf(data, capacity);
        System.out.println("trimToSize: " + Arrays.toString(data));
    }

    /**
     * Linear search from left to
     * right of the first entry into the array
     * of the specified element.
     *
     * @param value value element.
     * @return index element.
     */
    int indexOf(T value) {
        int index = 0;
        int count = 0;
        for (int i = 1; i <= data.length - 1; i++) {
            count += 1;
            if (data[i] == value) {
                index = count;
                break;
            } else {
                index = -1;
            }
        }
        return index;
    }

    /**
     * Linear search from right to
     * left of the first entry into the array
     * of the specified element.
     *
     * @param value value element.
     * @return index element.
     */
    @SuppressWarnings("SameParameterValue")
    int lastIndexOf(T value) {
        int index = 0;
        int count = data.length;
        for (int i = data.length - 1; i >= 0; i--) {
            count = count - 1;
            if (data[i] == value) {
                index = count;
                break;
            } else {
                index = -1;
            }
        }
        return index;
    }

    /**
     * Reorders elements in an array.
     *
     * @return array data.
     */
    @SuppressWarnings("unused")
    T[] reverse() {
        T[] arrayNew = Arrays.copyOf(data, data.length);
        for (int i = 0, j = arrayNew.length - 1; i <= data.length - 1 & j >= 0; i++, j--) {
            data[i] = arrayNew[j];
        }
        return data;
    }

    /**
     * Randomly mixes array elements.
     *
     * @return array data.
     */
    @SuppressWarnings("unused")
    T[] shuffle() {
        T[] arrayNew = Arrays.copyOf(data, data.length);
        Random rand = new Random();
        for (int i = 0; i <= data.length - 1; i++) {
            int j = rand.nextInt(data.length);
            data[i] = arrayNew[j];
        }
        return data;
    }

    /**
     * Compares arrays.
     *
     * @param myArr2 value of type MyArrayList
     * @return boolean value.
     */
    boolean equals(MyArrayList<T> myArr2) {
        return (data.length == myArr2.data.length)
                & (data == myArr2.data);
    }

    /**
     * Returns element of array by index.
     *
     * @param index index element.
     * @return value element.
     */
    T getElementAt(int index) {
        if ((index <= data.length - 1) & (index >= 0)) {
            return data[index];
        } else {
            return null;
        }
    }

    /**
     * Returns an array clone.
     *
     * @return copy of array.
     * @throws CloneNotSupportedException thrown when trying to clone
     *                                    an object that does not implement
     *                                    the Cloneable interface.
     */
    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}

