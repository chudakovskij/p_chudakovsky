package com.pchudakovsky.hw41;

import com.pchudakovsky.hw41.card.Card;
import com.pchudakovsky.hw41.card.CreditCard;
import com.pchudakovsky.hw41.card.DebitCard;

import java.math.BigDecimal;

/**
 * This class for launch app.
 *
 * @author Pavel Chudakovsky.
 */
public class RunAtm {

    public static void main(String[] args) {

        // initialization of credit card and debit card
        Card creditCard = new CreditCard("Pavel Chudakovsky", new BigDecimal("1000"));
        Card debitCard = new DebitCard("Ivan Ivanov", new BigDecimal("1100"));

        // Run ATM
        Atm atm = new Atm(creditCard, debitCard);
        atm.run();
    }
}
