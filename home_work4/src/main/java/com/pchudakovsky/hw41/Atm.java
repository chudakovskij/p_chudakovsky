package com.pchudakovsky.hw41;

import com.pchudakovsky.hw41.card.Card;
import com.pchudakovsky.hw41.service.ServiceCreditCard;
import com.pchudakovsky.hw41.service.ServiceDebitCard;
import com.pchudakovsky.hw41.service.api.ServiceCard;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Scanner;

/**
 * Class models the behavior of an ATM.
 *
 * @author Pavel Chudakovsky.
 */
class Atm {

    /**
     * Indicates application termination.
     */
    private static final String EXIT = "0";
    /**
     * Debit card indicator.
     */
    private static final String DEBIT_CARD = "1";
    /**
     * Credit card indicator.
     */
    private static final String CREDIT_CARD = "2";
    /**
     * Cash replenishment indicator.
     */
    private static final String ADD = "3";
    /**
     * Withdrawal indicator.
     */
    private static final String WITHDRAW = "4";
    /**
     * Field  creditCard appears as credit card.
     */
    private Card creditCard;
    /**
     * Field debitCard appears as debit card.
     */
    private Card debitCard;

    /**
     * Sole constructor.
     *
     * @param creditCard as credit card
     * @param debitCard  as debit card
     */
    Atm(Card creditCard, Card debitCard) {
        this.creditCard = creditCard;
        this.debitCard = debitCard;
    }

    /**
     * Presents a strategy for choosing
     * a service depending on the type of bank card.
     */
    void run() {
        try (Scanner scanner = new Scanner(System.in)) {
            while (true) {
                info();
                String keyCard = scanner.nextLine();
                if (EXIT.equals(keyCard)) {
                    break;
                }
                ServiceCard service = getService(keyCard);
                printOperationMessage();
                String operation = scanner.nextLine();
                if (ADD.equals(operation)) {
                    service.addBalance(toBigDecimal(putMoney(scanner)));
                } else if (WITHDRAW.equals(operation)) {
                    service.withdrawBalance(toBigDecimal(putMoney(scanner)));
                }
            }
        }
    }

    /**
     * Used to enter a sum of money from the keyboard.
     *
     * @param scanner object of class Scanner
     * @return object of Scanner
     */
    private String putMoney(Scanner scanner) {
        System.out.println("Enter the amount of money:");
        return scanner.nextLine();
    }

    /**
     * Returns ServiceCard type variable from HashMap.
     *
     * @param keyService string type variable
     * @return ServiceCard type variable
     */
    private ServiceCard getService(String keyService) {
        HashMap<String, ServiceCard> map = new HashMap<>();
        map.put(DEBIT_CARD, new ServiceDebitCard(debitCard));
        map.put(CREDIT_CARD, new ServiceCreditCard(creditCard));
        return map.get(keyService);
    }

    /**
     * Prints information.
     */
    private void info() {
        System.out.println("\n **** ATM ****" +
                "\n Choose the type of bank card: \n 1-Debit card "
                + "\n 2-Credit card " + "\n 0-exit");
    }

    /**
     * Prints information for a select operation.
     */
    private void printOperationMessage() {
        System.out.println("To replenish a cash account, press 3\n"
                + "To withdraw from the cash account, press 4");

    }

    /**
     * Converts string to BigDecimal.
     *
     * @param value string variable
     * @return variable type BigDecimal
     */
    private BigDecimal toBigDecimal(String value) {
        return new BigDecimal(value);
    }
}



