package com.pchudakovsky.hw41.card;

import java.math.BigDecimal;

/**
 * Class Card describes the entity of a bank card
 * that store name of owner and balance.
 *
 * @author Pavel Chudakovsky.
 */

public class Card {

    /**
     * Name  for owner of bank card.
     */
    private String ownerName;

    /**
     * Balance of bank card.
     */
    private BigDecimal balance;

    /**
     * The first constructor to create of object for class Card
     * by name of owner and  balance.
     *
     * @param ownerName name for owner.
     * @param balance   balance of bank card.
     */
    public Card(String ownerName, BigDecimal balance) {
        this.ownerName = ownerName;
        this.balance = balance;
    }

    /**
     * The second constructor to create of object for class Card
     * by name of owner of bank card.
     *
     * @param ownerName name for owner of bank card.
     */
    public Card(String ownerName) {
        this.ownerName = ownerName;
    }

    // Getters and setters for fields.
    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }
}
