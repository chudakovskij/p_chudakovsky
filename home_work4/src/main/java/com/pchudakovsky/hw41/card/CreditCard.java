package com.pchudakovsky.hw41.card;

import java.math.BigDecimal;

/**
 * Class CreditCard describes the entity of a credit card
 * that store name of owner and balance.
 *
 * @author Pavel Chudakovsky.
 */
public class CreditCard extends Card {

    /**
     * Sole constructor.
     *
     * @param ownerName name of owner(String)
     * @param balance   as BigDecimal
     */
    public CreditCard(String ownerName, BigDecimal balance) {
        super(ownerName, balance);
    }
}