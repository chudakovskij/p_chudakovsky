package com.pchudakovsky.hw41.card;

import java.math.BigDecimal;

/**
 * Class DebitCard describes the entity of a debit card
 * that store name of owner and balance.
 *
 * @author Pavel Chudakovsky.
 */
public class DebitCard extends Card {
    /**
     * Sole constructor.
     *
     * @param ownerName name of owner(String)
     * @param balance   as BigDecimal
     */
    public DebitCard(String ownerName, BigDecimal balance) {
        super(ownerName, balance);
    }
}
