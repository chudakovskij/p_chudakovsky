package com.pchudakovsky.hw41.service;

import com.pchudakovsky.hw41.card.Card;

import java.math.BigDecimal;

/**
 * Class ServiceCreditCard represents of methods work to
 * * with credit card {@link ServiceCreditCard}.
 * This class inherits methods from class {@link ServiceDebitCard}.
 *
 * @author Pavel Chudakovsky.
 * @see com.pchudakovsky.hw41.service.api.ServiceCard
 * @see com.pchudakovsky.hw41.card.DebitCard
 */
public class ServiceCreditCard extends ServiceDebitCard {

    /**
     * Variable <b>cardCredit</b> provides access to methods
     * from the class Card.
     */
    private Card cardCredit;

    /**
     * Sole constructor.
     *
     * @param cardCredit reference type variable
     */
    public ServiceCreditCard(Card cardCredit) {
        super(cardCredit);
        this.cardCredit = cardCredit;
    }

    /**
     * Overridden method
     *
     * @param money amount of money.
     * @see ServiceDebitCard#withdrawBalance(BigDecimal)
     */
    @Override
    public void withdrawBalance(BigDecimal money) {
        BigDecimal newBalance = cardCredit.getBalance().subtract(money);
        cardCredit.setBalance(newBalance);
        System.out.println("Actual balance: " + getBalance());

    }

}
