package com.pchudakovsky.hw41.service.api;

import java.math.BigDecimal;

/**
 * Contains methods that are
 * implemented in {@link com.pchudakovsky.hw41.service.ServiceDebitCard}.
 *
 * @author Pavel Chudakovsky.
 */
public interface ServiceCard {

    void setBalanceCurrency(BigDecimal balanceCurrency);

    BigDecimal getBalanceCurrency();

    BigDecimal getBalance();

    void addBalance(BigDecimal many);

    void withdrawBalance(BigDecimal money);

    BigDecimal convertBalance(String nameCurrency, BigDecimal rate);

    String getOwnerName();
}
