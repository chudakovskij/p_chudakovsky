package com.pchudakovsky.hw42;

import com.pchudakovsky.hw42.api.Sorter;

import java.util.Arrays;

/**
 * Represents a method for sorting an array(Bubble sort).
 *
 * @author Pavel Chudakovsky
 */
public class BubbleSort implements Sorter {

    /**
     * Bubble sort.
     *
     * @param array integer array
     * @return sorted array
     */
    @Override
    public int[] sort(int[] array) {
        int[] copyArray = Arrays.copyOf(array, array.length);
        int size = copyArray.length;
        for (int i = size - 1; i >= 1; i--) {
            for (int j = 0; j < i; j++) {
                if (copyArray[j] > copyArray[j + 1]) {
                    int memoryElement = copyArray[j + 1];
                    copyArray[j + 1] = copyArray[j];
                    copyArray[j] = memoryElement;
                }
            }
        }
        printArray(copyArray);
        return copyArray;
    }

    /**
     * Prints message about sorted array.
     *
     * @param array sorted array
     */
    private void printArray(int[] array) {
        System.out.println("Bubble sort: " + Arrays.toString(array));
    }
}
