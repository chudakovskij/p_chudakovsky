package com.pchudakovsky.hw42.api;

/**
 * Contains   one method  for sort of  array.
 *
 * @author Pavel Chudakovsky.
 * @see com.pchudakovsky.hw42.BubbleSort
 * @see com.pchudakovsky.hw42.SelectionSort
 */
public interface Sorter {
    int[] sort(int[] array);
}
