package com.pchudakovsky.hw42;

import com.pchudakovsky.hw42.api.Sorter;

import java.util.HashMap;
import java.util.Scanner;

/**
 * The class provides an array sorting selection strategy.
 *
 * @author Pavel Chudakovsky.
 */
class SortingContext {

    /**
     * Bubble sort indicator.
     */
    private final static String BUBBLE_SORT = "1";
    /**
     * Selection sort indicator.
     */
    private final static String SELECTION_SORT = "2";
    /**
     * Indicates application termination.
     */
    private final static String EXIT = "0";
    /**
     * Reference type Sorter.
     *
     * @see Sorter
     */
    private Sorter sortStrategy;

    /**
     * Sole constructor.
     *
     * @param sortStrategy reference type Sorter
     */
    SortingContext(Sorter sortStrategy) {
        this.sortStrategy = sortStrategy;
    }

    /**
     * Run method {@link SortingContext#execute(int[])}.
     *
     * @param array integer array
     */
    void run(int[] array) {
        execute(array);
    }

    /**
     * Selects a sorting strategy at application startup.
     *
     * @param array integer array
     */
    private void execute(int[] array) {
        try (Scanner scanner = new Scanner(System.in)) {
            while (true) {
                printInfo();
                String keySort = scanner.nextLine();
                if (EXIT.equals(keySort)) {
                    break;
                }
                this.sortStrategy = getSortStrategy(keySort);
                this.sortStrategy.sort(array);
            }
        }
    }

    /**
     * Prints information.
     */
    private void printInfo() {
        System.out.println("\nProgram *ARRAY SORTING*");
        System.out.println("Enter:1-bubble sorting");
        System.out.println("Enter:2-selection sorting");
        System.out.println("Enter:0-exit");
    }

    /**
     * @param key string key for HashMap
     * @return value Sorter from HashMap by key
     */
    private Sorter getSortStrategy(String key) {
        HashMap<String, Sorter> map = new HashMap<>();
        map.put(BUBBLE_SORT, new BubbleSort());
        map.put(SELECTION_SORT, new SelectionSort());
        return map.get(key);
    }

}
