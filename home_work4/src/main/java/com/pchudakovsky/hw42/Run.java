package com.pchudakovsky.hw42;

import com.pchudakovsky.hw42.api.Sorter;

/**
 * This class for launch app.
 *
 * @author Pavel Chudakovsky.
 */
public class Run {
    public static void main(String[] args) {

        // unsorted array
        int[] array = {2, 5, 81, 3, 7, 7, 8, 2, 10};

        Sorter sortStrategy = null;
        SortingContext context = new SortingContext(sortStrategy);
        context.run(array);
    }
}
