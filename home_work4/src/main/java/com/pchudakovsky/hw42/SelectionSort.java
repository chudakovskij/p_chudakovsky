package com.pchudakovsky.hw42;

import com.pchudakovsky.hw42.api.Sorter;

import java.util.Arrays;

/**
 * Represents a method for sorting an array(Selection sort).
 *
 * @author Pavel Chudakovsky.
 */
public class SelectionSort implements Sorter {

    /**
     * Selection sort.
     *
     * @param array integer array
     * @return sorted array
     */
    @Override
    public int[] sort(int[] array) {
        int[] copyArray = Arrays.copyOf(array, array.length);
        int size = copyArray.length;
        for (int i = 0; i < size; i++) {
            int indexMin = i;
            int minElement = copyArray[i];

            for (int j = i + 1; j < size; j++) {
                if (copyArray[j] < minElement) {
                    minElement = copyArray[j];
                    indexMin = j;
                }
            }

            if (i != indexMin) {
                int memoryElement = copyArray[i];
                copyArray[i] = copyArray[indexMin];
                copyArray[indexMin] = memoryElement;
            }
        }
        printArray(copyArray);
        return copyArray;
    }

    /**
     * Print message about sorted array.
     *
     * @param array sorted array
     */
    private void printArray(int[] array) {
        System.out.println("Selection sort: " + Arrays.toString(array));
    }
}
