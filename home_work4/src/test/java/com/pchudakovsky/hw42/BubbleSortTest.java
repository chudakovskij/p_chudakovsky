package com.pchudakovsky.hw42;

import com.pchudakovsky.hw42.api.Sorter;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

public class BubbleSortTest {

    @Test
    public void testSort() {
        Sorter sorterStrategy = new BubbleSort();
        int[] array = {5, 3, 6, 1, 5, 7, 2};
        int[] copyArray = Arrays.copyOf(array, array.length);
        Arrays.sort(copyArray);
        assertArrayEquals(copyArray, sorterStrategy.sort(array));
    }

}