package com.pchudakovsky.hw41.service;

import com.pchudakovsky.hw41.card.Card;
import com.pchudakovsky.hw41.card.DebitCard;
import com.pchudakovsky.hw41.service.api.ServiceCard;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

public class ServiceDebitCardTest {

    private BigDecimal balance = new BigDecimal("1000.10");
    private Card card = new Card("Pavel Chudakovsky", balance);
    private ServiceCard serviceCard = new ServiceDebitCard(card);

    @Test
    public void testGetBalance() {
        BigDecimal actual = serviceCard.getBalance();
        BigDecimal expected = new BigDecimal("1000.10");
        assertEquals(expected, actual);
    }

    @Test
    public void testAddBalance() {
        BigDecimal addMoney = new BigDecimal("0.10");
        serviceCard.addBalance(addMoney);
        BigDecimal actual = serviceCard.getBalance();
        BigDecimal expected = new BigDecimal("1000.20");
        assertEquals(expected, actual);
    }

    @Test
    public void testWithdrawBalanceOnPositive() {
        BigDecimal withdrawMoney = new BigDecimal("0.20");
        serviceCard.withdrawBalance(withdrawMoney);
        BigDecimal actual = serviceCard.getBalance();
        BigDecimal expected = new BigDecimal("999.90");
        assertEquals(expected, actual);
    }

    @Test
    public void testWithdrawBalanceOnNegative() {
        BigDecimal withdrawMoney = new BigDecimal("5000.200");
        serviceCard.withdrawBalance(withdrawMoney);
        BigDecimal actual = serviceCard.getBalance();
        BigDecimal expected = new BigDecimal("1000.10");
        assertEquals(expected, actual);
    }

    @Test
    public void testConvertBalance() {
        BigDecimal rate = new BigDecimal("2.053");
        BigDecimal actual = serviceCard.convertBalance("USA", rate);
        BigDecimal expected = new BigDecimal("487.15");
        assertEquals(expected, actual);
    }

    @Test
    public void testGetOwnerName() {
        String actual = serviceCard.getOwnerName();
        String expected = "Pavel Chudakovsky";
        assertEquals(expected, actual);
    }

}