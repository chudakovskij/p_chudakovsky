package com.pchudakovsky.hw41.service;

import com.pchudakovsky.hw41.card.CreditCard;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

public class ServiceCreditCardTest {
    @Test
    public void testWithdrawBalanceOnNegative() {
        CreditCard creditCard = new CreditCard("Pavel", new BigDecimal("1000"));
        ServiceCreditCard serviceCreditCard = new ServiceCreditCard(creditCard);
        serviceCreditCard.withdrawBalance(new BigDecimal("5000"));
        BigDecimal actual = serviceCreditCard.getBalance();
        BigDecimal expected = new BigDecimal("-4000");
        assertEquals(expected, actual);
    }

    @Test
    public void testWithdrawBalanceOnPositive() {
        CreditCard creditCard = new CreditCard("Pavel", new BigDecimal("1000"));
        ServiceCreditCard serviceCreditCard = new ServiceCreditCard(creditCard);
        serviceCreditCard.withdrawBalance(new BigDecimal("700"));
        BigDecimal actual = serviceCreditCard.getBalance();
        BigDecimal expected = new BigDecimal("300");
        assertEquals(expected, actual);

    }
}