package com.pchudakovsky.hw7.api;

import com.pchudakovsky.hw7.exception.ValidationFailedException;

/**
 * Generic interface.
 *
 * @param <T> this is the type parameter.
 * @author Pavel Chudakovsky.
 */

public interface Validator<T> {
    /**
     * Generic method.
     *
     * @param value type parameter T.
     * @throws ValidationFailedException exception thrown
     *                                   then validation fails.
     */
    void validate(T value) throws ValidationFailedException;
}
