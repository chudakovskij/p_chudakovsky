package com.pchudakovsky.hw7;

import com.pchudakovsky.hw7.api.Validator;
import com.pchudakovsky.hw7.exception.ValidationFailedException;

import java.util.HashMap;
import java.util.Map;

/**
 * Class describing the behavior
 * of the validation system.
 *
 * @author Pavel Chudakovsky.
 */

final class ValidationSystem {

    /**
     * Key to select an integer validator.
     */
    private static final String INTEGER = "java.lang.Integer";
    /**
     * Key to select a string validator.
     */
    private static final String STRING = "java.lang.String";

    /**
     * Used to store validators.
     */
    private static Map<String, Validator> map = new HashMap<>();

    /**
     * Sole constructor.
     */
    private ValidationSystem() {
    }

    static {
        map.put(STRING, new StringValidator());
        map.put(INTEGER, new IntegerValidator());
    }

    /**
     * Generic method that selects the type of validator.
     *
     * @param value type parameter  T.
     * @param <T>   this is the type parameter.
     * @throws ValidationFailedException exception thrown
     *                                   then validation fails.
     */

    @SuppressWarnings("unchecked")
    static <T> void validate(final T value) throws ValidationFailedException {
        if (value instanceof String | value instanceof Integer) {
            String key = value.getClass().getName();
            map.get(key).validate(value);
        }
    }
}
