package com.pchudakovsky.hw7;

import com.pchudakovsky.hw7.exception.ValidationFailedException;

/**
 * Test class.
 *
 * @author Pavel Chudakovsky.
 */
public final class Main {
    /**
     * Privet constructor.
     */
    private Main() {
    }

    /**
     * Creates an object of type Runner and
     * calls the method run().
     *
     * @param args string array
     * @throws ValidationFailedException exception thrown
     *                                   then validation fails.
     */

    public static void main(final String[] args)
            throws ValidationFailedException {

        Runner runner = new Runner();
        runner.run();
    }
}
