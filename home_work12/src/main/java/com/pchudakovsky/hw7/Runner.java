package com.pchudakovsky.hw7;


import com.pchudakovsky.hw7.exception.ValidationFailedException;

import java.util.Scanner;

/**
 * The class implements the user interface.
 *
 * @author Pavel Chudakovsky.
 */
class Runner {

    /**
     * Used for exit app.
     */
    private static final String EXIT = "0";
    /**
     * Used for run validator.
     */
    private static final String VALIDATOR = "1";

    /**
     * Method  runs app.
     *
     * @throws ValidationFailedException exception thrown
     *                                   then validation fails.
     */
    void run() throws ValidationFailedException {
        try (Scanner scanner = new Scanner(System.in)) {
            while (true) {
                info();
                String line = scanner.nextLine();
                if (EXIT.equals(line)) {
                    break;
                } else if (VALIDATOR.equals(line)) {
                    System.out.println("Enter a string or integer:");
                    String text = scanner.nextLine();
                    Object value;
                    if (isDigit(text)) {
                        value = Integer.valueOf(text);
                    } else {
                        value = text;
                    }
                    ValidationSystem.validate(value);
                }
            }
        }
    }

    /**
     * Checks a string for numbers.
     *
     * @param line string value.
     * @return boolean value if line
     * contains only digits.
     */
    private boolean isDigit(final String line) {
        boolean flag = false;
        String regular = "\\d+";
        boolean isDigit = line.matches(regular);
        if (isDigit) {
            flag = true;
        }
        return flag;
    }

    /**
     * Prints information.
     */
    private void info() {
        System.out.println("*Program Validate *");
        System.out.println("To start the validator push - " + VALIDATOR);
        System.out.println("To exit push- " + EXIT);
    }
}

