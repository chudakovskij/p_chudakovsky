package com.pchudakovsky.service;

import com.pchudakovsky.entity.Product;

import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.MathContext;
import java.util.List;

/**
 * Provides methods for
 * presenting results in a browser.
 *
 * @author Pavel Chudakovsky.
 */

public class Service implements ServicePage {

    /**
     * Defines the style of the Web-form.
     */
    private static final String STYLE_FORM = " style=\" width:350px;height: 350px; " +
            "border-style: solid; background-color: coral;\"";

    /**
     * Link to start page of Online-shop.
     */
    private static final String LINK_ONLINE_SHOP = "<a href=\"http://localhost:8080/start\" > "
            + "  <h3>online shop</h3>"
            + "</a>";
    /**
     * Link to menu page of Online-shop.
     */
    private static final String LINK_MENU = "<a href=\"http://localhost:8080/menu\" > "
            + " <h3>product selection</h3>"
            + "</a>";


    /**
     * Returns total price of products.
     *
     * @param products information about products as array of string.
     * @return total price as BigDecimal.
     */
    private BigDecimal getTotalCost(String[] products) {
        String[] cost = new String[products.length];
        for (int i = 0; i < products.length; i++) {
            String[] array = products[i].split(" ");
            cost[i] = array[1];
        }
        BigDecimal total = BigDecimal.valueOf(0);
        for (String str : cost) {
            BigDecimal sum = new BigDecimal(Double.valueOf(str));
            total = total.add(sum, MathContext.DECIMAL32);
        }
        return total;
    }

    /**
     * Prints report for User.
     *
     * @param products information about products as array of string.
     * @param userName user of name as type of String.
     * @param out      object of {@link PrintWriter}.
     */
    @Override
    public void printReport(String[] products,
                            String userName, PrintWriter out) {
        out.println("<h1> Dear " + userName + ", you order:" + "</h1>");
        for (String product : products) {
            out.println("<p>" + product + "</p>");
        }

        BigDecimal total = getTotalCost(products);
        out.println("<h3> total: " + " $ " + total + "</h3>");
    }

    /**
     * Provides a product menu.
     *
     * @param products list products as  type of List.
     * @param userName user of name as type of String.
     * @param out      object of {@link PrintWriter}.
     */
    @Override
    public void formMenu(List<Product> products,
                         String userName, PrintWriter out) {
        out.print("<html>"
                + "<body>" +
                "<div  align=\"center\">"
                + "<h1> Hello " + userName + " !</h1>"
                + "<form " + STYLE_FORM +
                " method=\"post\" action=\"report\">"
                + "<div style=\"margin: 5% 25% 25% 25%;color:ivory;font-size: larger;\">"
                + "<div>Make you order:</div>"
                + "<select style=\"margin: 10% 25% 25% 15%;color:black;font-size: medium;\"" +
                "  name=\"products\" multiple=true size=\"3\">");

        for (Product product : products) {
            out.print("<option>" + product.getName()
                    + " " + product.getCost() +
                    " " + "$" + "</option>");
        }

        out.print("</select> "
                + "<input type = \"submit\" value = \"submit\" / >"
                + "<div style=\"color:ivory;font-size: small;\" >" +
                "<br/>"
                + "For multiple selection press"
                + " \"CTRL\" + mouse click... </div>"
                + "</div>"
                + "</form >"
                + "</div>"
                + "</body></html>");

    }

    /**
     * Provides a Web-form for start page.
     *
     * @param out object of {@link PrintWriter}.
     */
    @Override
    public void formStart(PrintWriter out) {

        out.print("<html>"
                + "<body>"
                + "<div align=\"center\">"
                + "<h1> Welcome Online Shop</h1>" +
                "<form  " + STYLE_FORM +
                " method=\"post\" action=\"menu\">"
                + "<div style=\"margin: 40% 25% 25% 25%;color:ivory;font-size: larger;\">"
                + "Name:<input type = \"text\" name = \"userName\" / > "
                + "<br><br>"
                + "<input style=\"width: 50x;\" type = \"submit\" value = \"Enter\" / >"
                + "</div>"
                + "</form >"
                + "</div>"
                + "</body></html>");

    }

    /**
     * Prints information on a page when
     * name of user is not valid.
     *
     * @param out object of {@link PrintWriter}.
     */
    @Override
    public void infoBadName(PrintWriter out) {
        out.print("<div> Bad username or username not entered! </div>");
        out.print("<div> Follow the link below...</div>");
        out.print(LINK_ONLINE_SHOP);
    }

    /**
     * Prints information on a page when
     * products are not selected.
     *
     * @param out object of {@link PrintWriter}.
     */
    @Override

    public void infoProductsNotSelected(PrintWriter out) {
        out.print("<div>  Sorry, item not selected! </div>");
        out.print("<div> Follow the link below...</div>");
        out.print(LINK_MENU);
    }

    /**
     * Prints information on a page when
     * list of  products is empty.
     *
     * @param out object of {@link PrintWriter}.
     */
    @Override
    public void listProductsIsEmpty(PrintWriter out) {
        out.print("<div>  Sorry, list of products is empty! </div>");
        out.print("<div> Follow the link below...</div>");
        out.print(LINK_MENU);
    }
}


