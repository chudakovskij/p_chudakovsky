package com.pchudakovsky.servlets;

import com.pchudakovsky.service.Service;
import com.pchudakovsky.utility.ValidatorName;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class ReportServlet extends HttpServlet {
    /**
     * Object of Service.
     */
    private Service service;

    /**
     * Logger.
     */
    private final static Logger logger = LogManager
            .getLogger(Service.class);

    /**
     * Method initializes servlet.
     */
    @Override
    public void init() {
        logger.info("Method init() for "
                + ReportServlet.class + " is started!");

        logger.info("Initialization of object for Service");
        service = new Service();
    }

    /**
     * Get-request.
     *
     * @param req  variable of type {@link HttpServletRequest}.
     * @param resp variable of type {@link HttpServletResponse}.
     * @throws IOException if an input or output exception occurred.
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        logger.info("Method doGet() for "
                + ReportServlet.class + " is started!");
        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();
        String userName = (String) req.getSession().getAttribute("user");

        if (ValidatorName.nameIsNull(userName)) {
            service.infoBadName(out);
        } else {
            String[] userProducts = (String[]) req.getSession().getAttribute("userProducts");
            if (userProducts != null) {
                service.printReport(userProducts, userName, out);
            } else {
                service.listProductsIsEmpty(out);
            }
        }
    }

    /**
     * Post-request.
     *
     * @param req  variable of type {@link HttpServletRequest}.
     * @param resp variable of type {@link HttpServletResponse}.
     * @throws IOException if an input or output exception occurred.
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        logger.info("Method doPost() for "
                + ReportServlet.class + " is started!");
        resp.setContentType("text/html");
        String userName = (String) req.getSession().getAttribute("user");
        String[] listProducts = req.getParameterValues("products");
        req.getSession().setAttribute("userProducts", listProducts);
        PrintWriter out = resp.getWriter();
        if (listProducts != null) {
            logger.info("Method printReport() is called from "
                    + Service.class + ".");
            service.printReport(listProducts, userName, out);
        } else {
            logger.info("Method infoProductsNotSelected() is called from "
                    + Service.class + ".");
            service.infoProductsNotSelected(out);
        }
    }

    /**
     * Called by the servlet container to indicate
     * to a servlet that the servlet is being
     * taken out of service.
     */
    @Override
    public void destroy() {
        logger.info("Method destroy() for "
                + ReportServlet.class + " is started!");
    }
}
