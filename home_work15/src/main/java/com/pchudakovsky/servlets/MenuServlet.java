package com.pchudakovsky.servlets;

import com.pchudakovsky.entity.Product;
import com.pchudakovsky.service.Service;
import com.pchudakovsky.utility.ValidatorName;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * Servlet for greeting the user
 * and displaying the product menu.
 */
public class MenuServlet extends HttpServlet {
    /**
     * Object of Service.
     */
    private Service service;

    /**
     * Logger.
     */
    private final static Logger logger = LogManager.getLogger(Service.class);

    /**
     * Method initializes servlet.
     */
    @Override
    public void init() {
        logger.info("Method init() for "
                + MenuServlet.class + " is started!");

        logger.info("Initialization of object for Service");
        service = new Service();
    }

    /**
     * Get-request.
     *
     * @param req  variable of type {@link HttpServletRequest}.
     * @param resp variable of type {@link HttpServletResponse}.
     * @throws IOException if an input or output exception occurred.
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        logger.info("Method doGet() for "
                + MenuServlet.class + " is started!");
        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();
        String userName = (String) req.getSession().getAttribute("user");

        if (ValidatorName.nameIsNull(userName)) {
            service.infoBadName(out);
        } else {
            service.formMenu(Product.listProduct, userName, out);
        }
    }

    /**
     * Post-request.
     *
     * @param req  variable of type {@link HttpServletRequest}.
     * @param resp variable of type {@link HttpServletResponse}.
     * @throws IOException if an input or output exception occurred.
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        logger.info("Method doPost() for "
                + MenuServlet.class + " is started!");

        resp.setContentType("text/html");
        String userName = req.getParameter("userName");
        PrintWriter out = resp.getWriter();
        if (ValidatorName.checkName(userName)) {
            logger.info("Setting session for username");
            HttpSession session = req.getSession();
            session.setAttribute("user", userName);
            List<Product> products = Product.listProduct;
            logger.info("Method formMenu() is called from " + Service.class + ".");
            service.formMenu(products, userName, out);
        } else {
            logger.info("Name user is not correct.");
            logger.info("Method infoBadeName() is called from "
                    + Service.class + ".");
            service.infoBadName(out);
        }

    }

    /**
     * Called by the servlet container to indicate
     * to a servlet that the servlet is being
     * taken out of service.
     */
    @Override
    public void destroy() {
        logger.info("Method destroy() for "
                + MenuServlet.class + " is started!");
    }
}
