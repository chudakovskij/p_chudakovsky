package com.pchudakovsky.service;

import com.pchudakovsky.entity.Product;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ServiceTest {
    @Mock
    private HttpServletResponse mockResponse;

    @Mock
    Service mockService = new Service();

    @Test
    public void testPrintReportCalledVerified() throws IOException {
        doNothing().when(mockService).printReport(new String[]{isA(String.class)},
                isA(String.class), isA(PrintWriter.class));
        String[] products = {"CarA 2000 $", "CarA 3000 $"};
        PrintWriter out = mockResponse.getWriter();
        mockService.printReport(products, "Ivanov", out);
        verify(mockService, times(1)).
                printReport(products, "Ivanov", out);
    }

    @Test
    public void testFormMenuCalledVerified() throws IOException {
        doNothing().when(mockService).formMenu(anyListOf(Product.class),
                isA(String.class), isA(PrintWriter.class));
        PrintWriter out = mockResponse.getWriter();
        mockService.formMenu(Product.listProduct, "Ivanov", out);
        verify(mockService, times(1)).
                formMenu(Product.listProduct, "Ivanov", out);
    }

    @Test
    public void testFormStartCalledVerified() throws IOException {
        doNothing().when(mockService).formStart(isA(PrintWriter.class));
        PrintWriter out = mockResponse.getWriter();
        mockService.formStart(out);
        verify(mockService, times(1)).
                formStart(out);
    }

    @Test
    public void testInfoBadNameCalledVerified() throws IOException {
        PrintWriter out = mockResponse.getWriter();
        doNothing().when(mockService).infoBadName(isA(PrintWriter.class));
        mockService.infoBadName(out);
        verify(mockService, times(1)).
                infoBadName(out);
    }


    @Test
    public void testListProductsIsEmptyCalledVerified() throws IOException {
        PrintWriter out = mockResponse.getWriter();
        doNothing().when(mockService).listProductsIsEmpty(isA(PrintWriter.class));
        mockService.listProductsIsEmpty(out);
        verify(mockService, times(1)).
                listProductsIsEmpty(out);
    }

    @Test
    public void testInfoProductsNotSelectedCalledVerified() throws IOException {
        PrintWriter out = mockResponse.getWriter();
        doNothing().when(mockService).infoProductsNotSelected(isA(PrintWriter.class));
        mockService.infoProductsNotSelected(out);
        verify(mockService, times(1)).
                infoProductsNotSelected(out);
    }
}