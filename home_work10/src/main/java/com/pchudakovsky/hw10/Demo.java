package com.pchudakovsky.hw10;

import java.math.BigDecimal;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Test class.
 */
public class Demo {

    public static void main(String[] args) {

        Card card = new Card("Pavel", BigDecimal.valueOf(500));
        ExecutorService executorService = Executors.newFixedThreadPool(8);

        Runnable consumer1 = new MoneyConsumer(card, executorService);
        Runnable consumer2 = new MoneyConsumer(card, executorService);
        Runnable consumer3 = new MoneyConsumer(card, executorService);
        Runnable consumer4 = new MoneyConsumer(card, executorService);

        Runnable producer1 = new MoneyProducer(card, executorService);
        Runnable producer2 = new MoneyProducer(card, executorService);
        Runnable producer3 = new MoneyProducer(card, executorService);
        Runnable producer4 = new MoneyProducer(card, executorService);

        executorService.execute(consumer1);
        executorService.execute(consumer2);
        executorService.execute(consumer3);
        executorService.execute(consumer4);
        executorService.execute(producer1);
        executorService.execute(producer2);
        executorService.execute(producer3);
        executorService.execute(producer4);
    }

}
