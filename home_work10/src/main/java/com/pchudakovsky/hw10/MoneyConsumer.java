package com.pchudakovsky.hw10;

import java.math.BigDecimal;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * The class provides a reduction in
 * the cash amount of the card.
 *
 * @author Pavel Chudakovsky.
 */
@SuppressWarnings("SynchronizeOnNonFinalField")
public class MoneyConsumer implements Runnable {

    /**
     * Amount of money.
     */
    private static final long WITHDRAW_DOLLAR = 10;
    /**
     * Card account limit.
     */
    private static final long LIMIT = 1000;
    /**
     * Thread blocking time (mls).
     */
    private static final long TIME = 3000;

    /**
     * Bank card.
     */
    private Card card;
    /**
     * ExecutorService.
     */
    private ExecutorService executorService;

    /**
     * Sole constructor.
     *
     * @param card            bank card.
     * @param executorService value of type ExecutorService.
     */
    MoneyConsumer(Card card, ExecutorService executorService) {
        this.card = card;
        this.executorService = executorService;
    }


    /**
     * Basic Method.
     */
    @Override
    public void run() {
        while (true) {
            if (balance().compareTo(BigDecimal.valueOf(0)) <= 0 |
                    balance().compareTo(BigDecimal.valueOf(LIMIT)) > 0) {
                try {
                    executorService.shutdown();
                    executorService.awaitTermination(3, TimeUnit.SECONDS);
                } catch (InterruptedException e) {
                    System.err.println("Task is interrupted");
                    break;
                } finally {
                    if (!executorService.isTerminated()) {
                        executorService.shutdownNow();
                        System.out.println("shutdown finished");
                    }
                }

            } else {
                withdraw();
            }
        }
    }

    /**
     * Method reduces cash balance.
     */
    private void withdraw() {
        synchronized (card) {
            BigDecimal tenDollar = BigDecimal.valueOf(WITHDRAW_DOLLAR);
            BigDecimal currentBalance = card.getBalance();
            if (currentBalance.compareTo(BigDecimal.valueOf(0)) > 0) {
                BigDecimal balance = card.getBalance().subtract(tenDollar);

                card.setBalance(balance);
                System.out.println("ATM: " + Thread.currentThread().getName() + " Balance withdraw: " + tenDollar
                        + " . Current Balance: " + card.getBalance());
            }

            try {
                card.wait(TIME);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();

            }

        }

    }

    /**
     * Returns balance.
     *
     * @return balance as BigDecimal.
     */
    private BigDecimal balance() {
        synchronized (card) {
            return card.getBalance();
        }
    }
}


