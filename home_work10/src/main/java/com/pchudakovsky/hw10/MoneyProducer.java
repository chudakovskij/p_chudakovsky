package com.pchudakovsky.hw10;

import java.math.BigDecimal;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * The class represents a method for replenishing
 * a card account.
 *
 * @author Pavel Chudakovsky.
 */

@SuppressWarnings("SynchronizeOnNonFinalField")
public class MoneyProducer implements Runnable {
    /**
     * Amount of money.
     */
    private static final long ADD_DOLLAR = 10;
    /**
     * Card account max limit.
     */
    private static final long LIMIT = 1000;
    /**
     * Card account min limit.
     */
    private static final long NULL_DOLLAR = 0;
    /**
     * Thread blocking time (mls).
     */
    private static final long TIME = 5000;

    /**
     * Bank card.
     */
    private Card card;
    /**
     * ExecutorService.
     */
    private ExecutorService executorService;

    /**
     * Sole constructor.
     *
     * @param card            as bank card.
     * @param executorService value of type ExecutorService.
     */
    MoneyProducer(Card card, ExecutorService executorService) {
        this.card = card;
        this.executorService = executorService;
    }

    /**
     * Basic Method.
     */
    @Override
    public void run() {
        while (true) {
            if (balance().compareTo(BigDecimal.valueOf(NULL_DOLLAR)) <= 0 |
                    balance().compareTo(BigDecimal.valueOf(LIMIT)) > 0) {
                try {
                    executorService.shutdown();
                    executorService.awaitTermination(3, TimeUnit.SECONDS);
                } catch (InterruptedException e) {
                    System.err.println("Task is interrupted");
                    break;
                } finally {
                    if (!executorService.isTerminated()) {
                        executorService.shutdownNow();
                        System.out.println("shutdown finished");
                    }
                }

            } else {
                add();
            }
        }
    }

    /**
     * Replenishes the cash balance.
     */
    private synchronized void add() {
        synchronized (card) {
            BigDecimal tenDollar = BigDecimal.valueOf(ADD_DOLLAR);
            BigDecimal currentBalance = card.getBalance();
            BigDecimal limit = BigDecimal.valueOf(LIMIT);
            if (currentBalance.compareTo(limit) <= 0) {
                BigDecimal balance = currentBalance.add(tenDollar);
                System.out.println("ATM: " + Thread.currentThread().getName() + " Balance add: " + tenDollar
                        + ". Current Balance: " + card.getBalance());
                card.setBalance(balance);
            }
            try {
                wait(TIME);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
    }

    /**
     * Returns balance.
     *
     * @return balance as BigDecimal.
     */
    private BigDecimal balance() {
        synchronized (card) {
            return card.getBalance();
        }
    }
}

