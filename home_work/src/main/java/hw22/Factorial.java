package hw22;

import hw22.api.Function;

/**
 * Class Factorial represents methods
 *  for calculating factorial of numbers.
 *
 * @author Pavel Chudakovsky
 */
public class Factorial implements Function {

    private int factorial;

    /**
     * Finds factorial of numbers by a loop 'while'.
     *
     * @param number positive integer or "0".
     * @return factorial as string format
     */
    @Override
    public String loopWhile(int number) {
        if (number == 0) {
            factorial = 1;
        } else if (number >= 1) {
            int count = 1;
            factorial = 1;
            while (count <= number) {
                factorial = count * factorial;
                count++;
            }
        }

        if (number < 0) {
            getMessage(number);
        }
        return String.valueOf(factorial);
    }

    /**
     * Finds factorial of numbers by a loop 'for'.
     *
     * @param number positive integer or "0".
     * @return factorial as string format
     */
    @Override
    public String loopFor(int number) {
        if (number == 0) {
            factorial = 1;
        } else if (number >= 1) {
            factorial = 1;
            for (int i = 1; i <= number; i++) {
                factorial = i * factorial;
            }
        }

        if (number < 0) {
            getMessage(number);
        }
        return String.valueOf(factorial);
    }

    /**
     * Finds factorial of numbers by a loop 'do while.
     *
     * @param number positive integer or "0".
     * @return factorial as string format
     */
    @Override
    public String loopDoWhile(int number) {
        if (number == 0) {
            factorial = 1;
        } else if (number >= 1) {
            factorial = 1;
            int count = 1;
            do {
                factorial = factorial * count;
                count++;
            } while (count <= number);
        }
        if (number < 0) {
            getMessage(number);
        }
        return String.valueOf(factorial);
    }

    /**
     * Prints a message when factorial of numbers is not defined.
     */
    private void getMessage(int value) {
        System.out.println("Value n=" + value + " is not correct.");
        System.out.println("Factorial isn't defined");
    }
}