package hw22;


import hw22.api.Function;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Class Fibonacci  represents methods
 * for calculating Fibonacci numbers.
 *
 * @author Pavel Chudakovsky
 */
public class Fibonacci implements Function {

    private int fibonacci;

    /**
     * Finds Fibonacci numbers by a loop 'for'.
     *
     * @param n positive integer defining the quantity of Fibonacci numbers.
     * @return list of Fibonacci numbers as an array in string format.
     */
    @Override
    public String loopFor(int n) {
        ArrayList<Integer> listFibonacci = new ArrayList<>();
        int number1 = 0;
        int number2 = 1;
        if (n > 1) {
            listFibonacci.add(number1);
            listFibonacci.add(number2);
            for (int count = 2; count < n; count++) {
                fibonacci = number1 + number2;
                number1 = number2;
                number2 = fibonacci;
                listFibonacci.add(count, number2);
            }
        } else if (n == 1) {
            listFibonacci.add(number1);
        }

        if (n == 0 | n < 0) {
            getMessage();
        }

        Object[] array = listFibonacci.toArray();
        return Arrays.toString(array);
    }

    /**
     * Finds Fibonacci numbers by a loop 'while'.
     *
     * @param n positive integer defining the quantity of Fibonacci numbers.
     * @return list of Fibonacci numbers as an array in string format.
     */
    @Override
    public String loopWhile(int n) {
        ArrayList<Integer> listFibonacci = new ArrayList<>();
        int number1 = 0;
        int number2 = 1;

        if (n > 1) {
            int count = 2;
            listFibonacci.add(number1);
            listFibonacci.add(number2);
            while (count < n) {
                fibonacci = number1 + number2;
                number1 = number2;
                number2 = fibonacci;
                listFibonacci.add(number2);
                count++;
            }
        } else if (n == 1) {
            listFibonacci.add(number1);
        }
        if (n == 0 | n < 0) {
            getMessage();
        }

        Object[] array = listFibonacci.toArray();
        return Arrays.toString(array);
    }

    /**
     * Finds Fibonacci numbers by a loop 'do while'.
     *
     * @param n positive integer defining the quantity of Fibonacci numbers.
     * @return list of Fibonacci numbers as an array in string format.
     */
    @Override
    public String loopDoWhile(int n) {
        ArrayList<Integer> listFibonacci = new ArrayList<>();
        int number1 = 0;
        int number2 = 1;
        if (n > 1) {
            int count = 2;
            listFibonacci.add(number1);
            listFibonacci.add(number2);
            do {
                fibonacci = number1 + number2;
                number1 = number2;
                number2 = fibonacci;
                count++;
                if (count > n) {
                    break;
                }
                listFibonacci.add(number2);

            } while (count < n);

        } else if (n == 1) {
            listFibonacci.add(number1);
        }
        if (n == 0 | n < 0) {
            getMessage();
        }

        Object[] array = listFibonacci.toArray();
        return Arrays.toString(array);
    }

    /**
     * Prints a message when the list of Fibonacci numbers is empty.
     */
    private void getMessage() {
        System.out.println("List \" Fibonacci\" is empty." +
                " Restart program: parameter n must be");
        System.out.println(" n!=0 and n>0 !!! ");
    }
}
