package hw22;

/**
 * Class Run  contains an executing method.
 * In method main() object of reference type Calculator is created and called
 * method doRun().
 * Run the app for instructions.
 *
 * @author Pavel Chudakovky
 * @see Calculator#doRun()
 * @see Calculator
 */
public class Run {
    public static void main(String[] args) {
        Calculator calculator = new Calculator(args);
        calculator.doRun();
    }
}
