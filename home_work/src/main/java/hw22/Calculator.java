package hw22;

import hw22.api.Function;

import java.util.Arrays;


/**
 * Class Calculator  controls methods for calculating
 * the factorial of number or Fibonacci numbers.
 *
 * @author Pavel Chudakovsky
 */
class Calculator {

    private final static String FIBONACCI = "1";
    private final static String FACTORIAL = "2";
    private final static String KEY_FOR = "1";
    private final static String KEY_WHILE = "2";
    private final static String KEY_DO_WHILE = "3";
    private final static String PRINT_FIBONACCI = "Fibonacci";
    private final static String PRINT_FACTORIAL = "Factorial";
    private String[] args;

    Calculator(String[] args) {
        this.args = args;
    }


    /**
     * Runs the method {@link #run()}.
     */
    void doRun() {
        boolean checkLength = args.length >= 3;
        printInstruction();
        if (checkLength) {
            if (checkInput()) {
                run();
            }
        } else {
            messageWarning();
        }
    }

    /**
     * Checks input for correctness
     *
     * @return flagRun boolean variable
     */
    private boolean checkInput() {
        boolean selectKey = args[0].equals(FIBONACCI) | args[0].equals(FACTORIAL);
        boolean selectLoopKey = args[1].equals(KEY_FOR) | args[1].equals(KEY_WHILE) | args[1].equals(KEY_DO_WHILE);
        boolean flagRun = false;

        if (selectKey & selectLoopKey) {
            boolean flagDigit = false;
            boolean flagMinus = false;

            String lineOne = Arrays.toString(args);
            String lineTwo = Arrays.toString(args).replaceAll("\\W", "");

            char[] array1 = lineOne.toCharArray();
            char[] array2 = lineTwo.toCharArray();

            for (char ch : array1) {
                if (ch == '-' | ch == '.') {
                    System.out.println("STOP:Values have minus or not integer. " +
                            "Enter correct numbers.");
                    flagMinus = true;
                    break;
                }
            }
            for (char ch : array2) {
                if (!Character.isDigit(ch)) {
                    System.out.println("STOP:Values aren't number. " +
                            "Enter correct numbers.");
                    flagDigit = true;
                    break;
                }
            }

            if (!flagDigit & !flagMinus) {
                flagRun = true;
            }
        } else {
            messageError();
        }
        return flagRun;
    }

    /**
     * Selects a branch for calculation: fibonacci numbers or factorial.
     * Runs the calculation by {@link #calculate(Function, String, String)}
     */
    private void run() {
        if (args[0].equals(FIBONACCI)) {
            Fibonacci fibonacci = new Fibonacci();
            calculate(fibonacci, FIBONACCI, PRINT_FIBONACCI);
        } else if (args[0].equals(FACTORIAL)) {
            Factorial factorial = new Factorial();
            calculate(factorial, FACTORIAL, PRINT_FACTORIAL);
        }
    }

    /**
     * Calculates Fibonacci numbers or factorial of number
     *
     * @param function             provides methods from classes Fibonacci or Factorial
     * @param keyFunction          "FIBONACCI" or "FACTORIAL"
     * @param nameFunctionForPrint indicates the function name when printing the result
     * @see Calculator#printLoopFor(String, String)
     * @see Calculator#printLoopWhile(String, String)
     * @see Calculator#printLoopDoWhile(String, String)
     * @see Factorial
     * @see Fibonacci
     */
    private void calculate(Function function, String keyFunction, String nameFunctionForPrint) {
        String[] keyLoop = {KEY_FOR, KEY_WHILE, KEY_DO_WHILE};
        String value;
        if (keyFunction.equals(args[0])) {
            printMessage(keyFunction);
            if (keyLoop[0].equals(args[1])) {
                value = function.loopFor(toNumber(args[2]));
                printLoopFor(nameFunctionForPrint, value);
            } else if (keyLoop[1].equals(args[1])) {
                value = function.loopWhile(toNumber(args[2]));
                printLoopWhile(nameFunctionForPrint, value);
            } else if (keyLoop[2].equals(args[1])) {
                value = function.loopDoWhile(toNumber(args[2]));
                printLoopDoWhile(nameFunctionForPrint, value);
            }
        }
    }

    /**
     * Prints an input error message
     */
    private void messageError() {
        System.out.println("Input error! Enter the correct data:");
        System.out.println("Key_Function(args[0]): 1-Fibonacci; 2-Factorial");
        System.out.println("Key_Loop (args[1]):1-For; 2-While; 3-Do While");
    }

    /**
     * Prints  message when arguments of command line are less
     * than three or are empty.
     */
    private void messageWarning() {
        System.out.println();
        System.out.println("--> Warning!Arguments of command line are less than three" +
                " or are empty.");
    }

    /**
     * Indicates which calculation is performed:
     * Fibonacci numbers or factorial of number
     *
     * @param keyFunction key of function for calculate
     */
    private void printMessage(String keyFunction) {
        if (keyFunction.equals(FIBONACCI)) {
            System.out.println("Fibonacci numbers:");
        } else if (keyFunction.equals(FACTORIAL)) {
            System.out.println("Factorial value:");
        }
    }

    /**
     * Prints of the instruction for app
     */
    private void printInstruction() {
        System.out.println("Instruction: IntellijIDEA->Run->Edit configuration...-> Program arguments");
        System.out.println(" -> set the first argument(args[0]): 1-Fibonacci or 2-Factorial");
        System.out.println(" -> set the second argument(args[1]): 1-loop For; 2-loop While; 3-loop Do While");
        System.out.println(" -> set the third argument(args[2]): positive number.");
        System.out.println("******************************");
    }

    /**
     * Converts a string to a number
     *
     * @param arg string to be converted to an integer
     * @return whole number
     */
    private int toNumber(String arg) {
        return Integer.parseInt(arg);
    }

    /**
     * Prints information for loop 'for'
     *
     * @param text  name function(for example "Fibonacci","Factorial")
     * @param value result of calculation for loopFor()
     * @see Fibonacci#loopFor(int)
     * @see Factorial#loopFor(int)
     */
    private void printLoopFor(String text, String value) {

        System.out.println(text + " -> Loop For: " + value);
    }

    /**
     * Prints information for loop 'while'
     *
     * @param text  name function(for example "Fibonacci","Factorial")
     * @param value result of calculation for loopWhile()
     * @see Fibonacci#loopWhile(int)
     * @see Factorial#loopWhile(int)
     */
    private void printLoopWhile(String text, String value) {
        System.out.println(text + " -> Loop While: " + value);

    }

    /**
     * Prints information for loop 'do while'
     *
     * @param text  name function(for example "Fibonacci","Factorial")
     * @param value result of calculation for loopDoWhile()
     * @see Fibonacci#loopDoWhile(int)
     * @see Factorial#loopDoWhile(int)
     */
    private void printLoopDoWhile(String text, String value) {
        System.out.println(text + " -> Loop Do While: " + value);
    }


}