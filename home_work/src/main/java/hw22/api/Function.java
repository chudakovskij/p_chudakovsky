package hw22.api;

/**
 * This interface is used to manipulate loops (for,while,do while).
 * <br> <b>All Know Implementing Classes: </b> {@link hw22.Fibonacci},{@link hw22.Factorial}.
 */
public interface Function {

    String loopFor(int n);

    String loopWhile(int n);

    String loopDoWhile(int n);

}
