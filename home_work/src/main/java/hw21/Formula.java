package hw21;

import static java.lang.Math.*;

/**
 * Class <b>Formula</b>  is designed to calculate the formula expression
 *
 * @author Pavel Chudakovsky
 */
class Formula {

    /**
     * This field  is string array:parameters of formula expression
     */
    private String[] args;

    /**
     * Sole constructor
     *
     * @param args string array
     * @see Formula#Formula(String[])
     */
    Formula(String[] args) {
        this.args = args;
    }

    /**
     * Method for calculation of formula
     *
     * @return g value of type double
     */
    double getResult() {
        double g;
        int a = toInteger(this.args[0]);
        int p = toInteger(this.args[1]);
        double m1 = toDouble(this.args[2]);
        double m2 = toDouble(this.args[3]);

        g = (4.0 * pow(PI, 2.0) * (pow(a, 3.0)) / (pow(p, 2.0) * (m1 + m2)));

        return g;
    }

    /**
     * Call  {@link #printInstruction()} ,
     * checks conditions to run of application {@link #isFractionalNumber(String)}, starts the method
     * {@link #getResult()} .
     */
    void printResult() {
        printInstruction();
        if (this.args.length >= 4) {
            if (!isFractionalNumber(args[0]) | !isFractionalNumber(args[1])) {
                System.out.println("Result: " + getResult());
            }
        } else {
            System.out.println("Input parameters are empty.\n" +
                    "Set input parameters!");
        }
    }

    /**
     * Prints the instructions for the application
     */
    private void printInstruction() {
        System.out.println("Instruction: IntellijIDEA->Run->Edit configuration...-> Program arguments");
        System.out.println(" -> set the first argument a (args[0]): Type `int`");
        System.out.println(" -> set the second argument p(args[1]):  Type `int`");
        System.out.println(" -> set the third argument m1 (args[2]):  Type `double`");
        System.out.println(" -> set the fourth argument m2 (args[3]):  Type `double`");
        System.out.println("*********************************************************");
    }

    /**
     * Checks of string on a fractional number
     *
     * @param str value of type String
     * @return flag value of type boolean
     */
    boolean isFractionalNumber(String str) {
        boolean flag = false;
        char[] array = str.toCharArray();

        for (char ch0 : array) {
            if (ch0 == '.') {
                flag = true;
                System.out.println("Error Input! Check args[0] or args[1]:Value " + str + " fractional number.");
                break;
            }
        }
        return flag;
    }

    /**
     * Converts a string variable to an int variable
     *
     * @param str value of type String
     * @return value of type int
     */
    int toInteger(String str) {
        int value = 0;
        try {
            value = Integer.parseInt(str);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
            System.out.println("Input error:");
            System.out.println("1) a or p - not number" + " **OR** " +
                    " 2) a or p - fractional number");
            System.out.println("Result isn't correct->");
        }
        return value;
    }

    /**
     * Converts a string variable to  double variable
     *
     * @param str value of type String
     * @return value of type double
     */
    double toDouble(String str) {
        double value = 0;
        try {
            value = Double.parseDouble(str);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
            System.out.println("Input error:");
            System.out.println("1) m1 or m2 - not number");
            System.out.println("Result isn't correct ->");
        }
        return value;
    }
}

