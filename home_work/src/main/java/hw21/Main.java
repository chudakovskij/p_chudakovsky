package hw21;

/**
 * Class Main  contains an executing method.
 * In method main() object of reference type Formula is created and called
 * method for calculating the expression formula.
 * Run the app for instructions.
 *
 * @author Pavel Chudakovsky
 * @see Formula
 * @see Formula#printResult()
 */
public class Main {

    public static void main(String[] args) {
        Formula formula = new Formula(args);
        formula.printResult();
    }
}
