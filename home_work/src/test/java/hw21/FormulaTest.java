package hw21;

import org.junit.Test;

import static org.junit.Assert.*;

public class FormulaTest {

    @Test
    public void getResultTest1() {
        String[] args = {"1", "1", "1", "1",};
        Formula formula = new Formula(args);
        double actual = formula.getResult();
        double expected = 19.739208802178716;
        double delta = 1.0e-15;
        assertEquals(expected, actual, delta);
    }

    @Test
    public void getResultTest2ForInfinity() {
        String[] args = {"1", "1", "1", "-1",};
        Formula formula = new Formula(args);
        double actual = formula.getResult();
        double expected = Math.pow(0.0, -1.0); // Infinity
        double delta = 1.0e-15;
        assertEquals(expected, actual, delta);
    }

    @Test
    public void getResultTes3ForNegativeInfinity() {
        String[] args = {"-1", "1", "1", "-1",};
        Formula formula = new Formula(args);
        double actual = formula.getResult();
        double expected = -Math.pow(0.0, -1.0); // negative infinity
        double delta = 1.0e-15;
        assertEquals(expected, actual, delta);
    }

    @Test
    public void getResultTest4ForNaN() {
        String[] args = {"0", "1", "1", "-1",};
        Formula formula = new Formula(args);
        double actual = formula.getResult();
        double expected = Math.sin(0.0) / Math.sin(0.0); // NaN
        double delta = 1.0e-15;
        assertEquals(expected, actual, delta);
    }

    @Test
    public void getResultTest5ForNaN() {
        String[] args = {"0", "0", "0", "0",};
        Formula formula = new Formula(args);
        double actual = formula.getResult();
        double expected = Math.sin(0.0) / Math.sin(0.0); // NaN
        double delta = 1.0e-15;
        assertEquals(expected, actual, delta);
    }

    @Test
    public void isFractionalNumberTest() {
        String[] args = {"10", "10.0"};
        Formula formula = new Formula(args);

        boolean actualFalse = formula.isFractionalNumber(args[0]);
        assertFalse(actualFalse);

        boolean actualTrue = formula.isFractionalNumber(args[1]);
        assertTrue(actualTrue);
    }

    @Test
    public void toIntegerTestForLineWithNotNumber() {
        String[] args = {"1ss", "5", "7", "5"};
        Formula formula = new Formula(args);
        int actual = formula.toInteger(args[0]);
        int expected = 0; // 0- as error
        assertEquals(expected, actual);
    }

    @Test
    public void toIntegerTestForLineWithNumbers() {
        String[] args = {"1", "5", "7", "6"};
        Formula formula = new Formula(args);
        int actual = formula.toInteger(args[1]);
        int expected = 5;
        assertEquals(expected, actual);
    }

    @Test
    public void toDoubleTestForLineWithNotNumber() {
        String[] args = {"1", "5", "7.0", "5s"};
        Formula formula = new Formula(args);
        double delta = 1.0e-15;
        double actual1 = formula.toDouble(args[3]);
        double expected1 = 0; // 0- as error
        assertEquals(expected1, actual1, delta);
    }

    @Test
    public void toDoubleTestForLineWithNumbers() {
        String[] args = {"1", "5", "7.0", "6.0"};
        Formula formula = new Formula(args);
        double delta = 1.0e-15;
        double actual2 = formula.toDouble(args[2]);
        double expected2 = 7.0;
        assertEquals(expected2, actual2, delta);
    }
}