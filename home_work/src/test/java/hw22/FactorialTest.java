package hw22;

import org.junit.Test;

import static org.junit.Assert.*;

public class FactorialTest {

    @Test
    public void loopForTest1() {
        Factorial factorial = new Factorial();
        String actual = factorial.loopFor(7);
        String expected = "5040";
        assertEquals(expected, actual);
    }

    @Test
    public void loopForTest2() {
        Factorial factorial = new Factorial();
        String actual = factorial.loopFor(0);
        String expected = "1";
        assertEquals(expected, actual);
    }

    @Test
    public void loopForTest3() {
        Factorial factorial = new Factorial();
        String actual = factorial.loopFor(1);
        String expected = "1";
        assertEquals(expected, actual);
    }

    @Test
    public void loopForTest4() {
        Factorial factorial = new Factorial();
        String actual = factorial.loopFor(-1);
        String expected = "0";                 // 0-factorial isn't defined
        assertEquals(expected, actual);
    }

    @Test
    public void loopWhileTest1() {
        Factorial factorial = new Factorial();
        String actual = factorial.loopWhile(7);
        String expected = "5040";
        assertEquals(expected, actual);
    }

    @Test
    public void loopWhileTest2() {
        Factorial factorial = new Factorial();
        String actual = factorial.loopWhile(0);
        String expected = "1";
        assertEquals(expected, actual);
    }

    @Test
    public void loopWhileTest3() {
        Factorial factorial = new Factorial();
        String actual = factorial.loopWhile(1);
        String expected = "1";
        assertEquals(expected, actual);
    }

    @Test
    public void loopWhileTest4() {
        Factorial factorial = new Factorial();
        String actual = factorial.loopWhile(-1);
        String expected = "0";  // 0-factorial isn't defined
        assertEquals(expected, actual);
    }

    @Test
    public void loopDoWhileTest1() {
        Factorial factorial = new Factorial();
        String actual = factorial.loopDoWhile(7);
        String expected = "5040";
        assertEquals(expected, actual);
    }

    @Test
    public void loopDoWhileTest2() {
        Factorial factorial = new Factorial();
        String actual = factorial.loopDoWhile(0);
        String expected = "1";
        assertEquals(expected, actual);
    }

    @Test
    public void loopDoWhileTest3() {
        Factorial factorial = new Factorial();
        String actual = factorial.loopDoWhile(1);
        String expected = "1";
        assertEquals(expected, actual);
    }

    @Test
    public void loopDoWhileTest4() {
        Factorial factorial = new Factorial();
        String actual = factorial.loopDoWhile(-1);
        String expected = "0"; // 0-factorial isn't defined
        assertEquals(expected, actual);
    }
}

