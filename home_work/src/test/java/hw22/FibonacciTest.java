package hw22;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

public class FibonacciTest {

    @Test
    public void loopForTest1() {
        Fibonacci fibonacci = new Fibonacci();
        String actual = fibonacci.loopFor(5);
        String[] strings = {"0", "1", "1", "2", "3"};
        String expected = Arrays.toString(strings);
        assertEquals(expected, actual);
    }

    @Test
    public void loopForTest2() {
        Fibonacci fibonacci = new Fibonacci();
        String actual = fibonacci.loopFor(0);
        String[] strings = {""};
        String expected = Arrays.toString(strings);
        assertEquals(expected, actual);
    }

    @Test
    public void loopForTest3() {
        Fibonacci fibonacci = new Fibonacci();
        String actual = fibonacci.loopFor(1);
        String[] strings = {"0"};
        String expected = Arrays.toString(strings);
        assertEquals(expected, actual);
    }

    @Test
    public void loopForTest4() {
        Fibonacci fibonacci = new Fibonacci();
        String actual = fibonacci.loopFor(9);
        String[] strings = {"0", "1", "1", "2", "3", "5", "8", "13", "21"};
        String expected = Arrays.toString(strings);
        assertEquals(expected, actual);
    }

    @Test
    public void loopForTest5() {
        Fibonacci fibonacci = new Fibonacci();
        String actual = fibonacci.loopFor(2);
        String[] strings = {"0", "1"};
        String expected = Arrays.toString(strings);
        assertEquals(expected, actual);
    }

    @Test
    public void loopWhileTest1() {
        Fibonacci fibonacci = new Fibonacci();
        String actual = fibonacci.loopWhile(5);
        String[] strings = {"0", "1", "1", "2", "3"};
        String expected = Arrays.toString(strings);
        assertEquals(expected, actual);
    }

    @Test
    public void loopWhileTest2() {
        Fibonacci fibonacci = new Fibonacci();
        String actual = fibonacci.loopWhile(0);
        String[] strings = {""};
        String expected = Arrays.toString(strings);
        assertEquals(expected, actual);

    }

    @Test
    public void loopWhileTest3() {
        Fibonacci fibonacci = new Fibonacci();
        String actual = fibonacci.loopWhile(1);
        String[] strings = {"0"};
        String expected = Arrays.toString(strings);
        assertEquals(expected, actual);
    }

    @Test
    public void loopWhileTest4() {
        Fibonacci fibonacci = new Fibonacci();
        String actual = fibonacci.loopFor(9);
        String[] strings = {"0", "1", "1", "2", "3", "5", "8", "13", "21"};
        String expected = Arrays.toString(strings);
        assertEquals(expected, actual);
    }

    @Test
    public void loopWhileTest5() {
        Fibonacci fibonacci = new Fibonacci();
        String actual = fibonacci.loopFor(2);
        String[] strings = {"0", "1"};
        String expected = Arrays.toString(strings);
        assertEquals(expected, actual);
    }

    @Test
    public void loopDoWhileTest1() {
        Fibonacci fibonacci = new Fibonacci();
        String actual = fibonacci.loopDoWhile(5);
        String[] strings = {"0", "1", "1", "2", "3"};
        String expected = Arrays.toString(strings);
        assertEquals(expected, actual);
    }

    @Test
    public void loopDoWhileTest2() {
        Fibonacci fibonacci = new Fibonacci();
        String actual = fibonacci.loopDoWhile(0);
        String[] strings = {""};
        String expected = Arrays.toString(strings);
        assertEquals(expected, actual);
    }

    @Test
    public void loopDoWhileTest3() {
        Fibonacci fibonacci = new Fibonacci();
        String actual = fibonacci.loopWhile(1);
        String[] strings = {"0"};
        String expected = Arrays.toString(strings);
        assertEquals(expected, actual);
    }

    @Test
    public void loopDoWhileTest4() {
        Fibonacci fibonacci = new Fibonacci();
        String actual = fibonacci.loopDoWhile(9);
        String[] strings = {"0", "1", "1", "2", "3", "5", "8", "13", "21"};
        String expected = Arrays.toString(strings);
        assertEquals(expected, actual);
    }

    @Test
    public void loopDoWhileTest5() {
        Fibonacci fibonacci = new Fibonacci();
        String actual = fibonacci.loopDoWhile(2);
        String[] strings = {"0", "1"};
        String expected = Arrays.toString(strings);
        assertEquals(expected, actual);
    }
}