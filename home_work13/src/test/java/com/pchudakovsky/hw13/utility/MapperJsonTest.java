package com.pchudakovsky.hw13.utility;

import com.fasterxml.jackson.databind.JsonNode;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.Test;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import static com.pchudakovsky.hw13.data.Data.URI;
import static org.junit.Assert.*;

public class MapperJsonTest {

    @Test
    public void testJsonNodeForHttpUrlNotNull() throws IOException {
        int id = 35;
        URL obj = new URL(URI + id);
        HttpURLConnection connection = (HttpURLConnection) obj.openConnection();

        JsonNode node = MapperJson.jsonNodeForHttpUrl(connection);

        assertNotNull(node);
        assertEquals(id, node.get("id").asInt());
    }

    @Test
    public void testJsonNodeForHttpClientNotNull() throws IOException {
        int id = 3;
        HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(URI + id);
        HttpResponse response = client.execute(request);

        JsonNode node = MapperJson.jsonNodeForHttpClient(response);

        assertNotNull(node);
        assertEquals(id, node.get("id").asInt());

    }
}