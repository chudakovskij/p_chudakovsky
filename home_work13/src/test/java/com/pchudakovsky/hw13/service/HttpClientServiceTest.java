package com.pchudakovsky.hw13.service;

import com.fasterxml.jackson.databind.JsonNode;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

public class HttpClientServiceTest {

    @Test
    public void testGetOnIdOfArticle() throws IOException {
        HttpClientService httpClientService = new HttpClientService();
        JsonNode jsonActual = httpClientService.get(5);
        int actual = jsonActual.get("id").asInt();
        int expected = 5;
        assertEquals(expected, actual);
    }

    @Test
    public void testGetOnTitleOfArticle() throws IOException {
        HttpClientService httpClientService = new HttpClientService();
        JsonNode jsonActual = httpClientService.get(5);
        String actual = jsonActual.get("title").asText();
        String expected = "nesciunt quas odio";
        assertEquals(expected, actual);
    }

    @Test
    public void testGetOnStatusCodeIsTrue() throws IOException {
        HttpClientService httpClientService = new HttpClientService();
        JsonNode jsonActual = httpClientService.get(7);

        int actual = jsonActual.get("id").asInt();
        int expected = 7;
        assertEquals(expected, actual);

        boolean actualFlag = httpClientService.isFlag();
        assertTrue(actualFlag);
    }

    @Test
    public void testGetOnStatusCodeIsFalse() throws IOException {
        HttpClientService httpClientService = new HttpClientService();

        JsonNode jsonActual = httpClientService.get(177);
        assertNull(jsonActual);

        boolean actualFlag = httpClientService.isFlag();
        assertFalse(actualFlag);
    }

    @Test
    public void testPostStatusCodeIsTrue() throws IOException {
        HttpClientService httpClientService = new HttpClientService();

        httpClientService.post(101, 101, "Java", "Java 8");
        assertTrue(httpClientService.isFlag());

        JsonNode jsonNode = httpClientService.getJsonNode();
        String actualTitle = jsonNode.get("title").asText();
        String expectedTitle = "Java";
        assertEquals(expectedTitle, actualTitle);
    }
}