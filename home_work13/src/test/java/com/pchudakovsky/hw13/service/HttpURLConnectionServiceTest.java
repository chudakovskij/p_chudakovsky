package com.pchudakovsky.hw13.service;

import com.fasterxml.jackson.databind.JsonNode;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

public class HttpURLConnectionServiceTest {

    @Test
    public void testGetOnIdOfArticle() throws IOException {
        HttpURLConnectionService httpURLService = new HttpURLConnectionService();
        JsonNode jsonActual = httpURLService.get(5);
        int actual = jsonActual.get("id").asInt();
        int expected = 5;
        assertEquals(expected, actual);
    }

    @Test
    public void testGetOnTitleOfArticle() throws IOException {
        HttpURLConnectionService httpURLService = new HttpURLConnectionService();
        JsonNode jsonActual = httpURLService.get(5);
        String actual = jsonActual.get("title").asText();
        String expected = "nesciunt quas odio";
        assertEquals(expected, actual);
    }

    @Test
    public void testGetOnStatusCodeIsTrue() throws IOException {
        HttpURLConnectionService httpURLService = new HttpURLConnectionService();
        JsonNode jsonActual = httpURLService.get(7);

        int actual = jsonActual.get("id").asInt();
        int expected = 7;
        assertEquals(expected, actual);

        boolean actualFlag = httpURLService.isFlag();
        assertTrue(actualFlag);
    }

    @Test
    public void testGetOnStatusCodeIsFalse() throws IOException {
        HttpURLConnectionService httpURLService = new HttpURLConnectionService();

        JsonNode jsonActual = httpURLService.get(177);
        assertNull(jsonActual);

        boolean actualFlag = httpURLService.isFlag();
        assertFalse(actualFlag);
    }

    @Test
    public void testPostStatusCodeIsTrue() throws IOException {
        HttpURLConnectionService httpURLService = new HttpURLConnectionService();
        httpURLService.post(101, 101, "Java", "Java 8");

        assertTrue(httpURLService.isFlag());

        JsonNode jsonNode = httpURLService.getJsonNode();
        String actualTitle = jsonNode.get("title").asText();
        String expectedTitle = "Java";
        assertEquals(expectedTitle, actualTitle);
    }
}