package com.pchudakovsky.hw13.api;

import com.fasterxml.jackson.databind.JsonNode;

import java.io.IOException;

/**
 * Interface provides two methods for HTTP-request.
 *
 * @author Pavel Chudakovsky.
 */
public interface HttpInterface {

    /**
     * Method returns variable
     * of type JsonNode after Get-request.
     *
     * @param id value for id of article.
     * @return object JsonNode.
     * @throws IOException if an I/O exception occurs.
     */
    JsonNode get(int id) throws IOException;

    /**
     * Method creates of  post-request.
     *
     * @param newUserId new Id for user.
     * @param newId     new Id for article.
     * @param newTitle  new title for article.
     * @param newBody   new body for article.
     * @throws IOException in case of a problem or
     *                     the connection was aborted.
     */
    void post(int newUserId, int newId, String newTitle, String newBody)
            throws IOException;
}
