package com.pchudakovsky.hw13;

/**
 * Test class.
 *
 * @author Pavel Chudakovsky.
 */
public class Main {

    /**
     * Calls method start() {@link Runner#start()} }
     *
     * @param args arrays of type String.
     */
    public static void main(String[] args) {
        Runner runner = new Runner();
        runner.start();
    }
}
