package com.pchudakovsky.hw13.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.pchudakovsky.hw13.api.HttpInterface;
import com.pchudakovsky.hw13.utility.MapperJson;
import org.apache.http.HttpException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import static com.pchudakovsky.hw13.data.Data.CREATED;
import static com.pchudakovsky.hw13.data.Data.OK;
import static com.pchudakovsky.hw13.data.Data.URI;

/**
 * Class for sending HTTP requests and
 * receiving HTTP responses from a resource
 * identified by a URI.
 *
 * @author Pavel Chudakovsky.
 * @see HttpURLConnection
 */
public class HttpURLConnectionService implements HttpInterface {
    /**
     * Logger.
     */
    private static Logger log = LoggerFactory
            .getLogger(HttpURLConnectionService.class);

    /**
     * Variable for working with JSON.
     */
    private JsonNode jsonNode;

    /**
     * Variable is used for Unit tests.
     */
    private boolean flag;

    /**
     * Returns variable of type boolean.
     *
     * @return boolean value.
     */
    boolean isFlag() {
        return flag;
    }

    /**
     * Setter for variable flag.
     *
     * @param flag variable of type boolean.
     */
    private void setFlag(boolean flag) {
        this.flag = flag;
    }

    /**
     * Getter for variable jsonNode.
     *
     * @return variable of type JsonNode.
     */
    JsonNode getJsonNode() {
        return jsonNode;
    }

    /**
     * Method returns variable
     * of type JsonNode after Get-request.
     *
     * @param id value for id of article.
     * @return object JsonNode.
     * @throws IOException if an I/O exception occurs.
     */
    public JsonNode get(int id) throws IOException {
        log.info("method execution get().");
        URL obj = new URL(URI + id);
        HttpURLConnection connection = (HttpURLConnection) obj.openConnection();
        connection.setRequestMethod("GET");
        int responseCode = connection.getResponseCode();
        System.out.println("\nSending 'GET' request to URL : " + URI);
        checkResponse(responseCode, OK, connection);
        return jsonNode;
    }

    /**
     * Method creates of  post-request.
     *
     * @param newUserId new Id for user.
     * @param newId     new Id for article.
     * @param newTitle  new title for article.
     * @param newBody   new body for article.
     * @throws IOException in case of a problem or
     *                     the connection was aborted.
     */
    public void post(int newUserId, int newId, String newTitle, String newBody)
            throws IOException {
        log.info("method execution post()");
        setFlag(false);
        URL obj = new URL(URI);
        HttpURLConnection connection = (HttpURLConnection) obj.openConnection();
        connection.setRequestMethod("POST");

        String urlParameters = "userId=" + newUserId + "&"
                + "id=" + newId + "&" + "title=" + newTitle
                + "&" + "body=" + newBody;

        connection.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
        wr.writeBytes(urlParameters);
        wr.flush();
        wr.close();

        int responseCode = connection.getResponseCode();
        System.out.println("\nSending 'POST' request to URL : " + URI);
        checkResponse(responseCode, CREATED, connection);
    }

    /**
     * Checks response status.
     *
     * @param responseCode actual status code.
     * @param code         expected code.
     * @param connection   variable of type HttpUrlConnection.
     * @throws IOException if an I/O error occurs while creating
     *                     the input stream.
     */
    private void checkResponse(int responseCode, int code, HttpURLConnection connection)
            throws IOException {

        if (responseCode == code) {
            System.out.println("Response Code : " + responseCode);
            jsonNode = MapperJson.jsonNodeForHttpUrl(connection);
            setFlag(true);
            log.info("Method finished working!");
        } else {
            try {
                setFlag(false);
                throw new HttpException("Bad request!Create a new request!");
            } catch (HttpException e) {
                log.error("Response code: " + responseCode + ".", e);
            }
        }
    }
}
