package com.pchudakovsky.entity;

/**
 * Describes of behavior for user.
 */
public class User {
    /**
     * User name.
     */
    private String name;

    /**
     * Sole constructor.
     *
     * @param name user name.
     */
    public User(String name) {
        this.name = name;
    }

    /**
     * Getter for name.
     *
     * @return user name.
     */
    @SuppressWarnings("unused")
    public String getName() {
        return name;
    }

    /**
     * Setter for name.
     *
     * @param name user name.
     */
    @SuppressWarnings("unused")
    public void setName(String name) {
        this.name = name;
    }


}
