package com.pchudakovsky.entity;

import java.math.BigDecimal;

/**
 * Describes the product for Online shop.
 */
public class Product {

    /**
     * Name of product.
     */
    private String name;

    /**
     * Cost of product.
     */
    private BigDecimal cost;

    /**
     * Sole constructor.
     *
     * @param name name of product.
     * @param cost ost of product.
     */
    public Product(String name, BigDecimal cost) {
        this.name = name;
        this.cost = cost;
    }

    /**
     * Getter for name of product.
     *
     * @return name of product.
     */
    @SuppressWarnings("unused")
    public String getName() {
        return name;
    }

    /**
     * Getter for cost of product.
     *
     * @return cost of product.
     */

    @SuppressWarnings("unused")
    public BigDecimal getCost() {
        return cost;
    }

}
