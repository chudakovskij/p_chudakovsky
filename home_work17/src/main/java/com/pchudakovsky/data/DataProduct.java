package com.pchudakovsky.data;

import com.pchudakovsky.entity.Product;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Data class.
 */
public class DataProduct {
    /**
     * List of products
     * as fake data for testing .
     */
    private static List<Product> listProduct = new ArrayList<>();

    /**
     * Returns of list of products.
     *
     * @return list of Product.
     */
    public List<Product> getProducts() {

        Product pr1 = new Product("CarA", BigDecimal.valueOf(2500.34));
        Product pr2 = new Product("CarB", BigDecimal.valueOf(2600.34));
        Product pr3 = new Product("CarC", BigDecimal.valueOf(2700.34));
        Product pr4 = new Product("CarD", BigDecimal.valueOf(2800.34));
        Product pr5 = new Product("CarE", BigDecimal.valueOf(2900.34));
        Product pr6 = new Product("CarCF", BigDecimal.valueOf(3000.34));

        listProduct.add(pr1);
        listProduct.add(pr2);
        listProduct.add(pr3);
        listProduct.add(pr4);
        listProduct.add(pr5);
        listProduct.add(pr6);
        return listProduct;
    }


}
