package com.pchudakovsky.service;

import com.pchudakovsky.entity.Product;

import java.math.BigDecimal;
import java.util.List;

/**
 * Provides methods for
 * the operation of the online store service.
 */
public interface ShopService {

    /**
     * Returns total price of products.
     *
     * @param products information about products as array of string.
     * @return total price as BigDecimal.
     */
    BigDecimal getTotalCost(String[] products);

    /**
     * Returns of list of products.
     *
     * @return list of products.
     */
    List<Product> getListProduct();

}

