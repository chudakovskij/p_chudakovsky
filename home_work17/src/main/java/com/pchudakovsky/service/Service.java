package com.pchudakovsky.service;

import com.pchudakovsky.data.DataProduct;
import com.pchudakovsky.entity.Product;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.List;

/**
 * Provides methods for
 * presenting results in a browser.
 *
 * @author Pavel Chudakovsky.
 */

public class Service implements ShopService {

    /**
     * Returns total price of products.
     *
     * @param products information about products as array of string.
     * @return total price as BigDecimal.
     */
    public BigDecimal getTotalCost(String[] products) {
        String[] cost = new String[products.length];
        for (int i = 0; i < products.length; i++) {
            String[] array = products[i].split(" ");
            cost[i] = array[1];
        }
        BigDecimal total = BigDecimal.valueOf(0);
        for (String str : cost) {
            BigDecimal sum = new BigDecimal(Double.valueOf(str));
            total = total.add(sum, MathContext.DECIMAL32);
        }
        return total;
    }


    /**
     * Returns of list of products.
     *
     * @return list of products.
     */
    public List<Product> getListProduct() {
        DataProduct dataProduct = new DataProduct();
        return dataProduct.getProducts();

    }
}


