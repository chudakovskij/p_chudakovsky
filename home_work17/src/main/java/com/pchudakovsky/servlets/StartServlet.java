package com.pchudakovsky.servlets;

import com.pchudakovsky.service.Service;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Servlet for  the start page
 * of an online shop.
 *
 * @author Pavel Chudakovsky.
 */
public class StartServlet extends HttpServlet {

    /**
     * Logger.
     */
    private final static Logger LOGGER = LogManager.getLogger(Service.class);

    /**
     * Method initializes servlet.
     */
    @Override
    public void init() {
        LOGGER.info("Method init() for "
                + StartServlet.class + " is started!");
    }

    /**
     * Get-request.
     *
     * @param req  variable of type {@link HttpServletRequest}.
     * @param resp variable of type {@link HttpServletResponse}.
     * @throws IOException if an input or output exception occurred.
     */
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {

        LOGGER.info("Method doGet() for "
                + StartServlet.class + " is started!");

        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/view/start.jsp");
        dispatcher.forward(req, resp);
    }

    /**
     * Called by the servlet container to indicate
     * to a servlet that the servlet is being
     * taken out of service.
     */
    @Override
    public void destroy() {
        LOGGER.info("Method destroy() for "
                + StartServlet.class + " is started!");
    }
}
