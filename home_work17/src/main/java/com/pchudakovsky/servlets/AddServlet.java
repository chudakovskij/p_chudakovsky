package com.pchudakovsky.servlets;

import com.pchudakovsky.service.Service;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Servlet to add products to basket.
 *
 * @author Pavel Chudakovsky.
 */
public class AddServlet extends HttpServlet {

    /**
     * Logger.
     */
    private final static Logger LOGGER = LogManager.getLogger(Service.class);

    /**
     * Method initializes servlet.
     */
    @Override
    public void init() {
        LOGGER.info("Method init() for "
                + AddServlet.class + " is started!");
    }

    /**
     * Post-request.
     *
     * @param req  variable of type {@link HttpServletRequest}.
     * @param resp variable of type {@link HttpServletResponse}.
     * @throws IOException if an input or output exception occurred.
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {

        LOGGER.info("Method doPost() for "
                + AddServlet.class + " is started!");

        String product = req.getParameter("selectProducts");
        HttpSession session = req.getSession();

        List basket;
        basket = (List) session.getAttribute("basket");

        if (basket == null) {
            basket = new ArrayList<>();
            session.setAttribute("basket", basket);
        }

        if (product != null) {
            //noinspection unchecked
            basket.add(product);
            session.setAttribute("basket", basket);
        }

        session.setAttribute("product", product);

        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/view/menu.jsp");
        dispatcher.include(req, resp);
    }

    /**
     * Called by the servlet container to indicate
     * to a servlet that the servlet is being
     * taken out of service.
     */
    @Override
    public void destroy() {
        LOGGER.info("Method destroy() for "
                + AddServlet.class + " is started!");
    }
}
