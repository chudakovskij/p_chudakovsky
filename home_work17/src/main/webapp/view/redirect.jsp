<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false"%>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Online Shop</title>
</head>

<body>
   <div style="margin: 25%; display: flex; align-items: center;
       justify-content: center;  font-size: larger;border-radius: 200px;
       width: 400px;height: 400px;background:red; color: white;">
           <div> Oops! You shouldn't be here.
               <div> Please, agree with the terms of service first</div>
               <div> Follow the link below...</div>
                   <a style="color: white;" href="http://localhost:8080/online-shop">
                   <h3>Start Page... </h3>
               </a>
           </div>
       </div>
       </div>
</body>

</html>