package com.pchudakovsky.utility;

import org.junit.Test;

import static com.pchudakovsky.utility.ValidatorName.checkName;
import static com.pchudakovsky.utility.ValidatorName.nameIsNull;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ValidatorNameTest {

    @Test
    public void testCheckNameOnGoodName() {
        assertTrue(checkName("adminA"));
        assertTrue(checkName("admin-admin"));
        assertTrue(checkName("admin_admin"));
        assertTrue(checkName("admin11"));
        assertTrue(checkName("12admin"));
    }

    @Test
    public void testCheckNameOnBadName() {
        assertFalse(checkName("Арбуз"));
        assertFalse(checkName("a"));
        assertFalse(checkName("admin_admin_admin_admin"));
        assertFalse(checkName("ad"));
    }

    @Test
    public void nameIsNullTrue() {

        assertTrue(nameIsNull(null));
    }

    @Test
    public void nameIsNullFalse() {
        assertFalse(nameIsNull("Petrov2019"));
    }
}