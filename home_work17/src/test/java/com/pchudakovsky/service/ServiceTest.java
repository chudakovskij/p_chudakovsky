package com.pchudakovsky.service;

import com.pchudakovsky.entity.Product;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ServiceTest {

    private List<Product> list1 = new ArrayList<>();
    private List<Product> list2 = new ArrayList<>();

    @Before
    public void init() {
        Product product1 = new Product("Product1", new BigDecimal("1000"));
        Product product2 = new Product("Product2", new BigDecimal("2000"));
        Product product3 = new Product("Product3", new BigDecimal("5000"));

        list1.add(product1);
        list1.add(product2);

        list2.add(product1);
        list2.add(product2);
        list2.add(product3);
    }

    @Mock
    Service mockService = new Service();

    @Test
    public void testGetTotalCost() {
        // given:
        String[] products = {"CarA 2000 $", "CarA 3000 $"};
        BigDecimal expected = new BigDecimal("5000");
        when(mockService.getTotalCost(products)).thenReturn(expected);

        // when,then 1:
        assertEquals(new BigDecimal("5000"), mockService.getTotalCost(products));

        // when,then 2:
        assertNotEquals(new BigDecimal("100"), mockService.getTotalCost(products));

        // when,then 3:
        assertNotNull(mockService.getTotalCost(products));

        // then:
        verify(mockService, times(3)).getTotalCost(products);
    }

    @Test
    public void getListProductOnEquals() {

        //given:
        when(mockService.getListProduct()).thenReturn(list1);

        // when,then 1:
        assertNotNull(mockService.getListProduct());

        // when, then 2:
        assertEquals(list1, mockService.getListProduct());

        //then:
        verify(mockService, times(2)).getListProduct();
    }

    @Test
    public void getListProductOnNotEquals() {

        //given:
        when(mockService.getListProduct()).thenReturn(list1);

        // when,then 1:
        assertNotNull(mockService.getListProduct());

        // when, then 2:
        assertNotEquals(mockService.getListProduct(), list2);

        //then:
        verify(mockService, times(2)).getListProduct();
    }

}