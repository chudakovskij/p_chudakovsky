package com.pchudakovsky.hw5;


import org.junit.Test;

import static org.junit.Assert.*;

public class FileTest {

    @Test
    public void testGetNameFile() {
        Folder root = new Folder("root");
        Folder folder = new Folder();
        LineHandler lineHandler = new LineHandler();
        File file = new File(lineHandler.nameFile("MyFolder/folder1/file1.exe"));
        String actual = file.getNameFile();
        String expected = "file1";
        assertEquals(expected, actual);
    }

    @Test
    public void print() {
        LineHandler lineHandler = new LineHandler();
        File file = new File(lineHandler.nameFile("MyFolder/folder1/file1.exe"));
        file.setFullPath("MyFolder/folder1/file1.exe");
        file.print();
        boolean actual = file.isResult();
        assertTrue(actual);
    }


}