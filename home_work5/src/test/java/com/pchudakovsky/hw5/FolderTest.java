package com.pchudakovsky.hw5;

import com.pchudakovsky.hw5.api.Component;
import org.junit.Test;

import static org.junit.Assert.*;

public class FolderTest {

    @Test
    public void testPrint() {
        Folder root = new Folder("root");
        Folder folder = new Folder();
        LineHandler lineHandler = new LineHandler();
        File file1 = new File(lineHandler.nameFile("MyFolder/folder1/ file1.exe"));
        File file2 = new File(lineHandler.nameFile("MyFolder/folder2/ file2.exe"));
        file1.setFullPath("MyFolder/folder1/ file1.exe");
        file2.setFullPath("MyFolder/folder2/ file1.exe");
        folder.add(file1);
        folder.add(file2);
        root.add(folder);
        root.print();
        boolean actual = root.isResult();
        assertTrue(actual);
    }

    @Test
    public void testAdd() {
        Folder folder = new Folder();
        Component file1 = new File("file1");
        folder.add(file1);
        boolean flagAdd = folder.getListPrinter().isEmpty();
        assertFalse(flagAdd);
        boolean actual = folder.isResult();
        assertTrue(actual);
    }
}