package com.pchudakovsky.hw5;

/**
 * Class provides methods for working in strings
 *
 * @author Pavel Chudakovsky
 */
class LineHandler {

    /**
     * Returns the file name by the entered path
     *
     * @param path path to file
     * @return name file
     */
    String nameFile(String path) {
        String nameFile = null;
        if (path.contains(".")) {
            String[] splitLine = path.split("(?=/)");// divide the path into tokens
            String file = splitLine[splitLine.length - 1]; // store the name and file extension
            String[] array = file.split("\\.");
            nameFile = array[0].replace("/", "");
        }
        return nameFile;
    }

    /**
     * Checks the name and file extension
     *
     * @param path path to file
     * @return value of true or false
     */
    boolean checkFile(String path) {
        boolean flag = false;
        boolean flagName = false;
        boolean flagExtension = false;

        if (path.contains(".")) {
            String[] splitLine = path.split("(?=/)");//  divide the path into tokens
            String file = splitLine[splitLine.length - 1]; // store the name and file extension

            String[] array = file.split("\\.");
            String nameFile = array[0].replace("/", "");
            String extension = array[1];

            String regular = ".*[@#%={}^$!*&+()\\[\\]><?].*";

            boolean nameNumber = nameFile.matches(".*[0-9].*");
            boolean nameLiter = nameFile.matches(".*[a-zA-Z].*");
            boolean nameSymbol = nameFile.matches(regular);

            boolean lengthExtension = extension.matches("\\w{3,6}");
            boolean beginExtension = extension.matches("^[^0-9].*");
            boolean symbolExtension = extension.matches(regular);

            if ((nameLiter | nameNumber) & !nameSymbol) {
                flagName = true;
            }
            if (lengthExtension & beginExtension & !symbolExtension) {
                flagExtension = true;
            }

            if (flagName & flagExtension) {
                flag = true;
            }
        }
        return flag;
    }


    /**
     * @param line value of String type
     * @return value true or false
     */
    boolean checkPath(String line) {
        boolean flag = true;
        if (line.contains(".")) {
            String[] splitLine = line.split("/");// divide the path into tokens
            String[] array = new String[splitLine.length - 1]; // stores path tokens
            if (splitLine.length - 1 >= 0) System.arraycopy(splitLine, 0, array, 0, splitLine.length - 1);
            String path = String.join("", array);
            String regular = ".*[:|*><?].*";
            boolean pathSymbol = path.matches(regular);//  invalid folder name
            if (pathSymbol) {
                flag = false;
            }
        }
        return flag;

    }
}






