package com.pchudakovsky.hw5;

import com.pchudakovsky.hw5.api.Component;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * Class models the behavior of folder.
 *
 * @author Pavel Chudakovsky.
 */
public class Folder implements Component {


    /**
     * Name of folder
     */
    private String nameFolder;

    /**
     * used in tests
     */
    private boolean result;

    /**
     * Empty constructor
     */
    Folder() {
    }

    /**
     * Constructor contains the folder name
     *
     * @param nameFolder as name of folder
     */
    Folder(String nameFolder) {
        this.nameFolder = nameFolder;
    }


    boolean isResult() {
        return result;
    }

    private void setResult(boolean result) {
        this.result = result;
    }

    HashSet<Component> getListPrinter() {
        return listPrinter;
    }

    /**
     * Stores components in a folder
     */
    private HashSet<Component> listPrinter = new HashSet<>();

    /**
     * Prints of structure root folder
     */
    @Override
    public void print() {
        setResult(false);
        if (nameFolder != null) {
            System.out.print("root node: " + nameFolder + "\n");
        }
        for (Component component : listPrinter) {
            component.print();
        }
        setResult(true);

    }

    /**
     * Adds files and folders to the root folder
     *
     * @param component as folder or file
     */
    void add(Component component) {
        setResult(false);
        listPrinter.add(component);
        setResult(true);
    }
}