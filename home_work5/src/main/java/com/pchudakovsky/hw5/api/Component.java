package com.pchudakovsky.hw5.api;

public interface Component {
    void print();
}
