package com.pchudakovsky.hw5;

/**
 * This class for launch app.
 *
 * @author Pavel Chudakovsky.
 */
public class Main {
    public static void main(String[] args) {
        Run run = new Run();
        run.run();
    }
}