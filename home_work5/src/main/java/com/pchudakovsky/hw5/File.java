package com.pchudakovsky.hw5;

import com.pchudakovsky.hw5.api.Component;

/**
 * Class models the behavior of file.
 *
 * @author Pavel Chudakovsky.
 */
public class File implements Component {

    /**
     * Name of  file
     */
    private String nameFile;
    /**
     * Path to file
     */
    private String fullPath;
    /**
     * Used in tests
     */
    private boolean result;


    //Getters and setters
    File(String nameFile) {
        this.nameFile = nameFile;
    }

    String getNameFile() {
        return nameFile;
    }

    private String getFullPath() {
        return fullPath;
    }

    void setFullPath(String fullPath) {
        this.fullPath = fullPath;
    }

    boolean isResult() {
        return result;
    }

    private void setResult(boolean result) {
        this.result = result;
    }

    /**
     * Prints structure of catalog
     */
    @Override
    public void print() {
        setResult(false);
        String path = getFullPath();
        String[] array = path.split("(?=/)");
        int space = 1;
        for (int i = 1; i <= array.length - 1; i++) {
            String shift = String.valueOf(space += 8);
            String tab = "%".concat(shift).concat("s%n");
            System.out.printf(tab, array[i]);
        }
        setResult(true);
    }
}
