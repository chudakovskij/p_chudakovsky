package com.pchudakovsky.hw5;

import java.util.Scanner;

/**
 * Class models the behavior of file system.
 *
 * @author Pavel Chudakovsky.
 */
class Run {

    private static final String PRINT = "print";
    private static final String ROOT = "root";
    private static final String EXIT = "0";

    /**
     * Starts file system simulation
     */
    void run() {
        try (Scanner scanner = new Scanner(System.in)) {
            Folder rootFolder = new Folder(ROOT);
            while (true) {
                LineHandler lineHandler = new LineHandler();
                info();
                String path = scanner.nextLine();

                if (EXIT.equals(path)) {
                    break;
                } else if (PRINT.equals(path)) {
                    rootFolder.print();
                } else if (lineHandler.checkFile(path) & lineHandler.checkPath(path)) {
                    String nameFile = lineHandler.nameFile(path);
                    File file = new File(nameFile);
                    System.out.println("File created with name: " + file.getNameFile());
                    file.setFullPath(path);
                    folder(file, rootFolder);
                    System.out.println("File recorded!");
                } else {
                    message();
                }
            }

        }
    }

    /**
     * Prints information
     */
    private void info() {
        System.out.println("\nProgram *File system* ");
        System.out.println("0- for exit");
        System.out.println("print - to print result ");
        System.out.println("Enter path to file: ");
        System.out.println("************************");
    }

    /**
     * Print message
     */
    private void message() {
        System.out.println("Incorrect entry of the folder name. " +
                "Forbidden symbols: |,*<,>,?,:.\n" +
                "OR\n"
                + "Incorrect input of the name or file extension:" +
                "Forbidden symbols:" + "@,!,*,#,$,(,),[,],{,},<,>,?,^=\n" +
                "Warning! Extension cannot be less than 3 and more than 6 characters.\n" +
                "Extension cannot start with a digit:0-9.");
    }

    // auxiliary method
    private void folder(File file, Folder rootFolder) {
        Folder folder = new Folder();
        folder.add(file);
        rootFolder.add(folder);
    }
}



